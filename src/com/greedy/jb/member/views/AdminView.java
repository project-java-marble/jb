package com.greedy.jb.member.views;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * 
 * <pre>
 * Class : AdminView
 * Comment : 클래스에 대한 설명
 * History
 * 2021/12/19 박성준 처음 작성
 * 2021/12/19 박성준 관리자 패널 추가
 * 2021/12/20 장민주 최종 완성
 * 2021/12/20 박성준 주석처리
 * </pre>
 * @author 박성준
 * @version 1.0.0
 * @see AdminDAO, AdminService, MainFrame, MainMenu
 *
 */

public class AdminView extends JFrame {
   /** MainFrame에 전달할 con */
   private JPanel contentPane;
   
   /**
    * MainFrame에 로그인 배경을 넣고 로그인창, 뒤로가기 버튼, 확인버튼을 넣은 contentPane을 추가해주기
    * @param mf
    */
   public AdminView(MainFrame mf) {
      /* 로그인 배경 넣고 크기 조절하기 */
      ImageIcon adminLogInImage = new ImageIcon("img/admin-login-page.png");
      
      JPanel adminLogInPanel = new JPanel() {
      public void paintComponent(Graphics g) {
         g.drawImage(adminLogInImage.getImage(), 0, 0, 1440, 900, null);
      }
   };
   	  
      adminLogInPanel.setBounds(0, 0, 1440, 900);
      
      /* 로그인창과 배경, 뒤로가기 버튼과 확인 버튼을 담아줄 contentpanePane 생성과 컴포넌트들을 겹칠 수 있게 하기 위한
       * isOptimizedDrawingEnabled 익명 메소드 선언 */
      contentPane = new JPanel() {
         public boolean isOptimizedDrawingEnabled() {
            return false;
         }
      };
      
      /*화면을 출력하기 위해 MainFrame에 contentPane 추가 */
      mf.add(contentPane);
      
      /* 뒤로가기 버튼 구현 */
      ImageIcon bBack = new ImageIcon("img/Back_image.png");
      Image img1 = bBack.getImage();
      Image changeImage1 = img1.getScaledInstance(200, 110, Image.SCALE_SMOOTH);
      ImageIcon bBack2 = new ImageIcon(changeImage1);    
 
      JButton btnBack = new JButton(bBack2);
      btnBack.setBounds(0, 0, 200, 110);
      
     btnBack.setBorderPainted(false);
     btnBack.setFocusPainted(false);
     btnBack.setContentAreaFilled(false);
     
     /* 관리자 아이디 입력 칸 이미지와 패스워드 입력칸 이미지 구현해서 라벨에 담아서 로그인 패널에 띄우기*/
     JPanel logIn = new JPanel();
     ImageIcon adminIdImg = new ImageIcon("img/adminIdInput.png");
     JLabel id = new JLabel(adminIdImg);
     ImageIcon passwordInputImg = new ImageIcon("img/adminPasswordInput.png");
     JLabel password = new JLabel(passwordInputImg);     
     
     /*id입력창과 비밀번호 입력창 만들기*/
     JTextField idInput = new JTextField(15);
     idInput.setFont(idInput.getFont().deriveFont(60.0f));
     JTextField passwordInput = new JTextField(15);
     passwordInput.setFont(passwordInput.getFont().deriveFont(60.0f));
     
     /*로그인 패널의 레이아웃 초기화, 로그인, id, password 입력칸의 위치 좌표와 크기 설정 */
     logIn.setLayout(null);
     logIn.setBounds(0, 284, 542, 412);
     id.setBounds(0, 0, 542, 67);
     idInput.setBounds(0, 68, 542, 139);
     password.setBounds(0, 206, 542, 67);
     passwordInput.setBounds(0, 273, 542, 139);
     
     /* 로그인 패널에 id, password 입력칸 추가하기 */
     logIn.add(id);
     logIn.add(idInput);
     logIn.add(password);
     logIn.add(passwordInput);
     
     /* 확인 버튼 만들기*/
     ImageIcon bcon = new ImageIcon("img/confirm_image.png");
     Image img2 = bcon.getImage();
     Image changeImage2 = img2.getScaledInstance(300, 110, Image.SCALE_SMOOTH);
     ImageIcon bcon2 = new ImageIcon(changeImage2);

     JButton btnCon = new JButton(bcon2);
     btnCon.setBounds(120, 710, 300, 110);
     btnCon.setFocusPainted(false);
     btnCon.setBorderPainted(false);
     btnCon.setContentAreaFilled(false);
      /* contentPane에 배경화면, 뒤로가기 버튼, 확인 버튼 로그인 창 추가 */
     contentPane.setLayout(null);
     contentPane.add(adminLogInPanel);
     contentPane.add(btnBack);
     contentPane.add(btnCon);
     contentPane.add(logIn);
     
     /* contentPane에 층 설정하기 */
     contentPane.setComponentZOrder(adminLogInPanel, 1);
     contentPane.setComponentZOrder(btnBack, 0);
     contentPane.setComponentZOrder(logIn, 0);
     contentPane.setComponentZOrder(btnCon, 0);
      
     /* 뒤로가기 버튼을 눌렀을 때 메인화면으로 이동할 수 있게 설정 */
     btnBack.addActionListener(new ActionListener() {
         
	     @Override
	     public void actionPerformed(ActionEvent e) {
	        PanelSwitch.changePanel(mf, contentPane, new MainMenu(mf).getMainPanel());
	        System.out.println("메인화면 패널로 전환");
	        
	     }
	  });
   }

/**
 * 메소드 getAdminLogIn으로 MainFrame에 보낼 contentPane 반환
 * @return contentPane 반환
 */
public JPanel getAdminLogIn() {
      
      return contentPane;
   } 
}
