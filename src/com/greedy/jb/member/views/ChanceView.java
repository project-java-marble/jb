package com.greedy.jb.member.views;

import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import com.greedy.jb.member.controller.chancecardcontroller.ChanceCardController;
import com.greedy.jb.member.model.dto.ChanceCardDTO;
/**
 * 	<pre>
 * 	Class : ChanceView
 *  Comment : 게임찬스카드 팝업창 랜덤구현
 *  History
 *  2021/12/20 (김영광)
 * 	</pre>
 *  @author GLORY
 *  @version 1.0.0
 *  	 
 * */
public class ChanceView {

	/** 찬스카드  정보가 담기위한 것 */
	private JPanel contentPane = new JPanel();
	
	public ChanceView(MainFrame mf, List<ChanceCardDTO> chanceCardList) {

		/* 찬스카드 리스트를 보여질 뷰 클래스 */

		ChanceCardController cc = new ChanceCardController();

		/* 랜덤 수 생성 */
		int random = (int)(Math.random() * 5) + 1;

		JLabel jl = null;
		contentPane.setLayout(null);

		/* 랜덤 수와 가져올 List<ChanceCardDTO> 정보를 스위치문을 통해 일치시켜 꺼내오기*/
		switch(random) {
		case 1 : chanceCardList.get(1);
		System.out.println(chanceCardList.get(1).toString());
		cc.ChanceCardController(); 
		jl = new JLabel(chanceCardList.get(1).toString());
		break;
		case 2 : chanceCardList.get(2);
		System.out.println(chanceCardList.get(2).toString());
		cc.ChanceCardController();
		jl = new JLabel(chanceCardList.get(2).toString());
		break;
		case 3 : chanceCardList.get(3);
		System.out.println(chanceCardList.get(3).toString());
		cc.ChanceCardController();
		jl = new JLabel(chanceCardList.get(3).toString());
		break;
		case 4 : chanceCardList.get(4);
		System.out.println(chanceCardList.get(4).toString());
		cc.ChanceCardController();
		jl = new JLabel(chanceCardList.get(4).toString());
		break;
		case 5 : chanceCardList.get(5);
		System.out.println(chanceCardList.get(5).toString());
		cc.ChanceCardController();
		jl = new JLabel(chanceCardList.get(5).toString());
		break;
		}

		/* 결과 나온 값을 라벨 사이즈를 맞춰주고 contentPane에  담아 출력하기*/
		jl.setBounds(300, 500, 500, 500);
		contentPane.add(jl);
		mf.add(contentPane);



	}
}	






