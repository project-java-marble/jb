package com.greedy.jb.member.views;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.jb.member.model.dto.ChanceCardDTO;
/**
 * 	<pre>
 * 	Class : goToChance
 *  Comment : 게임찬스카드 팝업창 랜덤구현
 *  History
 *  2021/12/20 (김영광)
 * 	</pre>
 *  @author GLORY
 *  @version 1.0.0
 *  @see ChanceView
 *  	 mapper/member-query.xml
 *  	 
 * */
public class goToChance {

	public goToChance() {
		
		/* 유저가 특별이벤트 칸을 밟으면 랜덤한 카드를 받기 위한 클래스 */
		
		/* 메인 부분 */
		JFrame f = new JFrame();
		f.setBounds(100, 100, 1440, 900);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);
		
		/* 서비스 연결 */
		Connection con = getConnection();
		
		/* DAO 로직 부분  */
		Statement stmt = null;
		
		ResultSet rset = null;
		
		try {
			stmt = con.createStatement();
			rset = stmt.executeQuery("SELECT CHANCE_NO FROM JB_CHANCE_CARD");
			
			if(rset.next()) {
			System.out.println(rset.getString("CHANCE_NO"));
		}
		
		} catch (SQLException e1) {
			e1.printStackTrace();
		}finally {
			close(rset);
			close(stmt);
			close(con);
		}
		
		/* 뷰 부분 */
		
		/* 랜덤한 카드를 주기 위한 난수 */
		int random = (int)(Math.random() * 5) + 1;
				
		switch(random) {
			case 1 :
				/* 다이얼로그에 넣을 패널생성  */
				JPanel dialoghellescapePanel = new JPanel();
				dialoghellescapePanel.setLayout(null);
				
				/* 복습지옥 1회면제 찬스카드 이미지*/
				ImageIcon hellescape = new ImageIcon("C:/Users/GLORY/git/jb/img/hellescape.png"); 
				JLabel hellescapeLabel = new JLabel(hellescape);
				hellescapeLabel.setBounds(0, 0, 550, 750);
				
				/* 버튼*/
				JButton btn = new JButton();
				btn.setLocation(17,10);
				btn.setSize(517, 731);
				btn.setOpaque(false); 
				btn.setContentAreaFilled(false); 
				
				/* 패널에 버튼과 라벨 이미지 넣기*/
				dialoghellescapePanel.add(btn);
				dialoghellescapePanel.add(hellescapeLabel);
				
				/* 다이얼로그 생성*/
				Dialog hellescapeDialog = new Dialog(f, "hellEscape");
				hellescapeDialog.setBounds(650, 100, 565, 800);
				hellescapeDialog.add(dialoghellescapePanel);
				hellescapeDialog.setVisible(true);
				
				/* 버튼 이벤트 */
				btn.addActionListener(new ActionListener() {			
					
					@Override
					public void actionPerformed(ActionEvent e) {
						hellescapeDialog.setVisible(false);
						hellescapeDialog.dispose();	
					}
				});
				break;
			case 2 :
				
				/* 다이얼로그에 넣을 패널생성 */
				JPanel doubleChancePanel = new JPanel();
				doubleChancePanel.setLayout(null);
				
				/* 주사위 더블 찬스칸드 이미지*/
				ImageIcon doubleDice = new ImageIcon("C:/Users/GLORY/git/jb/img/double.png");
				JLabel doubleLabel = new JLabel(doubleDice);
				doubleLabel.setBounds(0, 0, 550, 750);
				
				/* 버튼 */
				JButton btn2 = new JButton();
				btn2.setLocation(17,10);
				btn2.setSize(517, 731);
				btn2.setOpaque(false); //투명하게
				btn2.setContentAreaFilled(false); //내용 채우기 안함
				
				/* 패널에 버튼과 라벨 이미지 넣기 */
				doubleChancePanel.add(btn2);
				doubleChancePanel.add(doubleLabel);
				
				/* 다이얼로그 생성 */
				Dialog doubleDialog = new Dialog(f, "doubleChance");
				doubleDialog.setBounds(650, 100, 565, 800);
				doubleDialog.add(doubleChancePanel);
				doubleDialog.setVisible(true);
				
				/* 버튼 이벤트*/
				btn2.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						doubleDialog.setVisible(false);
						doubleDialog.dispose();		
					}
				});
				break;
			case 3 :
				/* 다이얼로그에 넣을 패널생성  */
				JPanel goHellPanel = new JPanel();
				goHellPanel.setLayout(null);
				
				/* 복습지옥 이동 찬스칸드 이미지*/
				ImageIcon goHell = new ImageIcon("C:/Users/GLORY/git/jb/img/gohell.png");
				JLabel goHellLabel = new JLabel(goHell);
				goHellLabel.setBounds(0, 0, 550, 750);
				
				/* 버튼 */
				JButton btn3 = new JButton();
				btn3.setLocation(17,10);
				btn3.setSize(517, 731);
				btn3.setOpaque(false); //투명하게
				btn3.setContentAreaFilled(false); //내용 채우기 안함
				
				/* 패널에 버튼과 라벨 이미지 넣기 */
				goHellPanel.add(btn3);
				goHellPanel.add(goHellLabel);
				
				/* 다이얼로그 생성 */
				Dialog goHellDialog = new Dialog(f, "goHell");
				goHellDialog.setBounds(650, 100, 565, 800);
				goHellDialog.add(goHellPanel);
				goHellDialog.setVisible(true);
				
				/* 버튼 이벤트*/
				btn3.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						goHellDialog.setVisible(false);
						goHellDialog.dispose();
					}
				});
				break;
			case 4 :
				/* 패널 */
				JPanel landPanel = new JPanel();
				landPanel.setLayout(null);
				
				/* 빈땅으로 가는 찬스카드 이미지 */
				ImageIcon land = new ImageIcon("C:/Users/GLORY/git/jb/img/bin.png");
				JLabel landLabel = new JLabel(land);
				landLabel.setBounds(0, 0, 550, 750);
				
				/* 버튼 */
				JButton btn4 = new JButton();
				btn4.setLocation(17,10);
				btn4.setSize(517, 731);
				btn4.setOpaque(false); //투명하게
				btn4.setContentAreaFilled(false); //내용 채우기 안함
				
				/* 패널에 라벨 버튼 넣기*/
				landPanel.add(btn4);
				landPanel.add(landLabel);
				
				/* 다이얼로그 생성*/
				Dialog landDialog = new Dialog(f, "Land");
				landDialog.setBounds(650, 100, 565, 800);
				landDialog.add(landPanel);
				landDialog.setVisible(true);
				
				/* 버튼 이벤트*/
				btn4.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						landDialog.setVisible(false);
						landDialog.dispose();
					}
				});
				break;
			case 5 :
				/* 패널 */
				JPanel dialogReChancePanel = new JPanel();
				dialogReChancePanel.setLayout(null);
				
				/* 문제풀이 재도전 이미지 카드 */
				ImageIcon reChance = new ImageIcon("C:/Users/GLORY/git/jb/img/rechance.png");
				JLabel reChanceLabel = new JLabel(reChance);
				reChanceLabel.setBounds(0, 0, 550, 750);
				
				/* 버튼 */
				JButton btn5 = new JButton();
				btn5.setLocation(17,10);
				btn5.setSize(517, 731);
				btn5.setOpaque(false); //투명하게
				btn5.setContentAreaFilled(false); //내용 채우기 안함
			
				/* 패널에 라벨 버튼 넣기*/
				dialogReChancePanel.add(btn5);
				dialogReChancePanel.add(reChanceLabel);
			
				/* 다이얼로그 생성*/
				Dialog reChacneDialog = new Dialog(f, "reChance");
				reChacneDialog.setBounds(650, 100, 565, 800);
				reChacneDialog.add(dialogReChancePanel);
				reChacneDialog.setVisible(true);
			
				btn5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						reChacneDialog.setVisible(false);
						reChacneDialog.dispose();
					}
				});
					
				break;
		}		
	}
	
	public static void main(String[] args) {
		new goToChance();
		
	}
}
