package com.greedy.jb.member.views;

import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.jb.member.controller.comsetcontroller.ComputerSettingController;
import com.greedy.jb.member.model.dto.CharacterDTO;
import com.greedy.jb.member.model.dto.GameResultDTO;
import com.greedy.jb.member.model.dto.InventoryDTO;
import com.greedy.jb.member.model.dto.UserDTO;

public class GameWindow {

	/** 배경이미지가 될 ImageIcon 이다. */
	private ImageIcon mainImage;
	
	/** 상태창에 나타낼 플레이어의 이름을 담을 라벨이다. */
	private JLabel player1Name;
	private JLabel player2Name;
	private JLabel player3Name;
	private JLabel player4Name;
	
	/** 남은 주사위 수 나타낼 라벨이다. */
	private JLabel remainedNumberOfPlayer1;
	private JLabel remainedNumberOfPlayer2;
	private JLabel remainedNumberOfPlayer3;
	private JLabel remainedNumberOfPlayer4;
	
	/** 다른 여러 패널, 버튼, 이미지 등을 ZOrder를 사용해 하나의 패널로 저장하기 위한 변수이다. */
	private JPanel contentPane;
	
	/** 주사위 순서를 랜덤하게 뽑고 그걸 내림차순으로 정렬해서 확인하기 위한 변수이다. */
	private Integer[] playerStartOrder;
	private Integer[] playerReverseStartOrder;
	
	/** 순서뽑기 결과를 보여주기 위한 라벨이다. */
    private JLabel orderLabelFirst;
    private JLabel orderLabelSecond;
    private JLabel orderLabelThird;
    private JLabel orderLabelFourth;
    
    /** 정해진 순서대로의 캐릭터 정보를 담는 라벨 배열이다.*/
    private JLabel[] orderCharacter;
    
    /** 주사위 이미지를 담을 라벨이다. */
    private JLabel diceRolled;
    
    /** 캐릭터를 움직이기 위해 사용하는 메소드를 사용하기 위한 MoveCharacter자로형 변수이다. */
    private MoveCharacter movingClass;
    
    /** 몇 번째 플레이어가 주사위를 굴릴 차례인지를 나타내는 변수이다. */
    private int moveCharacter;
    
    /** 컴퓨터의 캐릭터 이름과 캐릭터 번호를 데이터베이스에서 가져오기 위한 controller이다. */
    private final ComputerSettingController comController;
    
    /** 플레이어 닉네임과 순서를 매칭 시키기 위한 Map 변수이다. */
    private Map<String, Integer> playerOrderMap;
   
    /**
	 * @author 박인근
	 * @version 1.0.0
     * @param mf 메인 프레임
     * @param UserDTO 사용자의 정보를 담은 DTO
     * @param GameResultDTO 인원 수에 대한 정보를 담은 DTO
     */
	public GameWindow(MainFrame mf, UserDTO UserDTO, GameResultDTO GameResultDTO) {
		
		comController = new ComputerSettingController();
		
		/* 
		 * 배경 이미지가 될 패널인 mainPanel에 이미지를 그리기 위해 Graphics의 drawImage
		 * 메소드를 사용하여 해당 이미지와 좌표, 크기를 정해 패널을 설정하였다.
		 * 좌표와 크기는 프레임과 같게 한다.
		 */
		mainImage = new ImageIcon("img/main-map.png");
		
		JPanel mainPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(mainImage.getImage(), 0, 0, 1440, 900, null);
			}
		};

		mainPanel.setBounds(0, 0, 1440, 900);

		/* 
		 * 자식 컴포넌트들을 담을 패널인 contentPane을 초기화한다.
		 * 자식 컴포넌트들이 겹치지 않을 경우를 보장하지 못하기 때문에
		 * isOptimizedDrawingEnabled의 값을 false를 반환하도록 한다.
		 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		
		/* 
		 * 컴퓨터의 캐릭터를 설정하기 위해 플레이어수 - 1 만큼의 DTO배열을 생성하고,
		 * comRandom 메소드를 이용하여 1 ~ 10까지의 무작위 난수를 3개가 담긴 List를 반환받는다. 
		 */
		CharacterDTO[] comChar = new CharacterDTO[GameResultDTO.getPlayerHead() - 1];
	    List comCharacter = comRandom(GameResultDTO.getPlayerHead(), UserDTO.getCharNo());
	      
	    for(int i = 0; i < comCharacter.size(); i++) {
	       comChar[i] = comController.setComCharacter((int) comCharacter.get(i));
	    }
	    
	    /* 순서뽑기버튼 이미지와 결과 이미지 */
	    ImageIcon orderButtonImage = new ImageIcon("img/OrderButton.png");
	    Image orderBtnImg = orderButtonImage.getImage().getScaledInstance(300, 300, Image.SCALE_SMOOTH);
	    ImageIcon orderButtonImg = new ImageIcon(orderBtnImg);
	      
	    ImageIcon resultOrderImgIcon = new ImageIcon("img/PlayerOrder.png");
	    Image resultOrderImg = resultOrderImgIcon.getImage().getScaledInstance(850, 420, Image.SCALE_SMOOTH);
	    ImageIcon resultOrderImgIcon2 = new ImageIcon(resultOrderImg);
	    
	    /* 결과 이미지 라벨 */
	    JLabel resultOrder = new JLabel(resultOrderImgIcon2);
	    JPanel resultOrderPanel = new JPanel();
	    resultOrderPanel.add(resultOrder);

	    /* 순서뽑기 버튼 */
	    JButton orderButton = new JButton(orderButtonImg);
	    orderButton.setBounds(600, 300, 300, 300);
	     
	    /* 순서 뽑기 버튼 배경 제거 */
	    orderButton.setBorderPainted(false);
	    orderButton.setFocusPainted(false);
	    orderButton.setContentAreaFilled(false);

	    /* 순서 뽑기 결과 이미지를 버튼에 설정 및 배경지우기 */
	    ImageIcon resultOrderConfirmImgIcon = new ImageIcon("img/confirm_image.png");
	    Image resultOrderConfirmImg = resultOrderConfirmImgIcon.getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH);
	    ImageIcon resultOrderConfirmImgIcon2 = new ImageIcon(resultOrderConfirmImg);
	      
	    JButton resultOrderConfirmButton = new JButton(resultOrderConfirmImgIcon2);
	    resultOrderConfirmButton.setBounds(350, 280, 150, 90);
	      
	    resultOrderConfirmButton.setBorderPainted(false);
	    resultOrderConfirmButton.setFocusPainted(false);
	    resultOrderConfirmButton.setContentAreaFilled(false);
	    
	    /* 뽑기 결과를 저장할 정수형 배열과 순서대로 정렬할 배열 */
	    playerStartOrder = new Integer[GameResultDTO.getPlayerHead()];
	    playerReverseStartOrder = new Integer[GameResultDTO.getPlayerHead()];
	      
	    JDialog orderDialog = new JDialog(mf,"StartOrder",ModalityType.MODELESS);
	    orderDialog.setBounds(600, 300, 300, 300);

	    /* 캐릭터 페이지에서 게임 화면 패널로 넘어오면 바로 순서뽑기 dialog창 실행 */
	    orderDialog.add(orderButton);
	    orderDialog.setVisible(true);
	      
	    int x = 350;
	    int y = 150;
	      
	    /* 순서 뽑기 결과 라벨에 출력 */
	    JLabel confirmResult = new JLabel("이번 게임의 순서는?!");
	    confirmResult.setBounds(330, 120, 400, 20);
	    confirmResult.setFont(new Font("HY견고딕", Font.BOLD, 20));	      

	    /* 순서 뽑기 결과와 플레이어를 저장해놓을 Map */
	    playerOrderMap = new HashMap<>();
	      
	      
	    /* 1 ~ 100까지 발생한 난수를 저장 */ 
	    for(int i = 0; i < playerStartOrder.length; i++) {
			   
	    		playerStartOrder[i] = (int) getStartOrder(playerStartOrder.length).get(i);
				playerReverseStartOrder[i] = playerStartOrder[i];
		}
			
	    /* 내림차순으로 정렬 */
		Arrays.sort(playerReverseStartOrder, Collections.reverseOrder());
			
			
		/* 뽑기 결과와 정렬한 결과를 비교해 순서를 확인하고 화면에 출력 및 Map에 저장 */
		for(int i = 0; i < playerStartOrder.length; i++) {
			for(int j = 0; j < playerStartOrder.length; j++) {
					
				if(playerStartOrder[i] == playerReverseStartOrder[j]) {
						
					if(i == 0) {
							
						switch(j) {
							case 0:
								orderLabelFirst = new JLabel("1등 : " + UserDTO.getPlayId() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelFirst.setBounds(x, y, 400, 20);
								orderLabelFirst.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(UserDTO.getPlayId(), 0);
								break;
							case 1:
								orderLabelSecond = new JLabel("2등 : " + UserDTO.getPlayId() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelSecond.setBounds(x, y + 30, 400, 20);
								orderLabelSecond.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(UserDTO.getPlayId(), 1);
								break;
							case 2:
								orderLabelThird = new JLabel("3등 : " + UserDTO.getPlayId() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelThird.setBounds(x, y + 60, 400, 20);
								orderLabelThird.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(UserDTO.getPlayId(), 2);
								break;
							case 3:
								orderLabelFourth = new JLabel("4등 : " + UserDTO.getPlayId() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelFourth.setBounds(x, y + 90, 400, 20);
								orderLabelFourth.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(UserDTO.getPlayId(), 3);
								break;
						}
						
					}
					else {
						switch(j) {
							case 0:
								orderLabelFirst = new JLabel("1등 : " + comChar[i-1].getCharName() +" (" + playerReverseStartOrder[j] + ")");
								orderLabelFirst.setBounds(x, y, 400, 20);
								orderLabelFirst.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(comChar[i-1].getCharName(), 0);
								break;
							case 1:
								orderLabelSecond = new JLabel("2등 : " + comChar[i-1].getCharName() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelSecond.setBounds(x, y + 30, 400, 20);
								orderLabelSecond.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(comChar[i-1].getCharName(), 1);
								break;
							case 2:
								orderLabelThird = new JLabel("3등 : " + comChar[i-1].getCharName() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelThird.setBounds(x, y + 60, 400, 20);
								orderLabelThird.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(comChar[i-1].getCharName(), 2);
								break;
							case 3:
								orderLabelFourth = new JLabel("4등 : " + comChar[i-1].getCharName() + " (" + playerReverseStartOrder[j] + ")");
								orderLabelFourth.setBounds(x, y + 90, 400, 20);
								orderLabelFourth.setFont(new Font("HY견고딕", Font.BOLD, 20));
								playerOrderMap.put(comChar[i-1].getCharName(), 3);
								break;
						}
					}
				}
					
			}
		}
			
		/* 
		 * 순서 뽑기 결과 dialog창 초기화
		 * 등수에 대한 text가 저장된 label을 추가해 화면에 보여줌 
		 */
		JDialog resultOrderDialog = new JDialog(mf,"ResultOrder",ModalityType.MODELESS);
		resultOrderDialog.setBounds(300, 200, 850, 450);
		resultOrderDialog.add(confirmResult);
		resultOrderDialog.add(orderLabelFirst);
		resultOrderDialog.add(orderLabelSecond);
		if(GameResultDTO.getPlayerHead() == 3) {
			resultOrderDialog.add(orderLabelThird);
		} else if(GameResultDTO.getPlayerHead() == 4) {
			resultOrderDialog.add(orderLabelThird);
			resultOrderDialog.add(orderLabelFourth);
		}
		resultOrderDialog.add(resultOrderConfirmButton);
		resultOrderDialog.add(resultOrderPanel);
		
		/* 순서 뽑기 버튼을 누르게 되면 순서 뽑기 dialog창 꺼지고 순서 뽑기 결과 dialog창 켜짐 */ 
		orderButton.addActionListener(new ActionListener() {
		   	  
			@Override
		    public void actionPerformed(ActionEvent e) {
		            
				orderDialog.dispose();
				resultOrderDialog.setVisible(true);
		    }
		});
		
		
		resultOrderConfirmButton.addActionListener(new ActionListener() {
				
			@Override
			public void actionPerformed(ActionEvent e) {
					
				resultOrderDialog.dispose();
					
			}
		});
	      
				
	    /*==================================케릭터 이동 구현=======================================*/

	    /* 캐릭터가 화면에 보이도록 라벨 생성 */
	    JLabel userPiece = new JLabel();
	    userPiece.setBounds(520, 710, 60, 80);

	    JLabel[] comPiece = new JLabel[comCharacter.size()];
	    
	    /* 사용자가 선택한 캐릭터 게임 말이 화면에 표시됨*/
	    int n1 = UserDTO.getCharNo();   

	    switch(n1) {

	    	case 1 : 
	    		ImageIcon img1 = new ImageIcon("img/JJang9.png");
	    		Image toImage1 = img1.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img11 = new ImageIcon(toImage1);
	    		userPiece.setIcon(img11);
	    		break;
	    	case 2 : 
	    		ImageIcon img2 = new ImageIcon("img/JJangA.png");
	    		Image toImage2 = img2.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img22 = new ImageIcon(toImage2);
	    		userPiece.setIcon(img22);         
	    		break;
	    	case 3 : 
	    		ImageIcon img3 = new ImageIcon("img/ActionMask.png");
	    		Image toImage3 = img3.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img33 = new ImageIcon(toImage3);
	    		userPiece.setIcon(img33);
	    		break;
	    	case 4 : 
	    		ImageIcon img4 = new ImageIcon("img/CheolSu.png");
	    		Image toImage4 = img4.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img44 = new ImageIcon(toImage4);
	    		userPiece.setIcon(img44);
	    		break;
	    	case 5 : 
	    		ImageIcon img5 = new ImageIcon("img/Yuri.png");
	    		Image toImage5 = img5.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img55 = new ImageIcon(toImage5);
	    		userPiece.setIcon(img55);
	    		break;
	    	case 6 : 
	    		ImageIcon img6 = new ImageIcon("img/WhiteDog.png");
	    		Image toImage6 = img6.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img66 = new ImageIcon(toImage6);
	    		userPiece.setIcon(img66);
	    		break;
	    	case 7 : 
	    		ImageIcon img7 = new ImageIcon("img/Boss.png");
	    		Image toImage7 = img7.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img77 = new ImageIcon(toImage7);
	    		userPiece.setIcon(img77);
	    		break;
	    	case 8 : 
	    		ImageIcon img8 = new ImageIcon("img/BuriBuri.png");
	    		Image toImage8 = img8.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img88 = new ImageIcon(toImage8);
	    		userPiece.setIcon(img88);
	    		break;
	    	case 9 : 
	    		ImageIcon img9 = new ImageIcon("img/Maeng9.png");
	    		Image toImage9 = img9.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img99 = new ImageIcon(toImage9);         
	    		userPiece.setIcon(img99);
	    		break;
	    	case 10 : 
	    		ImageIcon img10 = new ImageIcon("img/Huni.png");
	    		Image toImage10 = img10.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	    		ImageIcon img1010 = new ImageIcon(toImage10);
	    		userPiece.setIcon(img1010);
	    		break;      
	    }

	    /* 컴퓨터에 할당된 캐릭터 게임 말이 화면에 표시됨*/
	    for(int i = 0; i < comPiece.length; i++) {

	       comPiece[i] = new JLabel();

	       switch((int) comCharacter.get(i)) {
	       		case 1 : 
	       			ImageIcon img1 = new ImageIcon("img/JJang9.png");
	       			Image toImage1 = img1.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img11 = new ImageIcon(toImage1);
	       			comPiece[i].setIcon(img11);
	       			break;
	       		case 2 : 
	       			ImageIcon img2 = new ImageIcon("img/JJangA.png");
	       			Image toImage2 = img2.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img22 = new ImageIcon(toImage2);
	       			comPiece[i].setIcon(img22);         
	       			break;
	       		case 3 : 
	       			ImageIcon img3 = new ImageIcon("img/ActionMask.png");
	       			Image toImage3 = img3.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img33 = new ImageIcon(toImage3);
	       			comPiece[i].setIcon(img33);
	       			break;
	       		case 4 : 
	       			ImageIcon img4 = new ImageIcon("img/CheolSu.png");
	       			Image toImage4 = img4.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img44 = new ImageIcon(toImage4);
	       			comPiece[i].setIcon(img44);
	       			break;
	       		case 5 : 
	       			ImageIcon img5 = new ImageIcon("img/Yuri.png");
	       			Image toImage5 = img5.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img55 = new ImageIcon(toImage5);
	       			comPiece[i].setIcon(img55);
	       			break;
	       		case 6 : 
	       			ImageIcon img6 = new ImageIcon("img/WhiteDog.png");
	       			Image toImage6 = img6.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img66 = new ImageIcon(toImage6);
	       			comPiece[i].setIcon(img66);
	       			break;
	       		case 7 : 
	       			ImageIcon img7 = new ImageIcon("img/Boss.png");
	            	Image toImage7 = img7.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	            	ImageIcon img77 = new ImageIcon(toImage7);
	            	comPiece[i].setIcon(img77);
	            	break;
	       		case 8 : 
	       			ImageIcon img8 = new ImageIcon("img/BuriBuri.png");
	       			Image toImage8 = img8.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img88 = new ImageIcon(toImage8);
	       			comPiece[i].setIcon(img88);
	       			break;
	       		case 9 : 
	       			ImageIcon img9 = new ImageIcon("img/Maeng9.png");
	       			Image toImage9 = img9.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img99 = new ImageIcon(toImage9);         
	       			comPiece[i].setIcon(img99);
	       			break;
	       		case 10 : 
	       			ImageIcon img10 = new ImageIcon("img/Huni.png");
	       			Image toImage10 = img10.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH);
	       			ImageIcon img1010 = new ImageIcon(toImage10);
	       			comPiece[i].setIcon(img1010);
	       			break;
	       }

	    }


	    int comPieceX = 570;
	    int comPieceY = 710;


	    for(int i = 0; i < comPiece.length; i++) {

	    	if(i == 0) {
	    		comPiece[i].setBounds(comPieceX, comPieceY, 60, 80);
	    	} else if(i == 1) {
	    		comPiece[i].setBounds(comPieceX - 50, comPieceY + 80, 60, 80);
	    	} else {
	    		comPiece[i].setBounds(comPieceX, comPieceY + 80, 60, 80);

	    	}
	    }
	    
	    int[] moveX = new int[GameResultDTO.getPlayerHead()];
	    int[] moveY = new int[GameResultDTO.getPlayerHead()];
	    
	    for(int i = 0; i < GameResultDTO.getPlayerHead(); i++) {
	    	
	    	if(i == 0) {
	    		moveX[i] = userPiece.getX();
	    		moveY[i] = userPiece.getY();
	    	} else {
	    		moveX[i] = comPiece[i - 1].getX();
	    		moveY[i] = comPiece[i - 1].getY();
	    	}
	    	
	    }

		contentPane.add(mainPanel);
		contentPane.setLayout(null);
		contentPane.setComponentZOrder(mainPanel, 0);
		
		player1Name = new JLabel(UserDTO.getPlayId());
		player1Name.setFont(new Font("HY견고딕", Font.BOLD, 40));
		player2Name = new JLabel(comChar[0].getCharName());
		player2Name.setFont(new Font("HY견고딕", Font.BOLD, 40));
		
		if(GameResultDTO.getPlayerHead() == 3) {
			player3Name = new JLabel(comChar[1].getCharName());			
			player3Name.setFont(new Font("HY견고딕", Font.BOLD, 40));
			player3Name.setBounds(126, 447, 250, 50);
			contentPane.setComponentZOrder(player3Name, 0);
		} else if(GameResultDTO.getPlayerHead() == 4) {
			player3Name = new JLabel(comChar[1].getCharName());			
			player3Name.setFont(new Font("HY견고딕", Font.BOLD, 40));
			player4Name = new JLabel(comChar[2].getCharName());
			player4Name.setFont(new Font("HY견고딕", Font.BOLD, 40));
			player3Name.setBounds(126, 447, 250, 50);
			player4Name.setBounds(126, 648, 250, 50);
			contentPane.setComponentZOrder(player3Name, 0);
			contentPane.setComponentZOrder(player4Name, 0);
		}
		
		
		remainedNumberOfPlayer1 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer1.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer2 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer2.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer3 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer3.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer4 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer4.setFont(new Font("HY견고딕", Font.BOLD, 20));
		
		
		player1Name.setBounds(126, 48, 250, 50);
		player2Name.setBounds(126, 251, 250, 50);
		
		
		remainedNumberOfPlayer1.setBounds(126, 112, 250, 35);
		remainedNumberOfPlayer2.setBounds(126, 309, 250, 35);
		remainedNumberOfPlayer3.setBounds(126, 511, 250, 35);
		remainedNumberOfPlayer4.setBounds(126, 713, 250, 35);
		
		contentPane.setComponentZOrder(player1Name, 0);
		contentPane.setComponentZOrder(player2Name, 0);

		contentPane.setComponentZOrder(remainedNumberOfPlayer1, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer2, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer3, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer4, 0);
		
		orderCharacter = new JLabel[GameResultDTO.getPlayerHead()];
		
		for(int i = 0; i < orderCharacter.length; i++) {
			for(int j = 0; j < orderCharacter.length; j++) {
				if(i == 0) {
					orderCharacter[playerOrderMap.get(UserDTO.getPlayId())] =  userPiece;
					System.out.println();
				} else {
					orderCharacter[playerOrderMap.get(comChar[i-1].getCharName())] = comPiece[i-1];
				}
				
			}
			
		}
		
		diceRolled = new JLabel();
	       
    	ImageIcon d = new ImageIcon("img/dice1.jpg");
    	Image dToImage = d.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
    	ImageIcon diceIcon = new ImageIcon(dToImage);
       
    	diceRolled.setIcon(diceIcon);
       
    	diceRolled.setBounds(764, 492, 120, 120);
    	
    	orderCharacter[0].setBounds(525, 715, 60, 80);
		
    	for(int i = 1; i < orderCharacter.length; i++) {

 	       if(i == 1) {
 	    	  orderCharacter[i].setBounds(comPieceX, comPieceY + 5, 60, 80);
 	       } else if(i == 2) {
 	    	  orderCharacter[i].setBounds(comPieceX - 45, comPieceY + 75, 60, 80);
 	       } else {
 	    	  orderCharacter[i].setBounds(comPieceX, comPieceY + 75, 60, 80);
 	       }
 	    }
    	
		
		for(int i = 0; i < orderCharacter.length; i++) {
			contentPane.add(orderCharacter[i]);
		    contentPane.setComponentZOrder(orderCharacter[i], 0);   
		}
		
		ImageIcon godiceImage = new ImageIcon("img/diceGo.png");
	    Image godiceImg = godiceImage.getImage().getScaledInstance(120, 100, Image.SCALE_SMOOTH);
	    ImageIcon godiceImageIcon = new ImageIcon(godiceImg);
	      
	    JButton goDiceButton = new JButton(godiceImageIcon);
	    goDiceButton.setBounds(980, 530, 120, 100);
	    goDiceButton.setBorderPainted(false);
	    goDiceButton.setFocusPainted(false);
	    goDiceButton.setContentAreaFilled(false);
		
	    contentPane.add(goDiceButton);
	    contentPane.setComponentZOrder(goDiceButton, 0); 
	    contentPane.add(diceRolled);
	    contentPane.setComponentZOrder(diceRolled, 0);
	    
		JButton nextBtn = new JButton("다음");
		nextBtn.setBounds(1330, 750, 100, 50);
	    
	    contentPane.add(nextBtn);
	    contentPane.setComponentZOrder(nextBtn, 0);
	    
	    nextBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				PanelSwitch.changePanel(mf, contentPane, new QuizPage().getPanel(mf));
				
			}
		});
	    
	    int charX = 60;
	    int charY = 80;
	    
	    moveCharacter = 0;
	    
	    movingClass = new MoveCharacter();
	    
	    /* 주사위 굴리기, 좌표 값에 따라 방향이 정해지고 메소드가 실행된다. */
	    goDiceButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				int dice = goDice();
				
				if(GameResultDTO.getPlayerHead() == 2) {
					switch(moveCharacter) {
						case 0:
							if(orderCharacter[0].getX() <= 1220 && orderCharacter[0].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[0]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[0]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 1:
							if(orderCharacter[1].getX() <= 1260 && orderCharacter[1].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[1]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[1]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
					}
					
					if(moveCharacter == 1) {
						moveCharacter = 0;
					} else {
						moveCharacter++;
					}
				}
				
				if(GameResultDTO.getPlayerHead() == 3) {
					switch(moveCharacter) {
						case 0:
							if(orderCharacter[0].getX() <= 1220 && orderCharacter[0].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[0]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[0]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 1:
							if(orderCharacter[1].getX() <= 1260 && orderCharacter[1].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[1]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[1]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 2:
							if(orderCharacter[2].getX() <= 1220 && orderCharacter[2].getY() <= 780) {
								movingClass.movingUpCharacter(dice, orderCharacter[2]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[2]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
					}
					if(moveCharacter == 2) {
						moveCharacter = 0;
					} else {
						moveCharacter++;
					}
				}
				
				if(GameResultDTO.getPlayerHead() == 4) {
					switch(moveCharacter) {
						case 0:
							if(orderCharacter[0].getX() <= 1220 && orderCharacter[0].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[0]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[0]);
							}
							
							
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 1:
							if(orderCharacter[1].getX() <= 1260 && orderCharacter[1].getY() <= 710) {
								movingClass.movingUpCharacter(dice, orderCharacter[1]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[1]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 2:
							if(orderCharacter[2].getX() <= 1220 && orderCharacter[2].getY() <= 780) {
								movingClass.movingUpCharacter(dice, orderCharacter[2]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[2]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
						case 3:
							if(orderCharacter[3].getX() <= 1260 && orderCharacter[3].getY() <= 780) {
								movingClass.movingUpCharacter(dice, orderCharacter[3]);
							} else {
								movingClass.movingDownCharacter(dice, orderCharacter[3]);
							}
							diceRolled = movingClass.getDiceRolled(dice, diceRolled);
							break;
					}
					
					if(moveCharacter == 3) {
						moveCharacter = 0;
					} else {
						moveCharacter++;
					}
				}
				
			}
		});
	    
		
		
		remainedNumberOfPlayer1 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer1.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer2 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer2.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer3 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer3.setFont(new Font("HY견고딕", Font.BOLD, 20));
		remainedNumberOfPlayer4 = new JLabel("남은 주사위 횟수 : " + "번");
		remainedNumberOfPlayer4.setFont(new Font("HY견고딕", Font.BOLD, 20));

		
		player1Name.setBounds(126, 48, 250, 50);
		player2Name.setBounds(126, 251, 250, 50);
		
		
		remainedNumberOfPlayer1.setBounds(126, 112, 250, 35);
		remainedNumberOfPlayer2.setBounds(126, 309, 250, 35);
		remainedNumberOfPlayer3.setBounds(126, 511, 250, 35);
		remainedNumberOfPlayer4.setBounds(126, 713, 250, 35);
		
		
		contentPane.setComponentZOrder(player1Name, 0);
		contentPane.setComponentZOrder(player2Name, 0);

		contentPane.setComponentZOrder(remainedNumberOfPlayer1, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer2, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer3, 0);
		contentPane.setComponentZOrder(remainedNumberOfPlayer4, 0);
		
		
		mf.add(contentPane);
		
		
		
	}
	
	public JPanel getGameWindowPanel() {
		
		return contentPane;
	}
	
	/**
	 * 
	 * @param playerHead 플레이어 수
	 * @return comCharacter 플레이어 수 만큼 생성한 난수 set
	 */
	public List comRandom(int playerHead) {
		Set<Integer> comCharSet = new TreeSet();

		while(comCharSet.size() < playerHead - 1) {
		   comCharSet.add((int) (Math.random() * 10) + 1);
		}
		
		List comCharacter = new ArrayList<>(comCharSet);
		
		return comCharacter;
	}
	
	/**
	 * 
	 * @param playerHead 인원수
	 * @return 순서 list
	 */
	public List getStartOrder(int playerHead) {
	      
	      Set<Integer> startOrder = new TreeSet<>();
	      
	      int ran = 0;
	      
	      while(startOrder.size() < playerHead) {
	    	  ran = (int) (Math.random() * 100) + 1;
	    	  startOrder.add((int) (Math.random() * 100) + 1);
	      }
	      
	      List startOrderList = new ArrayList<>(startOrder);
	      
	      return startOrderList;
	   }
	
	/**
	 * 
	 * @param playerHead 플레이어 수
	 * @param userCharacterNo 캐릭터 번호
	 * @return 플레이어 수 만큼의 랜덤한 난수 반환
	 * @exception 예외 이유에 대한 설명
	 */
   public List comRandom(int playerHead, int userCharacterNo) {
         Set<Integer> comCharSet = new TreeSet<>();

         int ran = 0;
         
         while(comCharSet.size() < playerHead - 1) {
            ran = (int) (Math.random() * 10) + 1;
            
            if(userCharacterNo == ran) {
               ran = (int) (Math.random() * 10) + 1;               
            }
            
            comCharSet.add(ran);
            
         }
         
         List comCharacter = new ArrayList<>(comCharSet);
         
         return comCharacter; 
   }
   
   /**
    * @return ranDice 주사위 값
    */
   public int goDice() {
	   
	   int ranDice = (int) (Math.random() * 6) + 1;
   
	   return ranDice;
	   
   }
	   
}
