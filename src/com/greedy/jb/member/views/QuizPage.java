package com.greedy.jb.member.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.greedy.jb.member.controller.quizcontroller.QuizController;
import com.greedy.jb.member.model.dao.QuizDAO;
import com.greedy.jb.member.model.dto.ChanceCardDTO;
import com.greedy.jb.member.model.dto.QuizDTO;
import com.greedy.jb.member.model.service.QuizService;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

/**
 * <pre>
 * Class : QuizPage
 * Comment : view용 어플리케이션(화면 구성 요소)
 * History
 * 2021/12/15 석원탁 작성
 * 2021/12/17 정답 제출 버튼 추가
 * 2021/12/19 난이도 랜덤 설정 if문 추가 
 * </pre>
 * @version 1
 * @author 석원탁
 */


class ImagePanel extends JPanel{
	
	/* 배경사진과 이미지를 사용하기위해 생성 */
	private Image img;

	public ImagePanel(Image img) {
		this.img = img;
		setSize(new Dimension(img.getWidth(null),img.getHeight(null)));
		setPreferredSize(new Dimension(img.getWidth(null),img.getHeight(null)));
		setLayout(null);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, null);
		
		
	}
	
}


public class QuizPage {

	/* 화면을 구성하는 요소들 */
	
	private Properties prop;
	private QuizService quizService;
	private QuizDTO quizDTO;
	private List<ChanceCardDTO> clist;
	
	public JPanel getPanel(MainFrame mf) {
		
		quizService = new QuizService();
		JFrame frame = new JFrame("QuizPage");
		frame.setBounds(0, 0, 1440, 900);
		frame.setLocationRelativeTo(null);
		
		quizDTO = new QuizDTO();
		

		ImagePanel panel = new ImagePanel(new ImageIcon("./img/QuizPage.png").getImage());
		panel.setBounds(0, 0, 1440, 900);														// 판넬 사이즈
		
		JTextArea textArea = new JTextArea(100, 300);											// 문제 내용 출제지
		
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new BorderLayout());
		textPanel.setBounds(250, 200, 1000, 400);
		textPanel.add(textArea);
		
		JTextField tf = new JTextField();														// 답안 제출지
		tf.setBounds(600, 605, 200, 30);
		tf.setFont(new Font("돋움", Font.BOLD, 23));												// 답안 입력지 글씨체
		
		ImageIcon btnImage = new ImageIcon("./img/GoAnswer.png");
		Image setBtnImage = btnImage.getImage().getScaledInstance(220, 110, Image.SCALE_SMOOTH);
		ImageIcon btnImage2 = new ImageIcon(setBtnImage);
		JButton bt = new JButton(btnImage2);
		
		bt.setBounds(575, 650, 250, 100);														// 정답제출 버튼
		bt = deleteBackgroundBtn(bt);															// 버튼의 배경 삭제

		/* 어떤 난이도의 문제를 뽑아서 실행하고 출력할지 결정하는 기능 추가*/
		int random = (int) (Math.random() * 3) + 1;

		String level = "";																		// level을 선언 해주고 

		if(random == 1){																		// random으로 [상, 중, 하 ] 불러오기 위한 if문..
		   level = "HARD";
		} else if(random == 2){
		   level = "NORMAL";
		} else {
		   level = "EASY";
		}
		
		quizDTO = new QuizController().selectQuiz(quizDTO, level);								// 랜덤으로 문제 출력하기위해 만들어줌!
		
		textArea.append(quizDTO.getQuizType() + " \n " + "\n" + quizDTO.getQuizQuestion());		// 문제유형과 문제내용 출력!!
		textArea.setFont(new Font("궁서", Font.BOLD, 25));										// 문제내용판의 글씨체,크기
		textArea.setLineWrap(true);																// 문제내용판 textArea 한줄로 쭉 써지는거 끊어주는것!
		
		panel.add(bt);																			// 정답 제출 버튼 추가  
		panel.add(tf);																			// 정답 입력 칸 추가
		
		/* 정답 제출 클릭 시 정답, 오답 구분 역할  */
		bt.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			
				String text = tf.getText();														// 입력받은 정답란! tf에 정답 입력해서 text로 출력해준다.
				
				System.out.println(text);														// 출력받을때 아예 대문자로 전환해줬더니 값이 틀리다고 나온다.
				System.out.println(quizDTO.getAnswer());										// 입력한 text자체를 비교하게 되기 때문에 equals문에 toUpperCase 넣어주기
				
				if(text.toUpperCase().equals(quizDTO.getAnswer())) {							// 정답들 모두 대문자로 해놔서 .toUpperCase() 사용해서 정답과 비교해준다.
					System.out.println("정답입니다.");
				} else {
					System.out.println("틀렸습니다.");
				}
			}
			
		});
		
		
		JPanel contentPane = new JPanel() {
			
			/* 화면에 버튼 등 패널을 띄우기 위한 기능과
			 * view의 다음 화면으로 넘겨주기 위한 기능 */
			public boolean isOptimizedDrawingEnabled() {
		
				return false;
			}
		};
		
		JButton nextBtn = new JButton("다음");
		nextBtn.setBounds(1330, 750, 100, 50);
		contentPane.setLayout(null);
		contentPane.add(panel);
		contentPane.add(textPanel);
	    contentPane.add(nextBtn);
		contentPane.setComponentZOrder(panel, 1);
		contentPane.setComponentZOrder(nextBtn, 0);
		contentPane.setComponentZOrder(textPanel, 0);
		
		return contentPane;
		
	}
	
	
	public static JButton deleteBackgroundBtn(JButton bt) {
		
		bt.setBorderPainted(false);
		bt.setFocusPainted(false);
		bt.setContentAreaFilled(false);
		
		return bt;
	}
	
	
	
	
}