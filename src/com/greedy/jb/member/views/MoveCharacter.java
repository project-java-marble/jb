package com.greedy.jb.member.views;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * 
 * <pre>
 * Class : MoveCharacter
 * Comment : 캐릭터를 이동시키기 위한 메소드가 있는 클래스
 * History
 * 2021/12/19 (박인근) 처음 작성함
 * </pre>
 * @author 박인근
 * @version 1.0.0
 * @see GameWindow
 *
 */
public class MoveCharacter {
	
	/**
	 * 
	 * @param dice 랜덤한 주사위 결과 값
	 * @param moving 라벨을 좌표값대로 이동 한 후 기록하기 위한 라벨
	 */
	public void movingUpCharacter(int dice, JLabel moving) {

		int x = moving.getX();
		int y = moving.getY();
		
		int width = 60;
		int height = 80;

		/* 
		 * 캐릭터 라벨의 좌표가 y축으로 -100만큼 위로 이동하는 반복문
		 * (y - 100)좌표가 10보다 작아질 경우 오른쪽으로 가게 함
		 * */
		for(int i = 0; i < dice; i++) {
			
			if((y - 100) < 10) {
				movingRightCharacter(dice - i, moving);
				break;
			} else {
				y -= 100;
				moving.setBounds(x, y, width, height);
			}
			
		}
		
		
	}
	
	/**
	 * 
	 * @param dice 랜덤한 주사위 결과 값
	 * @param moving 라벨을 좌표값대로 이동 한 후 기록하기 위한 라벨
	 */
	public void movingRightCharacter(int dice, JLabel moving) {
		
		int x = moving.getX();
		int y = moving.getY();
		
		int width = 60;
		int height = 80;
		
		/* 
		 * 캐릭터 라벨의 좌표가 x축으로 +100만큼 오른쪽으로 이동하는 반복문
		 * (x + 100)좌표가 1300보다 커질 경우 아래쪽으로 가게 함
		 * */
		for(int i = 0; i < dice; i++) {
			if((x + 100) > 1300) {
				movingDownCharacter(dice - i, moving);
				break;
			} else {
				x += 100;
				moving.setBounds(x, y, width, height);
			}
		}
		
	}
	
	/**
	 * 
	 * @param dice 랜덤한 주사위 결과 값
	 * @param moving 라벨을 좌표값대로 이동 한 후 기록하기 위한 라벨
	 */
	public void movingDownCharacter(int dice, JLabel moving) {
		
		int x = moving.getX();
		int y = moving.getY();
		
		int width = 60;
		int height = 80;
		
		/* 
		 * 캐릭터 라벨의 좌표가 y축으로 +100만큼 아래쪽으로 이동하는 반복문
		 * (y + 100)좌표가 800보다 커질 경우 왼쪽으로 가게 함
		 * */
		for(int i = 0; i < dice; i++) {
			if((y + 100) > 800) {
				movingLeftCharacter(dice - i, moving);
				break;
			} else {
				y += 100;
				moving.setBounds(x, y, width, height);
			}
		}
		
	}
	
	/**
	 * 
	 * @param dice 랜덤한 주사위 결과 값
	 * @param moving 라벨을 좌표값대로 이동 한 후 기록하기 위한 라벨
	 */
	public void movingLeftCharacter(int dice, JLabel moving) {
		
		int x = moving.getX();
		int y = moving.getY();
		
		int width = 60;
		int height = 80;
		/* 
		 * 캐릭터 라벨의 좌표가 x축으로 -100만큼 아래쪽으로 이동하는 반복문
		 * (x - 100)좌표가 800보다 커질 경우 왼쪽으로 가게 함
		 * */
		for(int i = 0; i < dice; i++) {
			if((x - 100) < 500) {
				movingUpCharacter(dice - i, moving);
				break;
			} else {
				x -= 100;
				moving.setBounds(x, y, width, height);
			}
		}
		
	}
	
	public JLabel getDiceRolled(int dice, JLabel diceRolled) {
		
		switch(dice) {
		case 1:
			ImageIcon d1 = new ImageIcon("img/dice1.jpg");
			Image d1ToImage = d1.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon1 = new ImageIcon(d1ToImage);
			diceRolled.setIcon(diceIcon1);
			break;
		case 2:
			ImageIcon d2 = new ImageIcon("img/dice2.jpg");
			Image d2ToImage = d2.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon2 = new ImageIcon(d2ToImage);
			diceRolled.setIcon(diceIcon2);
			break;
		case 3:
			ImageIcon d3 = new ImageIcon("img/dice3.jpg");
			Image d3ToImage = d3.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon3 = new ImageIcon(d3ToImage);
			diceRolled.setIcon(diceIcon3);
			break;
		case 4:
			ImageIcon d4 = new ImageIcon("img/dice4.jpg");
			Image d4ToImage = d4.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon4 = new ImageIcon(d4ToImage);
			diceRolled.setIcon(diceIcon4);
			break;
		case 5:
			ImageIcon d5 = new ImageIcon("img/dice5.jpg");
			Image d5ToImage = d5.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon5 = new ImageIcon(d5ToImage);
			diceRolled.setIcon(diceIcon5);
			break;
		case 6:
			ImageIcon d6 = new ImageIcon("img/dice6.jpg");
			Image d6ToImage = d6.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
			ImageIcon diceIcon6 = new ImageIcon(d6ToImage);
			diceRolled.setIcon(diceIcon6);
			break;
			
		}
		
		
		return diceRolled;
	}
	
}
