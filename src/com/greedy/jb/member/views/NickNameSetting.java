package com.greedy.jb.member.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import com.greedy.jb.member.controller.playersettingcontroller.PlayerSettingController;
import com.greedy.jb.member.model.dto.GameResultDTO;
import com.greedy.jb.member.model.dto.UserDTO;

/**
 * 
 * <pre>
 * class : NickNameSetting
 * Comment : 사용자의 닉네임을 입력받고 설정하는 클래스이다.
 * History
 * 2021/12/14 (박인근) 처음 작성
 * </pre>
 * @version 1.0.0
 * @author 박인근
 *  @see MainFrame, PlayerSetting, UserDAO, UserDTO
 *  
 */
public class NickNameSetting{
	
	/* 이전 패널에서 프레임을 전달받고 패널 전환을 통해 플레이어 수 선택에서 버튼 클릭시 NickNameSetting 클래스로 넘어오게 구현하였다. 
	 * ZOrder를 사용하기 위해 하나의 JPanel을 만들어서 하나의 패널로 전체적인 화면을 구현하였다.*/

	/** 프레임의 배경이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backImage;
	
	/** 이전 패널로 돌아가기 위해 사용하는 버튼의 이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backButtonImage;
	
	/** 다음 패널로 넘어가기 위해 사용하는 버튼의 이미지가 될 ImageIcon 변수이다. */
	private ImageIcon confirmImage;
	
	/** 다른 여러 패널, 버튼, 이미지 등을 ZOrder를 사용해 하나의 패널로 저장하기 위한 변수이다. */ 
	private JPanel contentPane;
	
	/** @author 박인근
	 *  @version 1.0.0
	 *  @param mf 이전 페이지에서 패널 전환을 위해 받아오는 프레임이다.
	 *  @param userDTO 사용자의 정보를 담아 놓기 위한 DTO변수이다.
	 *  @param gameReSultDTO 게임 인원 수에 대한 정보를 담아 놓기 위한 DTO변수이다.
	 * */
	public NickNameSetting(MainFrame mf, UserDTO userDTO, GameResultDTO gameResultDTO) {
		
		int x = 400;			//텍스트 필드의 기준 X좌표
		int y = 100;			//텍스트 필드의 기준 Y좌표
		
		/* 
		 * 배경이 될 이미지(backImage)와 이전 페이지로 이동할 버튼에 사용될 이미지,
		 * 다음 페이지로 이동할 버튼에 사용될 이미지 초기화 및 크기 조절 
		 */
		backImage = new ImageIcon("img/nickBackGround.PNG");
		
		backButtonImage = new ImageIcon("img/Back_image.png");
		Image backBtnImage = backButtonImage.getImage().getScaledInstance(200, 100, Image.SCALE_SMOOTH);
		ImageIcon backButtonImage2 = new ImageIcon(backBtnImage);
		
		confirmImage = new ImageIcon("img/confirm_image.png");
		Image confirmBtnImage = confirmImage.getImage().getScaledInstance(200, 100, Image.SCALE_SMOOTH);
		ImageIcon confirmImage2 = new ImageIcon(confirmBtnImage);
		
		/* 
		 * 이전 페이지로 돌아가기 버튼(backBtn)과 다음 페이지로 넘어가기 버튼(confirmBtn) 
		 * 버튼을 매개변수로 받은 이미지로 초기화하고,
		 * setBounds로 위치할 좌표와 너비, 높이를 정해주고
		 * setBorderPainted를 false로 바꿔 버튼의 외곽선을 없애준다.
		 * setFocusPainted를 false로 바꿔 버튼이 선택되었을 때 테두리를 생기지 않게 해준다.
		 * setContentAreaFilled를 false로 바꿔 내용영역 채우지 않기로 변경한다.
		 */
		JButton backBtn = new JButton(backButtonImage2);
		backBtn.setBounds(0, 0, 200, 110);
		backBtn.setBorderPainted(false);
		backBtn.setFocusPainted(false);
		backBtn.setContentAreaFilled(false);
		
		JButton confirmBtn = new JButton(confirmImage2);
		confirmBtn.setBounds(1230, 0, 200, 110);
		confirmBtn.setBorderPainted(false);
		confirmBtn.setFocusPainted(false);
		confirmBtn.setContentAreaFilled(false);
		
		/* 
		 * 배경 이미지가 될 패널인 backgroundPanel에 이미지를 그리기 위해 Graphics의 drawImage
		 * 메소드를 사용하여 해당 이미지와 좌표, 크기를 정해 패널을 설정하였다.
		 * 좌표와 크기는 프레임과 같게 한다.
		 */	
		JPanel backgoundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(backImage.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgoundPanel.setBounds(0, 0, 1440, 900);

		/*
		 * 안내 문구 라벨이다.
		 * setFont를 통해 글씨체와 굵기, 크기 등을 조절한다.
		 */
		JLabel nickOfPlayerLabel = new JLabel("닉네임을 입력하세요.");
		
		nickOfPlayerLabel.setFont(new Font("HY견고딕", Font.BOLD, 24));
		nickOfPlayerLabel.setFont(nickOfPlayerLabel.getFont().deriveFont(32.0f));
		
		/*
		 * 안내 문구 라벨을 담을 패널이다.
		 * RGB값을 통해 패널의 색을 정하고 setBounds를 통해 좌표 및 크기를 설정한다.
		 */
		JPanel textPanel = new JPanel();
		textPanel.add(nickOfPlayerLabel);
		textPanel.setBackground(new Color(216, 33, 55));
		textPanel.setBounds(x + 50, y + 220, x + 100, 50);
		
		/*
		 * 플레이어의 닉네임을 입력받을 텍스트필드와 텍스트필드를 담을 패널을 초기화한다.
		 * 패널과 텍스트필드의 배경색을 RGB를 통해 같은 값으로 설정하였고
		 * 텍스트필드의 setFont를 통한 글씨 크기 조절과 패널의 위치 조절등을 하였다.
		 */
		JTextField nickField = new JTextField(15);
		nickField.setBackground(new Color(255, 191, 191));
		nickField.setFont(nickField.getFont().deriveFont(30.0f));
		
		JPanel setNickPanel = new JPanel();
		setNickPanel.setBackground(new Color(255, 191, 191));
		setNickPanel.setBounds(x + 130, y + 450, 400, 50);
		setNickPanel.add(nickField);
		
		/* 
		 * 자식 컴포넌트들을 담을 패널인 contentPane을 초기화한다.
		 * 자식 컴포넌트들이 겹치지 않을 경우를 보장하지 못하기 때문에
		 * isOptimizedDrawingEnabled의 값을 false를 반환하도록 한다.
		 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		
		/* 
		 * contentPane은 단지 자식 컴포넌트들을 담을 패널이기 때문에 layout을 null 값을 넣어 지정해주지 않는다. 
		 * 자식 컴포넌트들을 add로 추가해주고 화면에 가장 최우선으로 보여야 될 값을 낮은 값으로 설정해준다.
		 */
		contentPane.setLayout(null);
		contentPane.add(backgoundPanel);
		contentPane.add(textPanel);
		contentPane.add(backBtn);
		contentPane.add(confirmBtn);
		contentPane.add(setNickPanel);
		contentPane.setComponentZOrder(backgoundPanel, 1);
		contentPane.setComponentZOrder(textPanel, 0);
		contentPane.setComponentZOrder(backBtn, 0);
		contentPane.setComponentZOrder(confirmBtn, 0);
		contentPane.setComponentZOrder(setNickPanel, 0);
		
		/*
		 * 이전 페이지로 돌아가기 버튼을 클릭시 발생하는 이벤트이다.
		 * 버튼을 클릭하게 되면 프레임의 패널을 변경해주는 changePanel 메소드가 실행된다.
		 * PlayerSetting의 getPanel 메소드를 사용해 panel값을 반환받아와서 패널을 변경하였다.
		 */
		backBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userDTO.setUserNo(userDTO.getUserNo() - 1);
				gameResultDTO.setPlayerHead(0);
				PanelSwitch.changePanel(mf, contentPane, new PlayerSetting(mf, userDTO, gameResultDTO).getPlayerPanel());
			}
		});
		
		/*
		 * 다음 페이지로 넘어가기 버튼을 클릭시 발생하는 이벤트이다.
		 * 버튼을 클릭하게 되면 프레임의 패널을 변경해주는 changePanel 메소드가 실행된다.
		 * CharacterSetting의 getPanel 메소드를 사용해 panel값을 반환받아와서 패널을 변경하였다.
		 * 사용자가 입력한 값을 가져와서 영어(대,소문자) 또는 한글인지 확인하고 만약 아니라면 다이얼로그창을 띄운다.
		 * 다이얼로그 창에 닉네임 입력에 관해 안내하는 이미지버튼를 띄워준다.
		 * 닉네임 입력에 성공하면 다음 페이지changePanel 메소드를 통해 현재 클래스의 패널을 다음 페이지인 CharacterSetting 클래스의 panel로 변경한다.
		 */		
		confirmBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String nickName = nickField.getText(); 
				
				int flag = 0;													//영어, 한글이 아닌 값을 입력했는지 확인하기 위한 변수
				
				/* 다이얼로그  생성 및 좌표, 크기 설정 */
				Dialog nickException = new Dialog(mf, "닉네임 입력 예외");
				
				nickException.setBounds(300, 450, 800, 300);
				
				/* 버튼 이미지화 및 다이얼로그 창에 추가 */
				ImageIcon nickNameException = new ImageIcon("img/nickNameException.png");
				Image nameException = nickNameException.getImage().getScaledInstance(800, 300, Image.SCALE_SMOOTH);
				ImageIcon nickNameException2 = new ImageIcon(nameException);
				
				JButton excepBtn = new JButton(nickNameException2);

				nickException.add(excepBtn);

				/* 입력받은 닉네임이 2글자 미만이고 10글자 초과일 경우 닉네임 입력 예외이므로 다이얼로그 창을 띄운다.  */
				if(nickName.length() < 2 || nickName.length() > 10) {
										
					nickException.setVisible(true);
					flag = 1;
					
					/* 다이얼로그 창의 이미지버튼 을 클릭시 발생하는 이벤트로 다이얼로그창을 닫는다. */
					excepBtn.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							nickException.dispose();
						}
					});
					
					
				/*
				 * 글자수에서 예외가 발생하지 않았지만 입력받은 닉네임이 한글 또는 영어가 아닌 경우를 확인한다.
				 * 글자수가 저장된 문자열을 한 글자씩 확인하고 만약 영어나 한글이 아니라면 다이얼로그창을 띄워준다.
				 */
				} else {
					for(int i = 0; i < nickName.length(); i++) {
						if((nickName.charAt(i) < 'a' || nickName.charAt(i) > 'z') && (nickName.charAt(i) < 'A' || nickName.charAt(i) > 'Z') && Character.getType(nickName.charAt(i)) != 5) {
							nickException.setVisible(true);
							
							flag = 1;
							
							/* 다이얼로그 창의 이미지버튼 을 클릭시 발생하는 이벤트로 다이얼로그창을 닫는다. */
							excepBtn.addActionListener(new ActionListener() {
								
								@Override
								public void actionPerformed(ActionEvent e) {
									nickException.dispose();
								}
							});
						} 
					}
					
				} /* 위에서 닉네임 입력 예외가 발생하지 않을 경우를 확인하고 입력받은 값을 userDTO의 setPlayId 메소드를 사용하여 설정하고 changePanel메소드를 통해 CharacterSetting 클래스의 패널로 변환한다. */
				if(flag == 0) {
					userDTO.setPlayId(nickName);
					PanelSwitch.changePanel(mf, contentPane, new CharacterSetting(mf, userDTO, gameResultDTO).getCharacterPanel());
				}
				
			}
		});
	}
	
	/**
	 * 
	 * @return contentPane 현재 클래스의 패널을 반환하는 메소드이다.
	 */
	public JPanel getNickPanel() {
		
		return contentPane;
	}
	
}
