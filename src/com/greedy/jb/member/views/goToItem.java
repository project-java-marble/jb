package com.greedy.jb.member.views;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <pre>
 * Class : goToItem
 * Comment : 아이템에 대한 팝업창 구현 클래스
 * History
 * 2021/12/21 김영광
 * </pre>
 * @version 1.0.0
 * @author GLORY
 *
 * */
public class goToItem {
    
	public goToItem() {
		
		/* 아이템상점 팝업창 구현 */
		
		JFrame f = new JFrame();
		f.setBounds(100, 100, 1440, 900);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);
		
		/* 다이얼로그에 넣을 패널생성 */
		JPanel dialogItemPanel = new JPanel();
		dialogItemPanel.setLayout(null);
		
		/* 아이템 상점 이미지 라벨에 넣기 */
		ImageIcon goToItem = new ImageIcon("C:/Users/GLORY/git/jb/img/item.png");
		JLabel ItemLabel = new JLabel(goToItem);
		ItemLabel.setBounds(0, 0, 1200, 460);
		
		/* 첫번째 구매 버튼 셋팅 */
		JButton btn = new JButton();
		btn.setLocation(245, 330);
		btn.setSize(167, 68);
		btn.setOpaque(false); //투명하게
		btn.setContentAreaFilled(false); //내용 채우기 안함
		
		/* 두번째 구매 버튼 셋팅 */
		JButton btn2 = new JButton();
		btn2.setLocation(433, 330);
		btn2.setSize(167, 68);
		btn2.setOpaque(false);
		btn2.setContentAreaFilled(false);
		
		/* 셋번째 구매 버튼 셋팅 */
		JButton btn3 = new JButton();
		btn3.setLocation(621, 330);
		btn3.setSize(167, 68);
		btn3.setOpaque(false);
		btn3.setContentAreaFilled(false);
		
		/* 네번째 구매 버튼 셋팅 */
		JButton btn4 = new JButton();
		btn4.setLocation(808, 330);
		btn4.setSize(167, 68);
		btn4.setOpaque(false);
		btn4.setContentAreaFilled(false);
		
		/* 버튼 및 이미지 패널에 넣기 */
		dialogItemPanel.add(btn);
		dialogItemPanel.add(btn2);
		dialogItemPanel.add(btn3);
		dialogItemPanel.add(btn4);		
		dialogItemPanel.add(ItemLabel);
		
		/* 다이얼 그램 생성 */
		Dialog ItemDialog = new Dialog(f, "ItemLabel");
		ItemDialog.setBounds(400, 300, 1200, 500);
		ItemDialog.add(dialogItemPanel);
		ItemDialog.setVisible(true);
		
		/* 버튼 이벤트 */
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ItemDialog.setVisible(false);
				ItemDialog.dispose();


			}
		});

		btn2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ItemDialog.setVisible(false);
				ItemDialog.dispose();


			}
		});

		btn3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ItemDialog.setVisible(false);
				ItemDialog.dispose();


			}
		});
			
		btn4.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ItemDialog.setVisible(false);
				ItemDialog.dispose();
			}
		});
	
		

	}

	public static void main(String[] args) {
		new goToItem();

	}
}
