package com.greedy.jb.member.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.greedy.jb.member.controller.playersettingcontroller.PlayerSettingController;
import com.greedy.jb.member.model.dao.UserDAO;
import com.greedy.jb.member.model.dto.GameResultDTO;
import com.greedy.jb.member.model.dto.UserDTO;

/**
 * 
 * <pre>
 * class : PlayerSetting
 * Comment : 게임을 진행 할 플레이어 수를 선택하는 클래스이다.
 * History
 * 2021/12/13 (박인근) 처음 작성
 * </pre>
 * @version 1.0.0
 * @author 박인근
 * @see MainFrame, MainMenu, UserDAOㄴrService, UserDTO, PlayerSettingController
 * 
 */
public class PlayerSetting {
	
	/* 하나의 프레임으로 패널 전환을 통해 메인 화면에서 버튼 클릭시 PlayerSetting 클래스로 넘어오게 구현하였다. 
	 * ZOrder를 사용하기 위해 하나의 JPanel을 만들어서 하나의 패널로 전체적인 화면을 구현하였다.
	 * 플레이어 수를 입력하는 View 클래스이다.*/
	
	/** 프레임의 배경이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backImage;
	
	/** 이전 패널로 돌아가기 위해 사용하는 버튼의 이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backButtonImage;
	
	/** 다른 여러 패널, 버튼, 이미지 등을 ZOrder를 사용해 하나의 패널로 저장하기 위한 변수이다. */ 
	private JPanel contentPane;
	
	/** 
	 *  플레이어 수 선택 페이지에 들어오면 유저의 정보 저장을 시작하기 위해 가장 최근에 플레이한 유저의
	 *  유저의 번호를 조회하기 위한 변수이다.
	 *  */
	private final PlayerSettingController pController;
	
	/** @author 박인근
	 *  @version 1.0.0
	 *  @param mf 이전 페이지에서 패널 전환을 위해 받아오는 프레임이다.
	 *  @param userDTO 사용자의 정보를 담아 놓기 위한 DTO변수이다.
	 *  @param gameReSultDTO 게임 인원 수에 대한 정보를 담아 놓기 위한 DTO변수이다.
	 * */
	public PlayerSetting(MainFrame mf, UserDTO userDTO, GameResultDTO gameResultDTO) {
		
		pController = new PlayerSettingController();
		
		int x = 250;					// 버튼, 패널등의 기준 x좌표
		int y = 150;					// 버튼, 패널등의 기준 y좌표

		/* 배경이 될 이미지(backImage)와 이전 페이지로 이동할 버튼에 사용될 이미지 초기화 및 크기 조절 */
		backImage = new ImageIcon("img/lightBackground.PNG");
		backButtonImage = new ImageIcon("img/Back_image.png");
		Image backBtnImage = backButtonImage.getImage().getScaledInstance(200, 100, Image.SCALE_SMOOTH);
		ImageIcon backButtonImage2 = new ImageIcon(backBtnImage);
		
		/* 
		 * 이전 페이지로 돌아가기 버튼 
		 * 버튼을 매개변수로 받은 이미지로 초기화하고,
		 * setBounds로 위치할 좌표와 너비, 높이를 정해주고
		 * setBorderPainted를 false로 바꿔 버튼의 외곽선을 없애준다.
		 * setFocusPainted를 false로 바꿔 버튼이 선택되었을 때 테두리를 생기지 않게 해준다.
		 * setContentAreaFilled를 false로 바꿔 내용영역 채우지 않기로 변경한다.
		 */
		JButton backBtn = new JButton(backButtonImage2);
		backBtn.setBounds(0, 0, 200, 110);
		backBtn.setBorderPainted(false);
		backBtn.setFocusPainted(false);
		backBtn.setContentAreaFilled(false);
		
		/*
		 * 이전 페이지로 돌아가기 버튼을 클릭시 발생하는 이벤트이다.
		 * 버튼을 클릭하게 되면 프레임의 패널을 변경해주는 changePanel 메소드가 실행된다.
		 * MainMenu의 getPanel 메소드를 사용해 panel 값을 반환받아와서 패널을 변경하였다.
		 */
		backBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				/* PanelSwitch 클래스의 changePanel메소드를 사용하여 이전 페이지의 클래스인 MainMenu의 getMainPanel 메소드를 이용해
				 * MainMenu클래스의 패널을 받아와서 변경한다. 
				 */
				PanelSwitch.changePanel(mf, contentPane, new MainMenu(mf).getMainPanel());
			}
		});
		
		/* 
		 * SWING의 컴포넌트가 자신의 모양을 그리는 메소드인 paintComponent 메소드를 사용하여
		 * 배경 이미지가 될 패널인 backgroundPanel에 이미지를 그리기 위해 Graphics의 drawImage
		 * 메소드를 사용하여 해당 이미지와 좌표, 크기를 정해 패널을 설정하였다.
		 * drawImage의 마지막 매개변수가 null을 준 이유는 이미지를 로딩하는 스레드가 있고 
		 * 마지막 매개변수로 이미지의 로딩 시점을 알 수 있는데 이미지의 로딩 시점을 알 필요가 없기 때문이다.
		 * 좌표와 크기는 프레임과 같게 한다.
		 */
		JPanel backgroundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(backImage.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgroundPanel.setBounds(0, 0, 1440, 900);
		
		/* 배경 패널 위에 올라갈 라벨이며 글자수와 크기를 조정해준다. */
		JLabel numOfPlayerLabel = new JLabel("플레이어 수를 선택하세요.");
		numOfPlayerLabel.setFont(new Font("HY견고딕", Font.BOLD, 24));
		numOfPlayerLabel.setFont(numOfPlayerLabel.getFont().deriveFont(32.0f));
		
		/* numOfPlayerLabel을 담을 패널이다. RGB 값으로 색을 정해주고 좌표와 크기를 정해준다.  */
		JPanel textPanel = new JPanel();
		textPanel.add(numOfPlayerLabel);
		textPanel.setBackground(new Color(216, 33, 55));
		textPanel.setBounds(x, y, x + 650, 50);

		/* 인원 수를 선택할 라디오 버튼을 초기화해준다. 그룹화를 하여 다중 선택이 안되도록 한다. */
		JRadioButton[] radioBtn = new JRadioButton[3];
		ButtonGroup selectNumPlayer = new ButtonGroup();
		
		/* 
		 * 각 라디오 버튼 객체 초기화와 그룹에 추가해주고 버튼의 배경을
		 * SetBorderPainted, setContentAreaFilled, setFocusPainted로 지워준다.
		 * setBounds를 통해 위치와 크기를 조정한다.  
		 */
		for(int i = 0; i < radioBtn.length; i++) {
			radioBtn[i] = new JRadioButton();
			selectNumPlayer.add(radioBtn[i]);

			radioBtn[i].setBorderPainted(false);
			radioBtn[i].setContentAreaFilled(false);
			radioBtn[i].setFocusPainted(false);
		}

		radioBtn[0].setBounds(x - 10, y + 350, 20, 20);
		radioBtn[1].setBounds(x + 470, y + 350, 20, 20);
		radioBtn[2].setBounds(x + 920, y + 350, 20, 20);
	
		/* 
		 * 선택할 수 있는 인원 수를 화면에 보여주기 위한 라벨 배열 선언 및 초기화한다.
		 * setFont를 통해 라벨의 글씨 크기를 조절한다.
		 */
		JLabel[] numLabel = new JLabel[3];
		numLabel[0] = new JLabel("2");
		numLabel[1] = new JLabel("3");
		numLabel[2] = new JLabel("4");
		
		for(int i = 0; i < numLabel.length; i++) {
			numLabel[i].setFont(numLabel[i].getFont().deriveFont(30.0f));
		}
		
		/* 
		 * 각 인원 수가 적힌 라벨에 대한 패널 배열을 선언 및 초기화한다.
		 *  각 순서에 맞게 패널에 추가하고 setBounds를 통해 위치를 조정한다.
		 */
		
		JPanel[] numPanel = new JPanel[3];
		
		for(int i = 0; i < numPanel.length; i++) {
			numPanel[i] = new JPanel();
			numPanel[i].add(numLabel[i]);
		}
		
		numPanel[0].setBounds(x - 50, y + 230, 100, 50);
		numPanel[1].setBounds(x + 430, y + 230, 100, 50);
		numPanel[2].setBounds(x + 880, y + 230, 100, 50);
		
		/* 
		 * 자식 컴포넌트들을 담을 패널인 contentPane을 초기화한다.
		 * 자식 컴포넌트들이 겹치지 않을 경우를 보장하지 못하기 때문에
		 * isOptimizedDrawingEnabled의 값을 false를 반환하도록 한다.
		 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		
		/* 
		 * contentPane은 단지 자식 컴포넌트들을 담을 패널이기 때문에 layout을 null 값을 넣어 지정해주지 않는다. 
		 * 자식 컴포넌트들을 add로 추가해주고 화면에 가장 최우선으로 보여야 될 값을 낮은 값으로 설정해준다.
		 */
		contentPane.setLayout(null);
		contentPane.add(backgroundPanel);
		contentPane.add(textPanel);
		contentPane.add(backBtn);
		contentPane.setComponentZOrder(backgroundPanel, 1);
		contentPane.setComponentZOrder(textPanel, 0);
		contentPane.setComponentZOrder(backBtn, 0);
		
		for(int i = 0; i < numPanel.length; i++) {
			contentPane.add(numPanel[i]);
			contentPane.setComponentZOrder(numPanel[i], 0);
			contentPane.setComponentZOrder(radioBtn[i], 0);
		}
		
		/* 
		 * 각 라디오 버튼을 클릭 시 발생하는 이벤트이다.
		 * pController를 통해 가장 최근에 플레이한 사용자의 번호를 조회하고
		 * userDTO의 회원번호를 조회한 회원번호에 1을 증가 시킨 값을 저장한다.
		 * gameResultDTO의 인원 수에 대한 값을 사용자가 선택한 인원 수로 변경하고
		 * changePanel 메소드를 통해 현재 클래스의 패널을 다음 페이지인 NickNameSetting 클래스의 panel로 변경한다.
		 */
		radioBtn[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userDTO.setUserNo(pController.selectLastUserNo() + 1);
				gameResultDTO.setPlayerHead(2);
				PanelSwitch.changePanel(mf, contentPane, new NickNameSetting(mf, userDTO, gameResultDTO).getNickPanel());
			}
		});
		
		radioBtn[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userDTO.setUserNo(pController.selectLastUserNo() + 1);
				gameResultDTO.setPlayerHead(3);
				PanelSwitch.changePanel(mf, contentPane, new NickNameSetting(mf, userDTO, gameResultDTO).getNickPanel());
			}
		});

		radioBtn[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userDTO.setUserNo(pController.selectLastUserNo() + 1);
				gameResultDTO.setPlayerHead(4);
				PanelSwitch.changePanel(mf, contentPane, new NickNameSetting(mf, userDTO, gameResultDTO).getNickPanel());
			}
		});
		
	}
	
	/**
	 * 
	 * @return contentPane 현재 클래스의 패널을 반환하는 메소드이다.
	 */
	public JPanel getPlayerPanel() {
		
		return contentPane;
	}
	
	
}