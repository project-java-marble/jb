package com.greedy.jb.member.views;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.greedy.jb.member.model.dto.GameResultDTO;
import com.greedy.jb.member.model.dto.RankingDTO;
import com.greedy.jb.member.model.dto.RuleDTO;
import com.greedy.jb.member.model.dto.UserDTO;
import com.greedy.jb.member.model.service.GameInfoService;
import com.greedy.jb.member.model.service.RankingViewService;
/**
 * 
 * <pre>
 * Class : MainMenu
 * Comment : [시작화면] 메인메뉴
 * History
 * 2020/09/08 (장민주) 처음 작성함
 * </pre>
 * @author 장민주
 * @version 1.0.0
 * @see 
 *
 */
public class MainMenu {
	
	/* 시작 화면 구현, [게임설명], [랭킹보기], [게임시작], [관리자모드] 이동 기능 구현 */
	
	/** 하위 패널들을 취합해 MainFrame에 인자로 전달할 변수 필드로 지정 */
	private JPanel contentPane;
	/** [게임시작] 패널로 이동할 때 인자값을 넣어줄 필드 생성 */
	private UserDTO userDTO;
	private GameResultDTO gameResultDTO;
	
	/** MainFrame과 DTO자료형의 list parameter를 받는 생성자
	 * 화면구현 소스코드, 화면 내 버튼 이벤트 동작 소스코드로 구성 */
	public MainMenu(MainFrame mf) {
		
		/** 선언한 필드의 인스턴스화 */
		userDTO = new UserDTO();
		gameResultDTO = new GameResultDTO();
		
		/** 사용할 배경화면 이미지 */
		ImageIcon background = new ImageIcon("img/MainFrame1.png");

		/** 배경화면 패널 생성하며 이미지 삽입 */
		JPanel backgroundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(background.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgroundPanel.setBounds(0, 0, 1440, 900);

		/** 패널 위에 패널을 중첩 시 오류 발생을 방지하는 코드 작성*/
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};

		/** 버튼에 삽입될 이미지아이콘 설정 */
		ImageIcon bStart = new ImageIcon("img/button_start.png");
		Image img1 = bStart.getImage();
		Image changeImage1 = img1.getScaledInstance(400, 100, Image.SCALE_SMOOTH);
		ImageIcon bStart2 = new ImageIcon(changeImage1);

		ImageIcon bInfo = new ImageIcon("img/button_info.png");
		Image img2 = bInfo.getImage();
		Image changeImage2 = img2.getScaledInstance(400, 100, Image.SCALE_SMOOTH);
		ImageIcon bInfo2 = new ImageIcon(changeImage2);

		ImageIcon bRanking = new ImageIcon("img/button_ranking.png");
		Image img3 = bRanking.getImage();
		Image changeImage3 = img3.getScaledInstance(400, 100, Image.SCALE_SMOOTH);
		ImageIcon bRanking2 = new ImageIcon(changeImage3);

		ImageIcon bEnd = new ImageIcon("img/button_end.png");
		Image img4 = bEnd.getImage();
		Image changeImage4 = img4.getScaledInstance(400, 100, Image.SCALE_SMOOTH);
		ImageIcon bEnd2 = new ImageIcon(changeImage4);

		ImageIcon bAdmin = new ImageIcon("img/button_admin.png");
		Image img5 = bAdmin.getImage();
		Image changeImage5 = img5.getScaledInstance(25, 20, Image.SCALE_SMOOTH);
		ImageIcon bAdmin2 = new ImageIcon(changeImage5);

		/** 버튼에 이미지 삽입 및 상세 설정 */
		JButton btnStart = new JButton(bStart2);
		btnStart.setLocation(960, 465);
		btnStart.setSize(400, 100);
		btnStart.setBorderPainted(false);
		btnStart.setFocusPainted(false);
		btnStart.setContentAreaFilled(false);

		JButton btnInfo = new JButton(bInfo2);
		btnInfo.setLocation(960, 590);
		btnInfo.setSize(400, 100);
		btnInfo.setBorderPainted(false);
		btnInfo.setFocusPainted(false);
		btnInfo.setContentAreaFilled(false);

		JButton btnRanking = new JButton(bRanking2);
		btnRanking.setLocation(960, 715);
		btnRanking.setSize(400, 100);
		btnRanking.setBorderPainted(false);
		btnRanking.setFocusPainted(false);
		btnRanking.setContentAreaFilled(false);

		JButton btnEnd = new JButton(bEnd2);
		btnEnd.setLocation(70, 715);
		btnEnd.setSize(400, 100);
		btnEnd.setBorderPainted(false);
		btnEnd.setFocusPainted(false);
		btnEnd.setContentAreaFilled(false);

		JButton btnAdmin = new JButton(bAdmin2);
		btnAdmin.setLocation(1160, 245);
		btnAdmin.setSize(25, 20);	
		btnAdmin.setBorderPainted(false);
		btnAdmin.setFocusPainted(false);
		btnAdmin.setContentAreaFilled(false);

		/** contentPane에 컴포넌트 추가, 화면 중첩 우선순위 정하기 */
		contentPane.setLayout(null);
		contentPane.add(backgroundPanel);
		contentPane.add(btnStart);
		contentPane.add(btnInfo);
		contentPane.add(btnRanking);
		contentPane.add(btnEnd);
		contentPane.add(btnAdmin);
		contentPane.setComponentZOrder(backgroundPanel, 1);
		contentPane.setComponentZOrder(btnStart, 0);
		contentPane.setComponentZOrder(btnInfo, 0);
		contentPane.setComponentZOrder(btnRanking, 0);
		contentPane.setComponentZOrder(btnEnd, 0);
		contentPane.setComponentZOrder(btnAdmin, 0);

		/**	버튼 클릭 이벤트 생성 */
		btnStart.addActionListener(new ActionListener() {
		
			/** 게임시작 화면으로 이동 
			 * @param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new PlayerSetting(mf, userDTO, gameResultDTO).getPlayerPanel());
				System.out.println("플레이어세팅 패널로 전환");
			}
			
		});
		
		/** 버튼 클릭 이벤트 생성 */
		btnInfo.addActionListener(new ActionListener() {

			/** 게임설명 화면으로 이동 
			 * @param e
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				/** 사용자가 버튼 클릭 시 service에 데이터 요청 */
				GameInfoService gameInfoService = new GameInfoService();

				/** service로부터 반환받은 데이터 저장 목적의 List */
				List<RuleDTO> rules = gameInfoService.selectAllRules();
				
				PanelSwitch.changePanel(mf, contentPane, new GameInfo(mf, rules).getGameInfoPanel());
				System.out.println("게임설명 패널로 전환");
			}

		});

		/** 버튼 클릭 이벤트 생성 */
		btnRanking.addActionListener(new ActionListener() {
	        
	        /** 랭킹보기 화면으로 이동 
			 * @param e
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {

				/** 사용자가 버튼 클릭시 service에 데이터 요청 */
		        RankingViewService rankingViewService = new RankingViewService();
		        
		        /** service로부터 반환받은 데이터 저장 목적의 List */
		        List<RankingDTO> ranking = rankingViewService.selectRanking();			

				PanelSwitch.changePanel(mf, contentPane, new RankingView(mf, ranking).getRankingViewPanel());
				System.out.println("랭킹조회 패널로 전환");
			}
		});

		/** 버튼 클릭 이벤트 생성 */
		btnEnd.addActionListener(new ActionListener() {
			
			/** 프로그램 종료
			 * @param e 
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("프로그램 종료");
				System.exit(0);

			}
		});
		
		/** 버튼 클릭 이벤트 생성 */
		btnAdmin.addActionListener(new ActionListener() {

			/** 관리자모드 로그인 화면으로 이동 
			 * @param e
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new AdminView(mf).getAdminLogIn());
				System.out.println("관리자모드 로그인");
			}
		});
		
		mf.add(contentPane);
	}
	
	/**
	 * 초기화면 패널 반환
	 * @return contentPane 시작메뉴 패널
	 */
	public JPanel getMainPanel() {
		
		return contentPane;
	}

	
}
