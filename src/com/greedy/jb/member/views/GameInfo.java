package com.greedy.jb.member.views;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.greedy.jb.member.model.dto.RuleDTO;

public class GameInfo {

	private JPanel contentPane;
	
	public GameInfo() {}
	
	public GameInfo(MainFrame mf, List<RuleDTO> rules) {
		
		/** 사용할 배경화면 이미지 불러오기 */
		ImageIcon background = new ImageIcon("img/GameInfo.png");
	
		/** 패널에 배경화면 이미지 삽입 */
		JPanel backgroundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(background.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgroundPanel .setBounds(0, 0, 1440, 900);
	
		/** 패널 위에 패널을 중첩 시 오류 발생을 방지하는 코드 작성 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		contentPane.setLayout(null);
		
		/** 버튼에 삽입될 이미지 아이콘 설정 */
		ImageIcon bBack = new ImageIcon("img/Back_image.png");
		Image img = bBack.getImage();
		Image changeImage = img.getScaledInstance(200, 110, Image.SCALE_SMOOTH);
		ImageIcon bBack2 = new ImageIcon(changeImage);
		
		/** 버튼에 이미지 삽입 및 상세 설정 */
		JButton btnBack = new JButton(bBack2);
		btnBack.setBounds(0, 0, 200, 110);
		btnBack.setBorderPainted(false);
		btnBack.setFocusPainted(false);
		btnBack.setContentAreaFilled(false);
		
		/** 게임 규칙을 담을 문자열 배열 선언 후 값 대입 */
		String[] ruleList = new String[6];

		for(int i = 0; i < rules.size(); i++) {
			ruleList[i] = rules.get(i).toString();
		}
		
		/** 조회된 게임 규칙을 담을 textArea를 추가해줄 패널과 함께 생성 */
		JPanel space = new JPanel();
		
		JTextArea rule1 = new JTextArea(ruleList[0]);
		JTextArea rule2 = new JTextArea(ruleList[1]);
		JTextArea rule3 = new JTextArea(ruleList[2]);
		JTextArea rule4 = new JTextArea(ruleList[3]);
		JTextArea rule5 = new JTextArea(ruleList[4]);
		JTextArea rule6 = new JTextArea(ruleList[5]);
		
		/** 자동 줄바꿈 설정 */
		rule1.setLineWrap(true);
		rule2.setLineWrap(true);
		rule3.setLineWrap(true);
		rule4.setLineWrap(true);
		rule5.setLineWrap(true);
		rule6.setLineWrap(true);
		
		space.setBounds(280,  210,  880,  480);
		space.setLayout(new GridLayout(6, 1));
		space.add(rule1);
		space.add(rule2);
		space.add(rule3);
		space.add(rule4);
		space.add(rule5);
		space.add(rule6);
		
		/** 텍스트 크기 설정 */
		rule1.setFont(new Font("돋움", Font.BOLD, 20));
		rule2.setFont(new Font("돋움", Font.BOLD, 20));
		rule3.setFont(new Font("돋움", Font.BOLD, 20));
		rule4.setFont(new Font("돋움", Font.BOLD, 20));
		rule5.setFont(new Font("돋움", Font.BOLD, 20));
		rule6.setFont(new Font("돋움", Font.BOLD, 20));
		
		/** 글씨를 입력할 수 없도록 설정 */
		rule1.setEditable(false);
		rule2.setEditable(false);
		rule3.setEditable(false);
		rule4.setEditable(false);
		rule5.setEditable(false);
		rule6.setEditable(false);
		
		/** contentPane에 컴포넌트 추가 */
		contentPane.add(backgroundPanel);
		contentPane.add(btnBack);
		contentPane.add(space);
		
		/** 화면 중첩 우선순위 정하기 */
		contentPane.setComponentZOrder(backgroundPanel, 1);
		contentPane.setComponentZOrder(btnBack, 0);
		contentPane.setComponentZOrder(space, 0);
		
		
		/** 버튼 클릭 이벤트 메소드 */
		btnBack.addActionListener(new ActionListener() {
			
			/**
			 *이전화면으로 이동
			 *@param e
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new MainMenu(mf).getMainPanel());
				System.out.println("메인화면 패널로 전환");
				
			}
		});
		
		mf.add(contentPane);
	}
	
	/**
	 *게임설명 패널 반환 메소드 
	 *@return contentPane 게임설명 패널
	 **/
	public JPanel getGameInfoPanel() {
		
		return contentPane;
	}
	

}
