package com.greedy.jb.member.views;

import javax.swing.JPanel;

public class PanelSwitch {
	
	public static void changePanel(MainFrame mf, JPanel op, JPanel np) {
		mf.remove(op);
		mf.add(np);
		mf.repaint();			// 갱신
		mf.revalidate(); 		// repaint가 동작을 안할 경우도 있기 때문에 사용
		
	}

}
