package com.greedy.jb.member.views;

import java.awt.Button;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * <pre>
 * Class : goToHell
 * Comment : 복습지옥 도착 팝업창
 * History
 * 2021/12/21 (김영광)
 * </pre>
 * @version 1.0.0
 * @author GLORY
 * 
 * */
public class goToHell {
	
	public goToHell() {
		
		/* 복습지옥 팝업창 구현 */
		
		/* 패널생성 */
		JFrame f = new JFrame();
		f.setBounds(100, 100, 1440, 900);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);
		
		/* 다이얼로그에 집어넣을 패널 */
		JPanel dialogHellPanel = new JPanel();
		dialogHellPanel.setLayout(null);
		
		/* 복습지옥 이미지 라벨에 넣기*/
		ImageIcon goToHell = new ImageIcon("C:/Users/GLORY/git/jb/img/hell.png");
		JLabel hellLabel = new JLabel(goToHell);
		hellLabel.setBounds(0, 0, 900, 350);
		
		/* 버튼 셋팅 */
		JButton btn = new JButton();
		btn.setLocation(355,227);
		btn.setSize(200, 68);
		btn.setOpaque(false); //투명하게
		btn.setContentAreaFilled(false); //내용 채우기 안함
		
		/* 버튼과 라벨 패널에 넣기 */
		dialogHellPanel.add(btn);
		dialogHellPanel.add(hellLabel);
		
		/* 복습지옥 다이얼 그램 생성  */
		Dialog hellDialog = new Dialog(f, "hellLabel");
		hellDialog.setBounds(400, 300, 900, 370);
		hellDialog.add(dialogHellPanel);
		hellDialog.setVisible(true);
		
		/* 버튼 이벤트  */
		btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				hellDialog.setVisible(false);
				hellDialog.dispose();
		
				
			}
		});
	}

	public static void main(String[] args) {
		new goToHell();

	}

}
