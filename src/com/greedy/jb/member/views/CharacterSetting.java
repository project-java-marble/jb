package com.greedy.jb.member.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.jb.member.controller.playersettingcontroller.PlayerSettingController;
import com.greedy.jb.member.model.dto.GameResultDTO;
import com.greedy.jb.member.model.dto.UserDTO;

/**
 * 
 * <pre>
 * Class : CharacterSetting
 * Comment : 사용자가 사용자의 캐릭터를 선택하는 클래스이다.
 * History
 * 2021/12/14 (박인근) 처음 작성함
 * </pre>
 * @author 박인근
 * @version 1.0.0
 * @see MainFrame, UserDTO, GameResultDTO, PlayerSettingController
 *
 */
public class CharacterSetting {
	
	/** 프레임의 배경이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backImage;
	
	/** 이전 패널로 돌아가기 위해 사용하는 버튼의 이미지가 될 ImageIcon 변수이다. */
	private ImageIcon backButtonImage;
	
	/** 다른 여러 패널, 버튼, 이미지 등을 ZOrder를 사용해 하나의 패널로 저장하기 위한 변수이다. */
	private JPanel contentPane;
	
	/** 캐릭터 선택까지 완료가 되면 PlayerSettingContoller의 setUser메소드를 통해 데이터베이스에 매개변수로 받은 UserDTO의 값들을 저장한다. */
	private final PlayerSettingController playerSettingController;
	
	/** 캐릭터버튼의 각 번호를 기록할 변수이다. */
	private String cNum;
	
	/**
	 * @author 박인근
	 * @version 1.0.0
	 * @param mf 이전 페이지에서 패널 전환을 위해 받아오는 프레임이다.
	 * @param userDTO 사용자의 정보를 담아 놓기 위한 DTO변수이다.
	 * @param gameResultDTO 게임 인원 수에 대한 정보를 담아 놓기 위한 DTO변수이다.
	 */
	public CharacterSetting(MainFrame mf, UserDTO userDTO, GameResultDTO gameResultDTO) {
		
		playerSettingController = new PlayerSettingController();
		
		/* 
		 * 배경이 될 이미지(backImage)와 이전 페이지로 이동할 버튼에 사용될 이미지 초기화 및 크기 조절 
		 */
		backImage = new ImageIcon("img/choiceCharacterPage.PNG");
		
		backButtonImage = new ImageIcon("img/Back_image.png");
		Image backBtnImage = backButtonImage.getImage().getScaledInstance(200, 100, Image.SCALE_SMOOTH);
		ImageIcon backButtonImage2 = new ImageIcon(backBtnImage);
		
		/* 
		 * 뒤로가기 이미지버튼의 좌표, 크기 배경 설정
		 */
		JButton backBtn = new JButton(backButtonImage2);
		backBtn.setBounds(0, 0, 200, 110);
		backBtn.setBorderPainted(false);
		backBtn.setFocusPainted(false);
		backBtn.setContentAreaFilled(false);
		
		/* 
		 * 배경 이미지가 될 패널인 backgroundPanel에 이미지를 그리기 위해 Graphics의 drawImage
		 * 메소드를 사용하여 해당 이미지와 좌표, 크기를 정해 패널을 설정하였다.
		 * 좌표와 크기는 프레임과 같게 한다.
		 */
		JPanel backgoundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(backImage.getImage(), 0, 0, 1440, 900, null);
			}
		};
		
		backgoundPanel.setBounds(0, 0, 1440, 900);
		
		/*
		 * 캐릭터 선택 버튼 배열 10개를 만들어주고 각 버튼에 대한 이미지를 설정해준다.
		 */
		JButton[] character = new JButton[10];
		
		ImageIcon JJang9 = getImageIcon(new ImageIcon("img/JJang9.png"));
		ImageIcon JJangA = getImageIcon(new ImageIcon("img/JJangA.png"));
		ImageIcon Maeng9 = getImageIcon(new ImageIcon("img/Maeng9.png"));
		ImageIcon Yuri = getImageIcon(new ImageIcon("img/Yuri.png"));
		ImageIcon WhiteDog = getImageIcon(new ImageIcon("img/WhiteDog.png"));
		ImageIcon Huni = getImageIcon(new ImageIcon("img/Huni.png"));
		ImageIcon CheolSu = getImageIcon(new ImageIcon("img/CheolSu.png"));
		ImageIcon BuriBuri = getImageIcon(new ImageIcon("img/BuriBuri.png"));
		ImageIcon Boss = getImageIcon(new ImageIcon("img/Boss.png"));
		ImageIcon ActionMask = getImageIcon(new ImageIcon("img/ActionMask.png"));
		
		character[0] = new JButton(JJang9);
		character[1] = new JButton(JJangA);
		character[2] = new JButton(ActionMask);
		character[3] = new JButton(CheolSu);
		character[4] = new JButton(Yuri);
		character[5] = new JButton(WhiteDog);
		character[6] = new JButton(Boss);
		character[7] = new JButton(BuriBuri);
		character[8] = new JButton(Maeng9);
		character[9] = new JButton(Huni);
		
		/*
		 * 캐릭터버튼을 각 위치에 배치하기 위해 x, y 기준 좌표를 잡고 배치하였다.
		 * deleteBackgroundBtn 메소드를 사용해서 버튼의 배경을 지워준다.
		 * 캐릭터버튼은 한줄에 5개씩 2줄이 존재하도록 반복문을 돌며 화면에 위치하도록 한다.
		 * 각 캐릭터버튼마다 setText를 통해 캐릭터의 번호륿 부여해준다.
		 */
		int x = 240;										// 캐릭터 버튼의 x 기준 좌표
		int y = 270;										// 캐릭터 버튼의 y 기준 좌표
		int area = 150;										// 캐릭터 크기
		
		for(int i = 0; i < character.length; i++) {
			if(i < 5) {
				character[i].setBounds(x, y, area, area);
				x += 200;
			} else {
				if(i == 5) {
					x = 240;
					y = 500;
				}
				character[i].setBounds(x, y, area, area);
				x += 200;
			}
			character[i] = deleteBackgroundBtn(character[i]);
			character[i].setText(String.valueOf(i + 1));
		}
		
		/* 
		 * 자식 컴포넌트들을 담을 패널인 contentPane을 초기화한다.
		 * 자식 컴포넌트들이 겹치지 않을 경우를 보장하지 못하기 때문에
		 * isOptimizedDrawingEnabled의 값을 false를 반환하도록 한다.
		 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		
		contentPane.setLayout(null);
		contentPane.add(backgoundPanel);
		contentPane.add(backBtn);
		contentPane.setComponentZOrder(backgoundPanel, 1);
		contentPane.setComponentZOrder(backBtn, 0);

		for(int i = 0; i < character.length; i++) {
			contentPane.add(character[i]);
			contentPane.setComponentZOrder(character[i], 0);
		}
		
		/*
		 * 이전 페이지로 돌아가기 버튼을 클릭시 발생하는 이벤트이다.
		 * 버튼을 클릭하게 되면 프레임의 패널을 변경해주는 changePanel 메소드가 실행된다.
		 * PlayerSetting의 getPanel 메소드를 사용해 panel값을 반환받아와서 패널을 변경하였다.
		 */
		backBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				userDTO.setPlayId(null);
				PanelSwitch.changePanel(mf, contentPane, new NickNameSetting(mf, userDTO, gameResultDTO).getNickPanel());
			}
		});
		
		/*
		 * 캐릭터 선택 확인 dialog에 들어갈 이미지를 초기화 해주고 라벨에 이미지를 추가한다.
		 * 이미지가 올라간 라벨을 패널위에 올리고 dialog에 추가한다.
		 * setCha라는 메소드들 통해 캐릭터의 선택을 확인한다.
		 */
		ImageIcon selectConfirm = new ImageIcon("img/characterSelectConfirm2.png");
		Image selectConfirmImg = selectConfirm.getImage().getScaledInstance(800, 300, Image.SCALE_SMOOTH);
		JLabel selectLabel = new JLabel(selectConfirm);
		
		JPanel dialogBackPanel = new JPanel();
		dialogBackPanel.add(selectLabel);
		
		Dialog selectConfirmDialog = new Dialog(mf, "캐릭터 선택 확인");
		
		selectConfirmDialog.setBounds(320, 300, 800, 370);
		
		selectConfirmDialog.add(dialogBackPanel);
		
		setCha(character, selectConfirmDialog, dialogBackPanel, userDTO, gameResultDTO, mf);
		
	}
	
	/**
	 * 
	 * @return contentPane 현재 클래스의 패널을 반환하는 메소드이다.
	 */
	public JPanel getCharacterPanel() {
		
		return contentPane;
	}
	
	/**
	 * 
	 * @param imageIcon 캐릭터이미지의 크기를 버튼에 올릴 수 있도록 높이, 너비를 맞출 변수
	 * @return imageIcon2 캐릭터버튼 크기만큼 변환한 ImageIcon
	 */
	public ImageIcon getImageIcon(ImageIcon imageIcon) {
		
		Image image = imageIcon.getImage().getScaledInstance(150, 150, Image.SCALE_SMOOTH);
		ImageIcon imageIcon2 = new ImageIcon(image);
		
		return imageIcon2;
	}
	
	/**
	 * 
	 * @param chaBtn 배경, 테두리 등을 지울 버튼
	 * @return chaBtn 배경, 테두리 등을 지운 버튼
	 */
	public JButton deleteBackgroundBtn(JButton chaBtn) {
		
		chaBtn.setBorderPainted(false);
		chaBtn.setFocusPainted(false);
		chaBtn.setContentAreaFilled(false);
		
		return chaBtn;
	}
	
	/***
	 * 
	 * @param character 10개의 캐릭터 버튼이 들어있는 버튼 배열
	 * @param selectConfirmDialog 캐릭터 선택 확인창 dialog
	 * @param dialogBackPanel dialog에 들어갈 패널(배경)
	 * @param userDTO 유저의 정보가 기록될 DTO
	 * @param gameResultDTO 선택한 인원 수 정보를 다음 페이지로 전달 하기 위한 DTO
	 * @param mf 메인 프레임
	 */
	public void setCha(JButton[] character, Dialog selectConfirmDialog, JPanel dialogBackPanel, UserDTO userDTO, GameResultDTO gameResultDTO, MainFrame mf) {
		
		userDTO.setUserId("PLAYER");												//사용자가 관리자가 이닌 플레이어라는걸 데이터베이스에 전달하기 위해 userDTO의 userID를 PLAYER로 저장한다.
		
		/* 캐릭터버튼을 클릭 시 클릭한 캐릭터의 번호를 저장하고 dialog창을 보여준다. */
		for(int i = 0; i < character.length; i++) {
			
			character[i].addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					cNum = ((JButton)e.getSource()).getText();
					selectConfirmDialog.setVisible(true);
				}
			});
			
		}
		
		/*
		 * dialog창의 캐릭터 선택 확인 창에 선택한 캐릭터 확정 버튼의 좌표 안에서 마우스 클릭이 일어날 시 
		 * 선택한 캐릭터버튼의 번호를 userDTO의 setCharNo을 통해 캐릭터번호를 추가해준다.
		 * 이 때, 캐릭터버튼의 번호는 문자열로 저장했고 userDTO의 CharNo은 정수형이기때문에 parsing을 해주고 값을 넘겨준다.
		 * playerSettingController의 setUser를 통해 userDTO에 저장된 데이터들을 데이터베이스에 저장한다.
		 * changePanel을 통해 GameWindow 클래스의 contentPane 패널로 변경한다.
		 * 마우스 클릭이 아니오 좌표 범위 안에서 일어나면 그냥 dialog창을 종료한다.
		 */
		dialogBackPanel.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if((e.getX() >= 200 && e.getX() <= 370) && (e.getY() >= 175 && e.getY() <= 225)) {
					userDTO.setCharNo(Integer.parseInt(cNum));
					gameResultDTO.setUserNo(userDTO.getUserNo());
					playerSettingController.setUser(userDTO.getUserId(), userDTO.getUserNo(), userDTO.getPlayId(), userDTO.getCharNo());
					PanelSwitch.changePanel(mf, contentPane, new GameWindow(mf, userDTO, gameResultDTO).getGameWindowPanel());
					selectConfirmDialog.dispose();
				
				} else if((e.getX() >= 425 && e.getX() <= 560) && (e.getY() >= 175 && e.getY() <= 225)) {
					selectConfirmDialog.dispose();
				}
				
			}
		});
	}
	
}
