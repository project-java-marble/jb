package com.greedy.jb.member.views;

import javax.swing.JFrame;

public class MainFrame extends JFrame {
	
	public MainFrame() {
		this.setBounds(0, 0, 1440, 900);
		
		new MainMenu(this);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}
