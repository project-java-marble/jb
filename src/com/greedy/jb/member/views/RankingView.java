package com.greedy.jb.member.views;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.greedy.jb.member.model.dto.RankingDTO;
/**
 * <pre>
 * Class : RankingView
 * Comment : [랭킹보기]
 * History
 * 2020/12/14 (장민주) 처음 작성
 * 2020/12/15 (장민주) 화면 구현 완성
 * 2020/12/17 (장민주) 기능 구현 완성
 * </pre>
 * @version 1.0.0
 * @author 장민주
 * @see MainMenu, RankingSecondView
 */
public class RankingView {

	/* [랭킹보기] 화면 상의 첫 번째 페이지, DB로부터 조회해 온 랭킹 정보 표시 : 1~10위 */
	
	/** 하위 패널들을 취합해 MainFrame에 인자로 전달할 변수 필드로 지정 */
	private JPanel contentPane;
	
	/** MainFrame과  DTO자료형의 list parameter를 받는 생성자 
	 * 화면구현 소스코드, 데이터 값 출력 소스코드, 화면 내 버튼 이벤트 동작 소스코드로 구성
	 * */
	public RankingView(MainFrame mf, List<RankingDTO> rankingList) {
		
		/** 사용할 배경화면 이미지 */
		ImageIcon background = new ImageIcon("img/rankingView1.png");
		
		/** 배경화면 패널 생성 후 이미지 삽입 */
		JPanel backgroundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(background.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgroundPanel.setBounds(0, 0, 1440, 900);
		
		/** contentPane 인스턴스를 생성과 동시에 패널 중첩 시 충돌로 인한 오류 발생을 방지 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};
		
		/** 각 버튼에 삽입될 이미지아이콘 */
		ImageIcon bBack = new ImageIcon("img/Back_image.png");
		Image img1 = bBack.getImage();
		Image changeImage1 = img1.getScaledInstance(200, 110, Image.SCALE_SMOOTH);
		ImageIcon bBack2 = new ImageIcon(changeImage1);
		
		ImageIcon bNextPage = new ImageIcon("img/nextPage.png");
		Image img2 = bNextPage.getImage();
		Image changeImage2 = img2.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		ImageIcon bNextPage2 = new ImageIcon(changeImage2);
	
		/** 버튼 생성 및 상세설정 */
		JButton btnBack = new JButton(bBack2);
		btnBack.setBounds(0, 0, 200, 110);
		btnBack.setBorderPainted(false);
		btnBack.setFocusPainted(false);
		btnBack.setContentAreaFilled(false);
		
		JButton btnNextPage = new JButton(bNextPage2);
		btnNextPage.setBounds(1180, 420, 60, 60);
		btnNextPage.setBorderPainted(false);
		btnNextPage.setFocusPainted(false);
		btnNextPage.setContentAreaFilled(false);
		
		/** 순위별 플레이어 정보를 담는 문자열 배열 */
		String[] rank = new String[10];
		
		for(int i = 0; i < rankingList.size(); i++) {
			rank[i] = (i + 1) + ". " + rankingList.get(i).toString();
			
		}
		/** 조회된 랭킹 순위를 담을 textArea를 추가해줄 패널과 함께 생성 */
		JPanel ranking = new JPanel();
		
		JTextArea rank1 = new JTextArea(rank[0]);
		JTextArea rank2 = new JTextArea(rank[1]);
		JTextArea rank3 = new JTextArea(rank[2]);
		JTextArea rank4 = new JTextArea(rank[3]);
		JTextArea rank5 = new JTextArea(rank[4]);
		JTextArea rank6 = new JTextArea(rank[5]);
		JTextArea rank7 = new JTextArea(rank[6]);
		JTextArea rank8 = new JTextArea(rank[7]);
		JTextArea rank9 = new JTextArea(rank[8]);
		JTextArea rank10 = new JTextArea(rank[9]);

		/** 랭킹리스트 크기 및 위치 지정, 
		 *  텍스트 크기 설정, 
		 *  글씨를 입력할 수 없도록 설정
		 */
		ranking.setBounds(280, 200, 880, 510);
		ranking.setLayout(new GridLayout(10, 1));
		ranking.add(rank1);
		ranking.add(rank2);
		ranking.add(rank3);
		ranking.add(rank4);
		ranking.add(rank5);
		ranking.add(rank6);
		ranking.add(rank7);
		ranking.add(rank8);
		ranking.add(rank9);
		ranking.add(rank10);
		
		rank1.setFont(new Font("돋움", Font.BOLD, 20));
		rank2.setFont(new Font("돋움", Font.BOLD, 20));
		rank3.setFont(new Font("돋움", Font.BOLD, 20));
		rank4.setFont(new Font("돋움", Font.BOLD, 20));
		rank5.setFont(new Font("돋움", Font.BOLD, 20));
		rank6.setFont(new Font("돋움", Font.BOLD, 20));
		rank7.setFont(new Font("돋움", Font.BOLD, 20));
		rank8.setFont(new Font("돋움", Font.BOLD, 20));
		rank9.setFont(new Font("돋움", Font.BOLD, 20));
		rank10.setFont(new Font("돋움", Font.BOLD, 20));
		
		rank1.setEditable(false);
		rank2.setEditable(false);
		rank3.setEditable(false);
		rank4.setEditable(false);
		rank5.setEditable(false);
		rank6.setEditable(false);
		rank7.setEditable(false);
		rank8.setEditable(false);
		rank9.setEditable(false);
		rank10.setEditable(false);

		/** contentPane에 컴포넌트 추가, 화면 중첩 우선순위 지정 */
		contentPane.setLayout(null);
		contentPane.add(backgroundPanel);
		contentPane.add(btnBack);
		contentPane.add(btnNextPage);
		contentPane.add(ranking);
		
		contentPane.setComponentZOrder(backgroundPanel, 1);
		contentPane.setComponentZOrder(btnBack, 0);
		contentPane.setComponentZOrder(btnNextPage, 0);
		contentPane.setComponentZOrder(ranking, 0);
		
		/** 버튼 클릭 이벤트 : 다음 화면으로 이동
		 * @param new ActionListener()
		 * @param e
		 * @param mf
		 * @param contentPane
		 * @param new RankingSecondView(mf, rankingList).getRankingSecondViewPanel()
		 * */
		btnNextPage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new RankingSecondView(mf, rankingList).getRankingSecondViewPanel());
				System.out.println("다음 화면으로 이동");
			}
		});
		
		/** 이전 화면으로 돌아가기 
		 * @param new ActionListener()
		 * @param e
		 * @param mf
		 * @param contentPane
		 * @param new MainMenu(mf).getMainPanel()
		 * */
		btnBack.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new MainMenu(mf).getMainPanel());
				System.out.println("이전 화면으로 이동");
			}
		});
	
		mf.add(contentPane);
		
	}
	
	/** 패널 전환을 위한 패널 반환 메소드
	 * @return contentPane 랭킹보기 패널
	 */
	public JPanel getRankingViewPanel() {
		
		return contentPane;
	}
}
