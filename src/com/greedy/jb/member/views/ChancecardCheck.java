package com.greedy.jb.member.views;

import java.awt.Dialog;
import java.awt.Dialog.ModalityType;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.greedy.jb.member.model.dto.ChanceCardDTO;

/**
 * <pre>
 * Class : ChancecardCheck
 * Comment : 보유한 찬스카드 확인 기능을 하는 클래스
 * History
 * 2021/12/20 (이용선) 처음 작성함
 * </pre>
 * @author 이용선
 * @version 1.0.0
 * @see
 * */
public class ChancecardCheck {

	
	
	public static void main(String[] args) {
		
		

		//메인프레임
		JFrame mainFrame = new JFrame("MyFrame");
		
		Rectangle rectAngle = new Rectangle(0, 0, 1440, 900);
		mainFrame.setBounds(rectAngle);
		
		
		
/*--------이미지설정부분 : 찬스카드 버튼 이미지   -----------------------------------------------*/
		ImageIcon icon = new ImageIcon("./img/hart.gif");
		JButton chanceview = new JButton(icon);
		JButton chanceview1 = new JButton(icon);
		JButton chanceview2 = new JButton(icon);
		JButton chanceview3 = new JButton(icon);

		/* 별(재화) 라벨 이미지 설정 */
		ImageIcon staricon = new ImageIcon("./img/Star.gif");
		JLabel label5 = new JLabel(staricon);
		
		
		/* 다이얼로그 배경이미지 크기를 변환한다 */
		ImageIcon dbackicon = new ImageIcon("./img/chancecardList.png");
		Image dbackimg = dbackicon.getImage();
		Image dbackchangeImage = dbackimg.getScaledInstance(700, 500, Image.SCALE_SMOOTH);
		ImageIcon dbackchangeicon = new ImageIcon(dbackchangeImage);
		JLabel dialbackg = new JLabel(dbackchangeicon);
		JPanel dialogbackpanel = new JPanel();
		dialogbackpanel.add(dialbackg);
		
		/* 되돌아가기 버튼 이미지를 변환한다. */
		ImageIcon dicon = new ImageIcon("./img/returnBtn.png");
		Image dimg = dicon.getImage();
		Image dchangeImage = dimg.getScaledInstance(150, 80, Image.SCALE_SMOOTH);
		ImageIcon dchangeicon = new ImageIcon(dchangeImage);
		
		chanceview.setBorderPainted(false);
		chanceview.setContentAreaFilled(false);
		chanceview.setFocusPainted(false);
		chanceview1.setBorderPainted(false);
		chanceview1.setContentAreaFilled(false);
		chanceview1.setFocusPainted(false);
		chanceview2.setBorderPainted(false);
		chanceview2.setContentAreaFilled(false);
		chanceview2.setFocusPainted(false);
		chanceview3.setBorderPainted(false);
		chanceview3.setContentAreaFilled(false);
		chanceview3.setFocusPainted(false);
		
		
		/* 찬스버튼의 크기 설정 */
		chanceview.setBounds(350, 100, 50, 50);
		chanceview1.setBounds(350, 250, 50, 50);
		chanceview2.setBounds(350, 400, 50, 50);
		chanceview3.setBounds(350, 550, 50, 50);
		
		
/*-------------------찬스카드확인 부분  다이얼로그 설정-----------------------------------------------------------------------------------------*/	
		chanceview.addActionListener(new ActionListener() {
			
			/**
			 * @param mainFrame 프레임을 표시해줄 메인 프레임을 지정
			 * @param "cardinfoFrame" 다이얼로그 이름
			 * @param ModalityType.MODELESS.APPLICATION_MODAL 가장 뒤의 APPLICATION_MODAL로 모달속성 창으로 설정
			 * 이슈사항 : 찬스카드 보기 버튼을 클릭하였을때 마다 다이얼로그 창이 중복하여 발생
			 * 해결 : 다이얼로그 창을 모달 창으로 전환하여 버튼을 다시 누를 수 없도록 함
			 */
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JDialog cardviewdialog = new JDialog(mainFrame,"cardinfoFrame",ModalityType.MODELESS.APPLICATION_MODAL);
				cardviewdialog.add(dialogbackpanel);
				
				
				
				//다이얼로그 되돌아가기 버튼 설정
				JButton dbtn = new JButton(dchangeicon);
				dbtn.setBounds(290, 330, 130, 100);
				dbtn.setFocusPainted(false);
				dbtn.setContentAreaFilled(false);
				dbtn.setBorderPainted(false);
				
				
				
				/*
				 * 리스트로 저장된 값을 순차적으로 불러와 toString으로 출력 해주려고 하였다
				String[] chanceview = new String[2];
				for(int i =0; i<chanceview.length; i++) {
				chanceview[i]=chancecardlist.get(i).toString();
				}
				*/
				
				
				
				/*-----------------찬스카드 목록 확인---------------------
				 * JLabel label(숫자) = new JLabel(chanceview[숫자]); 
				 * 로 각각 라벨에 넣어주려하였다.
				 * */
				
				JLabel label1 = new JLabel("찬스카드 내용 1번");
				label1.setBounds(100, 150, 350, 50);
				label1.setFont(label1.getFont().deriveFont(25.0f));
				dialbackg.add(label1);
				
				JLabel label2 = new JLabel("찬스카드 내용 2번");
				label2.setBounds(100, 200, 350, 50);
				label2.setFont(label2.getFont().deriveFont(25.0f));
				dialbackg.add(label2);
				
				JLabel label3 = new JLabel("찬스카드 내용 3번");
				label3.setBounds(100, 250, 350, 50);
				label3.setFont(label3.getFont().deriveFont(25.0f));
				dialbackg.add(label3);
				
				/* ----------------------------별 보유 갯수 확인----------------------------*/
				JLabel label4 = new JLabel("별 보유 갯수");
				label4.setBounds(100, 300, 150, 50);
				label4.setFont(label4.getFont().deriveFont(25.0f));
				dialbackg.add(label4);
				
				/*별 이미지  라벨삽입*/
				label5.setBounds(35, 350, 150, 50);
				dialbackg.add(label5);
				
				JLabel label6 = new JLabel("X 보유 숫자");
				label6.setBounds(130, 350, 200, 50);
				label6.setFont(label6.getFont().deriveFont(25.0f));
				dialbackg.add(label6);
				
				
				
				dbtn.addActionListener(new ActionListener() {
					
					/*다이얼로그 창 닫기*/
					@Override
					public void actionPerformed(ActionEvent e) {
						cardviewdialog.dispose();
						
						
//					대화상자 이벤트 종료
					}
				});
				/* 다이얼로그 창의 정의를해준다 
				 * 하단에 빼서 작성하는이유 
				 * 이슈사항:상단 다이얼로그 만들때 넣어주면 모달창으로 바뀌면서 다이얼로그에 올라간 라벨,버튼 등의 패널들이 출력이 안보이는 현상이 발생
				 * 해결 : 하단에 정의하면 패널이 전부 올라간 시점부터 모달창으로 표시가 됨*/ 
				cardviewdialog.setBounds(290, 200, 700, 500);
				cardviewdialog.add(dbtn);
				cardviewdialog.add(dialogbackpanel);
				cardviewdialog.setVisible(true);
				
//				찬스카드 보기 버튼 클릭 이벤트 종료
			}
		});
/*------------------------------------------------------------------------------------------------------------*/
/*결과 창 시작*/
		
		ImageIcon resulticon = new ImageIcon("./img/resultback.png");
		JPanel resultview = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(resulticon.getImage(), 0, 0, 650, 300, null);
				
			}
		};

		resultview.setBounds(200, 200, 700, 500);
		
		JLabel lbgameset = new JLabel("게임 종료");
		lbgameset.setBounds(460, 250, 150, 50);
		lbgameset.setFont(lbgameset.getFont().deriveFont(30.0f));
		lbgameset.setVisible(false);
		
		JLabel label1 = new JLabel("1등");
		label1.setBounds(460, 300, 150, 50);
		label1.setFont(label1.getFont().deriveFont(20.0f));
		label1.setVisible(false);				
		
		JLabel label2 = new JLabel("2등");
		label2.setBounds(460, 340, 150, 50);
		label2.setFont(label2.getFont().deriveFont(20.0f));
		label2.setVisible(false);
		
		JLabel label3 = new JLabel("3등");
		label3.setBounds(460, 380, 150, 50);
		label3.setFont(label3.getFont().deriveFont(20.0f));
		label3.setVisible(false);
		
		JLabel label11 = new JLabel("1등 이름");
		label11.setBounds(520, 300, 150, 50);
		label11.setFont(label11.getFont().deriveFont(20.0f));
		label11.setVisible(false);				
		
		JLabel label22 = new JLabel("2등 이름");
		label22.setBounds(520, 340, 150, 50);
		label22.setFont(label22.getFont().deriveFont(20.0f));
		label22.setVisible(false);
		
		JLabel label33 = new JLabel("3등 이름");
		label33.setBounds(520, 380, 150, 50);
		label33.setFont(label33.getFont().deriveFont(20.0f));
		label33.setVisible(false);
		
		
		/*남은 주사위 턴수 0일경우 결과창 출력되며 0이 아닐경우 결과창이 보이지 않도록 만듬
		 * dice결과 받아오는건 진행중*/
		int dice = 0;
		int flag = 0;

		if(dice == 0){
			resultview.setVisible(true);
			flag = 1;
		} else {
			resultview.setVisible(false);
			flag = 0;
		}
		
		if(flag == 1) {
			lbgameset.setVisible(true);
			label1.setVisible(true);
			label11.setVisible(true);
			label2.setVisible(true);
			label22.setVisible(true);
			label3.setVisible(true);
			label33.setVisible(true);
		} else {
			lbgameset.setVisible(false);
			label1.setVisible(false);
			label11.setVisible(false);
			label2.setVisible(false);
			label22.setVisible(false);
			label3.setVisible(false);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
			label33.setVisible(false);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
		}
		
		/**isOptimizedDrawingEnabled() 함수를 false값을 주어 패널이 겹쳐도 오류가 발생하지 않도록 함
		 * setComponentZOrder(first, 0)
		 * @param first JPanel인 content패널 안에 넣어 패널을 묶어줌
		 * @param 0 묶어준 패널들을 두 번째 매개변수를 이용하여 수가 적을수록 z축에서 우선순위를 가짐
		 * @return return값에 대한 설명
		 * @exception 예외 이유에 대한 설명
		 */
		JPanel contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() { 
				return false; 
			} 
		};
					
			
			contentPane.setLayout(null);
			//패널 추가
			contentPane.add(chanceview);
			contentPane.add(chanceview1);
			contentPane.add(chanceview2);
			contentPane.add(chanceview3);
			contentPane.add(resultview);
			contentPane.add(lbgameset);
			contentPane.add(label1);
			contentPane.add(label11);
			contentPane.add(label2);
			contentPane.add(label22);
			contentPane.add(label3);
			contentPane.add(label33);
			
			//패널 z축 우선순위 지정
			contentPane.setComponentZOrder(chanceview, 0);
			contentPane.setComponentZOrder(chanceview1, 0);
			contentPane.setComponentZOrder(chanceview2, 0);
			contentPane.setComponentZOrder(chanceview3, 0);
			contentPane.setComponentZOrder(resultview, 0);
			contentPane.setComponentZOrder(lbgameset, 0);
			contentPane.setComponentZOrder(label1, 0);
			contentPane.setComponentZOrder(label11, 0);
			contentPane.setComponentZOrder(label2, 0);
			contentPane.setComponentZOrder(label22, 0);
			contentPane.setComponentZOrder(label3, 0);
			contentPane.setComponentZOrder(label33, 0);
			
			
			
			
			
			mainFrame.setContentPane(contentPane);
			//세팅된값 마지막  보여주도록
			mainFrame.setVisible(true);
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


	}

}
