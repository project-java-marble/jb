package com.greedy.jb.member.views;

import java.awt.Dialog;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * 	<pre>
 * 	Class : goToAirport
 *  Comment : 공항도착 팝업창 구현
 *  History
 *  2021/12/20 (김영광)
 * 	</pre>
 *  @author GLORY
 *  @version 1.0.0
 *  	 
 * */
public class goToAirport {

	public goToAirport() {
		
		/* 공항 팝업창 구현 */
		
		JFrame f = new JFrame();
		f.setBounds(100, 100, 1440, 900);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.getContentPane().setLayout(null);

		/* 다이얼로그에 넣을 패널생성 */
		JPanel dialogAirprotPanel = new JPanel();
		dialogAirprotPanel.setLayout(null);
		
		/* 공항 팝업창 이미지 라벨에 넣기 */
		ImageIcon goToAirport = new ImageIcon("C:/Users/GLORY/git/jb/img/airport.png");
		JLabel airportLabel = new JLabel(goToAirport);
		airportLabel.setBounds(0, 0, 900, 350);
		
		/* 이지미 라벨 위에 놓을 버튼 셋팅  */
		JButton btn = new JButton();
		btn.setLocation(355,219);
		btn.setSize(200, 68);
		btn.setOpaque(false); //투명하게
		btn.setContentAreaFilled(false); //내용 채우기 안함
		
		/* 다이얼 그램에 넣을 패널 셋팅 */
		dialogAirprotPanel.add(btn);
		dialogAirprotPanel.add(airportLabel);
		
		/* 다이얼 그램 생성 */
		Dialog airportDialog = new Dialog(f, "airportLabel");
		airportDialog.setBounds(400, 300, 900, 370);
		airportDialog.add(dialogAirprotPanel);
		airportDialog.setVisible(true);
		
		/* 버튼 이벤트 */
		btn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				airportDialog.setVisible(false);
				airportDialog.dispose();


			}
		});
	}

	public static void main(String[] args) {
		new goToAirport();

	}

}
