package com.greedy.jb.member.views;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.greedy.jb.member.model.dto.RankingDTO;
/**
 * <pre>
 * Class : RankingThirdView
 * Comment : [랭킹보기] 화면 3
 * History
 * 2020/12/16 (장민주) 처음 작성
 * 2020/12/17 (장민주) 화면 구현 완성
 * 2020/12/19 (장민주) 기능 구현 완성
 * </pre>
 * @version 1.0.0
 * @author 장민주
 * @see MainMenu, RankingView, RankingSecondView
 */
public class RankingThirdView {

	/* [랭킹보기] 화면 상의 세 번째 페이지, DB로부터 조회해 온 랭킹 정보 표시 : 21~30위 */

	/** 하위 패널들을 취합해 MainFrame에 인자로 전달할 변수 필드로 지정 */
	private JPanel contentPane;

	/** MainFrame과  DTO자료형의 list parameter를 받는 생성자 
	 * 화면구현 소스코드, 데이터 값 출력 소스코드, 화면 내 버튼 이벤트 동작 소스코드로 구성
	 * */
	public RankingThirdView(MainFrame mf, List<RankingDTO> rankingList) {

		/** 사용할 배경화면 이미지 */
		ImageIcon background = new ImageIcon("img/rankingView1.png");

		/** 배경화면 패널 생성 후 이미지 삽입 */
		JPanel backgroundPanel = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(background.getImage(), 0, 0, 1440, 900, null);
			}
		};
		backgroundPanel.setBounds(0, 0, 1440, 900);

		/** contentPane 인스턴스를 생성과 동시에 패널 중첩 시 충돌로 인한 오류 발생을 방지 */
		contentPane = new JPanel() {
			public boolean isOptimizedDrawingEnabled() {
				return false;
			}
		};

		/** 각 버튼에 삽입될 이미지아이콘 */
		ImageIcon bBack = new ImageIcon("img/Back_image.png");
		Image changeToImage1 = bBack.getImage().getScaledInstance(200, 110, Image.SCALE_SMOOTH);
		ImageIcon bBack2 = new ImageIcon(changeToImage1);

		ImageIcon bPreviousPage = new ImageIcon("img/b-previous-page.png");
		Image changeToImage2 = bPreviousPage.getImage().getScaledInstance(60, 60, Image.SCALE_SMOOTH);
		ImageIcon bPreviousPage2 = new ImageIcon(changeToImage2);

		/** 버튼 생성 및 상세설정 */
		JButton btnBack = new JButton(bBack2);
		btnBack.setBounds(0, 0, 200, 110);
		btnBack.setBorderPainted(false);
		btnBack.setFocusPainted(false);
		btnBack.setContentAreaFilled(false);

		JButton btnPreviousPage = new JButton(bPreviousPage2);
		btnPreviousPage.setBounds(200, 420, 60 ,60);
		btnPreviousPage.setBorderPainted(false);
		btnPreviousPage.setFocusPainted(false);
		btnPreviousPage.setContentAreaFilled(false);

		/** 순위별 플레이어 정보를 담는 문자열 배열 */
		String[] rank = new String[10];

		for(int i = 0; i < rank.length; i++) {
			if(rankingList.size() >20) {
				rank[i] = (i + 21) + ", " + rankingList.get(i + 20).toString();
			} else {
				rank[i] = "-";    //값이 아직 존재하지 않는 순위의 경우 다음과 같이 표시
			}
		}

		/** 조회된 랭킹 순위를 담을 textArea를 추가해줄 패널과 함께 생성 */
		JPanel space = new JPanel();

		JTextArea[] rText = new JTextArea[10];

		space.setBounds(280, 200, 880, 510);
		space.setLayout(new GridLayout(10, 1));

		for(int i = 0; i < rank.length; i++) {
			rText[i] = new JTextArea();
			rText[i].setFont(new Font("돋움", Font.BOLD, 15));
			rText[i].setEditable(false);
			space.add(rText[i]);
			rText[i].setText(rank[i]);
		}

		/** contentPane에 컴포넌트 추가, 화면 중첩 우선순위 지정 */
		contentPane.setLayout(null);
		contentPane.add(backgroundPanel);
		contentPane.add(space);
		contentPane.add(btnBack);
		contentPane.add(btnPreviousPage);

		contentPane.setComponentZOrder(backgroundPanel, 1);
		contentPane.setComponentZOrder(space, 0);
		contentPane.setComponentZOrder(btnBack, 0);
		contentPane.setComponentZOrder(btnPreviousPage, 0);

		/** 버튼 클릭 이벤트 : 이전 화면으로 이동
		 * @param new ActionListener()
		 * @param e
		 * @param mf
		 * @param contentPane
		 * @param new RankingSecondView(mf, rankingList).getRankingSecondViewPanel()
		 * */
		btnPreviousPage.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new RankingSecondView(mf, rankingList).getRankingSecondViewPanel());
				System.out.println("이전 화면으로 이동");
			}
		});

		/** 메인 화면으로 돌아가기 
		 * @param new ActionListener()
		 * @param e
		 * @param mf
		 * @param contentPane
		 * @param new MainMenu(mf).getMainPanel()
		 * */
		btnBack.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				PanelSwitch.changePanel(mf, contentPane, new MainMenu(mf).getMainPanel());
				System.out.println("메인 화면으로 이동");
			}
		});
		mf.add(contentPane);

	}

	/** 패널 전환을 위한 패널 반환 메소드
	 * @return contentPane
	 */
	public JPanel getRankingThirdViewPanel() {
		return contentPane;
	}

}
