package com.greedy.jb.member.controller.chancecardcontroller;

import java.util.List;

import com.greedy.jb.member.model.dto.ChanceCardDTO;
import com.greedy.jb.member.model.service.ChanceService;

/**
 * <pre>
 * Class : ChanceCardController
 * Comment : 서비스로부터 찬스리스트를 갖고와 뷰에서 보여주기 위한 클래스
 * History 
 * 2021/12/21 (김영광)
 * </pre>
 * @version 1.0.0
 * @author GLORY
 * @see ChanceService,
 *      ChanceView
 * */
public class ChanceCardController {
	
	public List<ChanceCardDTO> ChanceCardController() {
		
		ChanceService cs = new ChanceService();
		List<ChanceCardDTO> chanceList = cs.selectChanceCard();
		
		return chanceList;
	}
}
