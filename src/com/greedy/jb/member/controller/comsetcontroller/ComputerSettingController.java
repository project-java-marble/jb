package com.greedy.jb.member.controller.comsetcontroller;

import com.greedy.jb.member.model.dto.CharacterDTO;
import com.greedy.jb.member.model.service.ComputerSetService;

public class ComputerSettingController {
	
	private final ComputerSetService computerSetService;
	
	public ComputerSettingController() {
		
		computerSetService = new ComputerSetService();
	}
	
	public CharacterDTO setComCharacter(int charNum) {
		
		CharacterDTO charDTO = computerSetService.setComCharacter(charNum);
		
		return charDTO;
	}
	
}
