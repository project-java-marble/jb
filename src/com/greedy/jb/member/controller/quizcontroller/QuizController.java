package com.greedy.jb.member.controller.quizcontroller;

import com.greedy.jb.member.model.dto.QuizDTO;
import com.greedy.jb.member.model.service.QuizService;

/**
 * <pre>
 * Class : QuizController
 * Comment : 요청받은 것을 처리해주는 역할
 * History
 * 2021/12/15 석원탁 작성
 * </pre>
 * @version 1
 * @author 석원탁
 */

public class QuizController {

	/* 내가 요청하고 보여줄 값들이 맞는지 확인  기능*/
	
	private QuizDTO quizDTO;
	private QuizService quizService;
	
	public QuizController() {
		
		quizService = new QuizService();

	}
	
	public QuizDTO selectQuiz(QuizDTO quizDTO, String level) {
		
		quizDTO = quizService.selectQuiz(level);
		
		return quizDTO;
		
	}
	
}
