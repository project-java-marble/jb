package com.greedy.jb.member.controller.rankingViewController;

import java.util.List;

import com.greedy.jb.member.model.dto.RankingDTO;
import com.greedy.jb.member.model.service.RankingViewService;
/**
 * <pre>
 * Class : RankingViewController
 * Comment : RankingView로 보낼 데이터 형태 변환
 * History
 * 2021/12/14 (장민주) 처음 작성함
 * 2021/12/16 (장민주) 구현 완료
 * </pre>
 * @author 장민주
 * @version 1.0.0
 * @see RankingView, RankingViewService, RankingDTO
 *
 */
public class RankingViewController {

	/* service로부터 반환받은 데이터를 사용자에게 보여질 형태로 변환 */
	
	/** 
	 * @return rankingList 전달받은 데이터값을 view에서 사용할 수 있도록 DTO를 이용해 형태를 변환
	 */
	public List<RankingDTO> selectRanking() {
		
		RankingViewService rankingViewService = new RankingViewService();
		List<RankingDTO> rankingList = rankingViewService.selectRanking();
		
		return rankingList;
	}
}
