package com.greedy.jb.member.controller.gameinfocontroller;

import java.util.List;

import com.greedy.jb.member.model.dto.RuleDTO;
import com.greedy.jb.member.model.service.GameInfoService;
/**
 * 
 * <pre>
 * Class : GameInfoController
 * Comment : 클래스에 대한 설명
 * History
 * 2021/12/14 (장민주) 처음 작성함
 * 2021/12/16 (장민주) 작성 완료
 * </pre>
 * @author 장민주
 * @version 1.0.0
 * @see GameInfo, GameInfoService
 *
 */
public class GameInfoController {

	/* service로부터 반환받은 데이터를 사용자에게 보여질 형태로 변환 */

	/**
	 * 
	 * @return rules 전달받은 데이터값을 view에서 사용할 수 있도록 DTO를 이용해 형태 변환
	 */
	public List<RuleDTO> selectAllRules() {
		
		GameInfoService gameInfoService = new GameInfoService();
		List<RuleDTO> rules = gameInfoService.selectAllRules();

		return rules;
	}
}
