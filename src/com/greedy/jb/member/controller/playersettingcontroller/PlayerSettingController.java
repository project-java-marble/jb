package com.greedy.jb.member.controller.playersettingcontroller;

import com.greedy.jb.member.model.dto.UserDTO;
import com.greedy.jb.member.model.service.UserService;

/**
 * 
 * <pre>
 * Class : PlayerSettingController
 * Comment : 유저 셋팅 컨트롤러
 * History
 * 2021/12/18 (박인근) 처음 작성함
 * </pre>
 * @author 박인근
 * @version 1.0.0
 * @see UserService
 *
 */
public class PlayerSettingController {
	
	/** 플레이어 셋팅 서비스 */
	private final UserService userService;
	
	/** UserService 초기화 */
	public PlayerSettingController() {
		this.userService = new UserService();
	}
	
	/**
	 * 
	 * @return userService.selectLastUserNO() 등록된 유저의 마지막 회원번호
	 */
	public int selectLastUserNo() {
		
		return userService.selectLastUserNO();
	}
	
	/**
	 * 
	 * @param userID 플레이어 구분
	 * @param userNo 회원번호
	 * @param nickName 닉네임
	 * @param characterNo 캐릭터번호
	 */
	public void setUser(String userID, int userNo, String nickName, int characterNo) {
		
		UserDTO userDTO = new UserDTO();
		
		userDTO.setUserId(userID); 
		userDTO.setUserNo(userNo); 
		userDTO.setPlayId(nickName);
		userDTO.setCharNo(characterNo);
		
		int result = userService.setUser(userDTO);
		
		if(result > 0) {
			System.out.println("유저 등록 성공!");
		} else {
			System.out.println("유저 등록 실패!");
		}
		
		
	}
	
	
}
