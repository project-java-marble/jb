package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.List;

import com.greedy.jb.member.model.dao.ChanceCardDAO;
import com.greedy.jb.member.model.dto.ChanceCardDTO;

/**
 * <pre>
 * Class : ChanceService
 * Comment : 컨트롤러와 DAO와의 상호작요을 위한 클래스
 * History
 * 2021/12/21 (김영광)
 * </pre>
 * @version 1.0.0
 * @author GLORY
 * @see ChanceCardDAO
 * 	  , ChanceService
 * */
public class ChanceService {
    
	/** chanceCardDAO에서 저장한 찬스카드 리스트를 불러오기 위한 변수명 */
	private ChanceCardDAO chanceCardDAO = new ChanceCardDAO();
	
	public List<ChanceCardDTO> selectChanceCard() {
		
		/* DAO에서부터 찬스카드 정보를 리스트에 담아오고 그걸 컨트롤러에 전하기 위한 클래스 */
		
		Connection con = getConnection();
		
		List<ChanceCardDTO> chanceList = chanceCardDAO.chance(con);
		
		close(con);
		
		return chanceList;
	}
}

