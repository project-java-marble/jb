package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.Iterator;
import java.util.Map;

import com.greedy.jb.member.model.dao.ComputerDAO;
import com.greedy.jb.member.model.dto.CharacterDTO;

public class ComputerSetService {
	
	private ComputerDAO comDAO;
	
	public ComputerSetService() {
		
		comDAO = new ComputerDAO();
	}
	
	public CharacterDTO setComCharacter(int charNum) {
		
		Connection con = getConnection();
		
		Map<Integer, String> comCharacter = comDAO.selectCharacter(con, charNum);
		
		int key = 0;
		Iterator<Integer> keys = comCharacter.keySet().iterator();
		
		if(keys.hasNext()) {
			
			key = keys.next();
		}
		
		close(con);
		
		CharacterDTO computerCharacter = new CharacterDTO(key, comCharacter.get(key));
		
		return computerCharacter;
	}
	
}
