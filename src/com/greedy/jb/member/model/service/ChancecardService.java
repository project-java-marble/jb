package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.getConnection;
import static com.greedy.jb.common.JDBCTemplate.close;

import java.sql.Connection;
import java.util.List;

import com.greedy.jb.member.model.dao.ChancecheckDAO;
import com.greedy.jb.member.model.dto.ChanceCardDTO;


public class ChancecardService {

	private ChancecheckDAO chancecardDAO = new ChancecheckDAO();
	
	public List<ChanceCardDTO> chanceView() {

		Connection con = getConnection();
		
		List<ChanceCardDTO> chancecardlist = chancecardDAO.chanceView(con);
		
		close(con);
		
		return chancecardlist; 
	}
}
