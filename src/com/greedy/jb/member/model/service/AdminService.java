package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.*;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import com.greedy.jb.member.model.dao.AdminDAO;
/**
 * 
 * <pre>
 * Class :  AdminService
 * Comment : DAO와 view의 Connection을 연결해 줄 클래스
 * History
 * 2021/12/20 박성준 처음 작성함
 * </pre>
 * @author 박성준
 * @version 1.0.0
 * @see AdminView, AdminDAO, AdminService
 */
public class AdminService {

/* AdminDAO 인스턴스 선언 */
private AdminDAO adminDAO = new AdminDAO();
   
	/**
	 * DB에 값을 전달할 메소드의 실행 결과를 con에 담아주고 DB에 입출력 되었는지 확인 하기 위한 result 값 초기화
	 * 값이 잘 전달 되었으면 commit 아니면 rollback 하고 Connection을 닫아주고 결과를 반환할 메소드
	 * @return DB에 입출력 확인을 위한 반환값
	 */
   public int selectAdmin() {
   
      Connection con = getConnection();	 
      
      int result = 0; 					
      
      result = adminDAO.loginAdmin(con);
      
      if(result > 0) {
         commit(con);
      } else {
         rollback(con);
      }
      
      close(con);
      
      return result;
   }
}