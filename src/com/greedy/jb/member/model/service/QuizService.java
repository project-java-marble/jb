package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;

import com.greedy.jb.member.model.dao.QuizDAO;
import com.greedy.jb.member.model.dto.QuizDTO;

/**
 * <pre>
 * Class : QuizService
 * Comment : Connection을 관리해준다.
 * History
 * 2021/12/15 석원탁 작성
 * </pre>
 * @version 1
 * @author 석원탁
 */

public class QuizService {
	
	/* Connection을 관리 */

	private QuizDAO quizDAO = new QuizDAO();
	private QuizDTO quizDTO;
	
	public QuizDTO selectQuiz(String level) {

		quizDTO = new QuizDTO();

		Connection con = getConnection();
		
		quizDTO = quizDAO.selectQuiz(con, level);
		
		close(con);
		
		return quizDTO;
		
		
	}

	
}
