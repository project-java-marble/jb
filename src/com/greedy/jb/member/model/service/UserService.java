package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.commit;
import static com.greedy.jb.common.JDBCTemplate.getConnection;
import static com.greedy.jb.common.JDBCTemplate.rollback;

import java.sql.Connection;

import com.greedy.jb.member.model.dao.UserDAO;
import com.greedy.jb.member.model.dto.UserDTO;

public class UserService {
	
	private UserDAO userDAO;
	
	public UserService() {
		this.userDAO = new UserDAO();
	}
	
	/* 마지막 플레이어의 USER_NO 값 가져오기 */
	public int selectLastUserNO() {
		
		Connection con = getConnection();
		
		int lastUserNo = userDAO.selectLastUserNo(con);
		
		close(con);
		
		return lastUserNo;
	}

	public int setUser(UserDTO userDTO) {
		
		Connection con = getConnection();
		
		int result = 0;
		
		int setUserResult = userDAO.insertUser(con, userDTO);
		
		if(setUserResult > 0) {
			result = 1;
			commit(con);
		} else {
			rollback(con);
		}
		
		close(con);
		
		return result;
	}
	
}
