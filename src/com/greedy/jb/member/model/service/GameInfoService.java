package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.List;

import com.greedy.jb.member.model.dao.RuleDAO;
import com.greedy.jb.member.model.dto.RuleDTO;

public class GameInfoService {

	private RuleDAO ruleDAO = new RuleDAO();
	
	public List<RuleDTO> selectAllRules() {

		Connection con = getConnection();
		
		List<RuleDTO> rules = ruleDAO.selectAllRules(con);
		
		close(con);
		
		return rules;
	}

}
