package com.greedy.jb.member.model.service;

import static com.greedy.jb.common.JDBCTemplate.close;
import static com.greedy.jb.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.util.List;

import com.greedy.jb.member.model.dao.RankingDAO;
import com.greedy.jb.member.model.dto.RankingDTO;


public class RankingViewService {

	private RankingDAO rankingDAO = new RankingDAO();
	
	public List<RankingDTO> selectRanking() {
	
		Connection con = getConnection();
		
		List<RankingDTO> rankingList = rankingDAO.selectRankings(con);
		
		close(con);
		
		return rankingList;
	}

}
