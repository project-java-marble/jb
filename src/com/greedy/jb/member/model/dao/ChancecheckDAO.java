package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.jb.member.model.dto.ChanceCardDTO;

/**
 * <pre>
 * Class : ChancecheckDAO
 * Comment : 찬스카드리스트를 받아오기 위한 DAO
 * History
 * 2021/12/20 (이용선) 처음 작성함
 * </pre>
 * @author 작성자
 * @version 1.0.0
 * @see 
 * */
public class ChancecheckDAO {

	private Properties prop = new Properties();
	
	/* xml 파일을 읽어와 사용할 준비작업 */
	public ChancecheckDAO() {
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param chancecardlist 찬스카드 DTO에서 
	 * @param 매개변수명 매개변수에 대한 설명
	 * @return return값에 대한 설명
	 * @exception 예외 이유에 대한 설명
	 */
	public List<ChanceCardDTO> chanceView(Connection con) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		List<ChanceCardDTO> chancecardlist = null;
		
		String query = prop.getProperty("chanceView");
		
		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();
			
			/* 리스트로 묶어줌 */
			chancecardlist = new ArrayList<>();
			
			while(rset.next()) {
				ChanceCardDTO chance = new ChanceCardDTO();
				chance.setChanceNo(rset.getString("chanceNO"));
				chance.setChanceType(rset.getString("chanceType"));
				chance.setChanceTrigger(rset.getString("chanceTrigger"));
				
				chancecardlist.add(chance);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return chancecardlist;
		
	}

	
	
}
