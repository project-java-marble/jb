package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.jb.member.model.dto.UserDTO;
/**
 * 
 * <pre>
 * Class : AdminDAO
 * Comment : DB에 쿼리문을 입출력해서 값을 반환 받기 위함
 * History
 * 2021/12/20
 * </pre>
 * @author 박성준
 * @version 1.0.0
 * @see AdminView, AdminService
 */
public class AdminDAO {
   
   /* DB와 연결하기 위한 Properties prop 선언 */
   private Properties prop;
   
   public AdminDAO() {
      
	   /* DB와 연결하기 위해 Properties 객체 생성하고 쿼리문을 properties와 연결 */
      prop = new Properties() ;
      
      /* Prop에 member-query문 입출력 하기 위해 연결하고 예외상황 추적*/
      try {
         prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
      } catch (InvalidPropertiesFormatException e) {
         e.printStackTrace();
      } catch (FileNotFoundException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      } 
   }
   
   /**
    * 서비스에서 사용 할 메소드
    * @param con DB와 연결할 커넥션 인스턴스를 받아오기 위함
    * @return 연결된 결과값 반환
    * @exception SQL문에서 발생할 에러 추적
    */
   public int loginAdmin(Connection con) {
      
	  /* 쿼리문을 담줄 PreparedStatement와 결과값을 담을 ResultSet 선언 */
      PreparedStatement pstmt = null;
      ResultSet rset = null;
      
      /* looinAdmin 쿼리문을 담을 쿼리라는 변수 선언 */
      String query = prop.getProperty("loginAdmin");
      
      int result = 0;	// DB에 담겼는지 확인해 보기 위해 result 선언 및 초기화
      try {
         pstmt = con.prepareStatement(query);								// 커넥션을 연결하고 pstmt에 쿼리 담기
         rset = pstmt.executeQuery();										// pstmt를 실행하고 결과를 rset에 담기 
         if(rset.next()) {													// rset에 값이 있으면 if문 실행, 반대면 else문 실행 
            System.out.println(rset.getString("ADMIN_ID") + "님 환영합니다.");
         } else {
            System.out.println("관리자 정보가 없습니다.");
         }
      } catch (SQLException e) {
         e.printStackTrace();
      } finally {
		 close(rset);							// Connection과 연결된 rset 닫아줌
		 close(pstmt);							// Connection과 연결된 pstmt 닫아줌
      }
      return result;
   }
}