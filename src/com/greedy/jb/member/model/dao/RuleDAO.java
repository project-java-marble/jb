package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.jb.member.model.dto.RuleDTO;

public class RuleDAO {

	private Properties prop = new Properties();

	public RuleDAO() {

		try {
			/** 쿼리문을 호출하기 위해 쿼리문을 담은 xml파일 로드 */
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<RuleDTO> selectAllRules(Connection con) {

		/** 실행할 쿼리문 selectAllRules 세팅 */
		PreparedStatement pstmt = null;
		ResultSet rset = null;

		String query = prop.getProperty("selectAllRules");

		List<RuleDTO> ruleList = null;

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			/** 모든 데이터 값을 DTO에 set한 뒤 arrayList 형태로 반환받기 */
			ruleList = new ArrayList<>();

			while(rset.next()) {
				RuleDTO rule = new RuleDTO();
				rule.setRuleNo(rset.getInt("RULE_NO"));
				rule.setRuleContent(rset.getString("RULE_CONTENT"));

				ruleList.add(rule);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}

		return ruleList;
	}

}
