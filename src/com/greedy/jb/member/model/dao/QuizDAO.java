package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.awt.Font;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.jb.member.model.dto.QuizDTO;

/**
 * <pre>
 * Class : QuizDAO
 * Comment : 난이도를 QuizPage(view)에서 전달받아 그 중 1개를 뽑아주는 것
 * History
 * 2021/12/19 석원탁 작성
 * </pre>
 * @version 1
 * @author 석원탁
 */

public class QuizDAO {

	/* xml의 쿼리문을 통해서 db가져오고 와서 원하는 값 조회  */
	
	private Properties prop;
	
	/* 쿼리문을 불러오는 기능 */
	public QuizDAO() {
		prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/* 난수를 이용해 어떤 문제가 출력되는지에 대한 기능  */
	public QuizDTO selectQuiz(Connection con, String level) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectQuiz");
		
		QuizDTO quizDTO = new QuizDTO();
		
		int random = (int) (Math.random() * 20) + 1;							// 랜덤으로 문제 뽑을 수 있게 하기 위한 난수 생성문

		
		String str = level;														
		
		if(random < 10) {

			str += "_0" + String.valueOf(random);
		} else {
			
			str += "_" + String.valueOf(random);
		}
			
		try {
			pstmt = con.prepareStatement(query);			
			pstmt.setString(1, level);											// view에서 level(유형)을 랜덤으로 출력해줌
			pstmt.setString(2, str);											// str(20문제中1개) 출력해줌
			
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				quizDTO.setQuizType(rset.getString("QUIZ_TYPE"));
				quizDTO.setQuizQuestion(rset.getString("QUIZ_QUESTION"));
				quizDTO.setAnswer(rset.getString("QUIZ_ANSWER"));
		
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
			close(pstmt);
			close(rset);
			
			
		}
			return quizDTO ;
	}
	

}
