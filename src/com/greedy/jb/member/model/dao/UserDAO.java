package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import com.greedy.jb.member.model.dto.UserDTO;

public class UserDAO {
	
	private Properties prop;
	
	public UserDAO() {
		prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public int selectLastUserNo(Connection con) {
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectLastUserNo");
		
		int lastUserNo = 0;
		
		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();
			
			
			while(rset.next()) {
				lastUserNo = rset.getInt("MAX(USER_NO)");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return lastUserNo;
	}

	public int insertUser(Connection con, UserDTO userDTO) {
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		String query= prop.getProperty("insertNewUser");
		
		try {
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, userDTO.getUserId());
			pstmt.setInt(2, userDTO.getUserNo());
			pstmt.setString(3, userDTO.getAdminId());
			pstmt.setString(4, userDTO.getAdmimPwd());
			pstmt.setString(5, userDTO.getPlayId());
			pstmt.setInt(6, userDTO.getCharNo());
			
			result = pstmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
		}
		
		return result;
	}
	
}
