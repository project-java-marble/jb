package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;

public class ComputerDAO {
	
	private Properties prop;
	
	public ComputerDAO() {
		prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Map selectCharacter(Connection con, int characterNum) {
		
		Map<Integer, String> comCharacter = new HashMap<>();
		
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		String query = prop.getProperty("selectCharacter");
		System.out.println("DAO에 넘어온 값 : " + characterNum);
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, characterNum);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				comCharacter.put(rset.getInt("PLAY_ID"), rset.getString("CHAR_NAME"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}
		
		return comCharacter;
	}
	
}
