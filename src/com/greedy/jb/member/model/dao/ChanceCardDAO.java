package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import com.greedy.jb.member.model.dto.ChanceCardDTO;
/**
 * <pre>
 * Class : ChanceCardDAO
 * Comment : DB에서 찬크카드 번호, 내용 , 사용구분을 리스트에 담는 로직이 있는 클래스
 * History
 * 2021/12/20 (김영광) 처음 작성
 * </pre>
 * @version 1.0.0
 * @author GLORY
 * @see ChanceService
 * */
public class ChanceCardDAO {

	/** xml에서 찬스카드 정보연결을 위한 것 */
	private Properties prop = new Properties();
	
	/* 오라클과 연동을 위한 것 */
	public ChanceCardDAO() {
	
		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/* 찬스카드 테이블 행 전체 불러와서 담기  */
	public List<ChanceCardDTO> chance(Connection con){
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
	
		String query = prop.getProperty("chanceCard");
		
		List<ChanceCardDTO> chanceList = null;
		
		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();
			
			chanceList = new ArrayList<>();
			
			if(rset.next()) {
				
				ChanceCardDTO chanceCard = new ChanceCardDTO();
				
				chanceCard.setChanceNo(rset.getString("CHANCE_NO"));
				chanceCard.setChanceType(rset.getString("CHANCETYPE"));
				chanceCard.setChanceTrigger(rset.getString("TRIGGER"));
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
		}
			return chanceList;	
	}


	
	
}
