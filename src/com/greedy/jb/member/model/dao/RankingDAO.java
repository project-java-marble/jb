package com.greedy.jb.member.model.dao;

import static com.greedy.jb.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;

import com.greedy.jb.member.model.dto.RankingDTO;

public class RankingDAO {

	private Properties prop = new Properties();

	public RankingDAO() {

		try {
			prop.loadFromXML(new FileInputStream("mapper/member-query.xml"));
		} catch (InvalidPropertiesFormatException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<RankingDTO> selectRankings(Connection con) {

		PreparedStatement pstmt = null;
		ResultSet rset = null;

		List<RankingDTO> rankingList = null;

		String query = prop.getProperty("selectRanking");

		try {
			pstmt = con.prepareStatement(query);
			rset = pstmt.executeQuery();

			rankingList = new ArrayList<>();
			while(rset.next()) {
				RankingDTO ranking = new RankingDTO();
				ranking.setPlayId(rset.getString("PLAY_ID"));
				ranking.setRateOfCorrectAnswer(rset.getInt("ANS_PERCENT"));
				ranking.setPlayerRank(rset.getInt("PLAYER_RANK"));
				ranking.setPlayerHead(rset.getInt("PLAYER_HEAD"));

				rankingList.add(ranking);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(rset);
		}

		return rankingList;
	}

}
