package com.greedy.jb.member.model.dto;

public class CharacterDTO {

   private Integer playId;
   private String charName;
   
   
   public CharacterDTO(Integer playId, String charName) {
      super();
      this.playId = playId;
      this.charName = charName;
   }


   public Integer getPlayId() {
      return playId;
   }


   public void setPlayId(Integer playId) {
      this.playId = playId;
   }


   public String getCharName() {
      return charName;
   }


   public void setCharName(String charName) {
      this.charName = charName;
   }


   @Override
   public String toString() {
      return "CharacterDTO [playId=" + playId + ", charName=" + charName + "]";
   }
   


}