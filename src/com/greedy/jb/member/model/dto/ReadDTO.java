package com.greedy.jb.member.model.dto;

public class ReadDTO {
	
	private String quizCode;
	private int readList;
	private String readContent;
	
	private ReadDTO() {}

	public ReadDTO(String quizCode, int readList, String readContent) {
		super();
		this.quizCode = quizCode;
		this.readList = readList;
		this.readContent = readContent;
	}

	public String getQuizCode() {
		return quizCode;
	}

	public void setQuizCode(String quizCode) {
		this.quizCode = quizCode;
	}

	public int getReadList() {
		return readList;
	}

	public void setReadList(int readList) {
		this.readList = readList;
	}

	public String getReadContent() {
		return readContent;
	}

	public void setReadContent(String readContent) {
		this.readContent = readContent;
	}

	@Override
	public String toString() {
		return "ReadDTO [quizCode=" + quizCode + ", readList=" + readList + ", readContent=" + readContent + "]";
	}
	
}
