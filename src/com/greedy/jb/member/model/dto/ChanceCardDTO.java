package com.greedy.jb.member.model.dto;

public class ChanceCardDTO {

   private String chanceNo;
   private String chanceType;
   private String chanceTrigger;
   
   
   public ChanceCardDTO(String chanceNo, String chanceType, String chanceTrigger) {
      super();
      this.chanceNo = chanceNo;
      this.chanceType = chanceType;
      this.chanceTrigger = chanceTrigger;
   }


   public ChanceCardDTO() {
}


public String getChanceNo() {
      return chanceNo;
   }


   public void setChanceNo(String chanceNo) {
      this.chanceNo = chanceNo;
   }


   public String getChanceType() {
      return chanceType;
   }


   public void setChanceType(String chanceType) {
      this.chanceType = chanceType;
   }


   public String getChanceTrigger() {
      return chanceTrigger;
   }


   public void setChanceTrigger(String chanceTrigger) {
      this.chanceTrigger = chanceTrigger;
   }


   @Override
   public String toString() {
      return "ChanceCardDTO [chanceNo=" + chanceNo + ", chanceType=" + chanceType + ", chanceTrigger=" + chanceTrigger
            + "]";
   }
   
   
   
   
}