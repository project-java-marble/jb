package com.greedy.jb.member.model.dto;

public class LandDTO {

	private int landNo;
	private String landType;
	private int landStar;
	
	public LandDTO() {}

	public LandDTO(int landNo, String landType, int landStar) {
		super();
		this.landNo = landNo;
		this.landType = landType;
		this.landStar = landStar;
	}

	public int getLandNo() {
		return landNo;
	}

	public void setLandNo(int landNo) {
		this.landNo = landNo;
	}

	public String getLandType() {
		return landType;
	}

	public void setLandType(String landType) {
		this.landType = landType;
	}

	public int getLandStar() {
		return landStar;
	}

	public void setLandStar(int landStar) {
		this.landStar = landStar;
	}

	@Override
	public String toString() {
		return "LandDTO [landNo=" + landNo + ", landType=" + landType + ", landStar=" + landStar + "]";
	}
	
}
