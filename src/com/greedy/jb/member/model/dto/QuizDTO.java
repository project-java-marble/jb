package com.greedy.jb.member.model.dto;

/**
 * <pre>
 * Class : QuizDTO
 * Comment : DB에 있는 QUIZ관련 정보를 JAVA 형식으로 바꿔줌
 * History
 * 2021/12/15 석원탁 작성
 * </pre>
 * @version 1
 * @author 석원탁
 */

public class QuizDTO {
	
	/* DB에있는 QUIZ관련 정보를 JAVA형식으로 바꿔줌*/
	
	private String quizCode;
	private String quizType;
	private String quizQuestion;
	private int score;
	private String answer;
	
	public QuizDTO() {}

	public QuizDTO(String quizCode, String quizType, String quizQuestion, int score, String answer) {
		super();
		this.quizCode = quizCode;
		this.quizType = quizType;
		this.quizQuestion = quizQuestion;
		this.score = score;
		this.answer = answer;
	}

	public String getQuizCode() {
		return quizCode;
	}

	public void setQuizCode(String quizCode) {
		this.quizCode = quizCode;
	}

	public String getQuizType() {
		return quizType;
	}

	public void setQuizType(String quizType) {
		this.quizType = quizType;
	}

	public String getQuizQuestion() {
		return quizQuestion;
	}

	public void setQuizQuestion(String quizQuestion) {
		this.quizQuestion = quizQuestion;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public String toString() {
		return "QuizDTO [quizCode=" + quizCode + ", quizType=" + quizType + ", quizQuestion=" + quizQuestion
				+ ", score=" + score + ", answer=" + answer + "]";
	}
	
	
}
