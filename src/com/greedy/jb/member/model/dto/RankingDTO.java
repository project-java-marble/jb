package com.greedy.jb.member.model.dto;

public class RankingDTO {

	private String playId;
	private int rateOfCorrectAnswer;
	private int playerRank;
	private int playerHead;
	
	public RankingDTO() {}

	public RankingDTO(String playId, int rateOfCorrectAnswer, int playerRank, int playerHead) {
		super();
		this.playId = playId;
		this.rateOfCorrectAnswer = rateOfCorrectAnswer;
		this.playerRank = playerRank;
		this.playerHead = playerHead;
	}

	public String getPlayId() {
		return playId;
	}

	public int getRateOfCorrectAnswer() {
		return rateOfCorrectAnswer;
	}

	public int getPlayerRank() {
		return playerRank;
	}

	public int getPlayerHead() {
		return playerHead;
	}

	public void setPlayId(String playId) {
		this.playId = playId;
	}

	public void setRateOfCorrectAnswer(int rateOfCorrectAnswer) {
		this.rateOfCorrectAnswer = rateOfCorrectAnswer;
	}

	public void setPlayerRank(int playerRank) {
		this.playerRank = playerRank;
	}

	public void setPlayerHead(int playerHead) {
		this.playerHead = playerHead;
	}

	@Override
	public String toString() {
		return playId + ": \n 정답률 " + rateOfCorrectAnswer + "% | 순위 " + playerRank + "/" + playerHead;
	}
	
	
}
