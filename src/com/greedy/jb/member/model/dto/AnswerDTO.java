package com.greedy.jb.member.model.dto;

public class AnswerDTO {

   private String ansPlayer;
   private Integer ansNo;
   private String quizCode;
   private Integer readList;
   private Integer userNo;
   
   
   public AnswerDTO(String ansPlayer, Integer ansNo, String quizCode, Integer readList, Integer userNo) {
      super();
      this.ansPlayer = ansPlayer;
      this.ansNo = ansNo;
      this.quizCode = quizCode;
      this.readList = readList;
      this.userNo = userNo;
   }


   public String getAnsPlayer() {
      return ansPlayer;
   }


   public void setAnsPlayer(String ansPlayer) {
      this.ansPlayer = ansPlayer;
   }


   public Integer getAnsNo() {
      return ansNo;
   }


   public void setAnsNo(Integer ansNo) {
      this.ansNo = ansNo;
   }


   public String getQuizCode() {
      return quizCode;
   }


   public void setQuizCode(String quizCode) {
      this.quizCode = quizCode;
   }


   public Integer getReadList() {
      return readList;
   }


   public void setReadList(Integer readList) {
      this.readList = readList;
   }


   public Integer getUserNo() {
      return userNo;
   }


   public void setUserNo(Integer userNo) {
      this.userNo = userNo;
   }


   @Override
   public String toString() {
      return "AnswerDTO [ansPlayer=" + ansPlayer + ", ansNo=" + ansNo + ", quizCode=" + quizCode + ", readList="
            + readList + ", userNo=" + userNo + "]";
   }
   
      
   

}