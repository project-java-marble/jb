package com.greedy.jb.member.model.dto;

public class UserDTO {
	
	private String userId;
	private int userNo;
	private String adminId;
	private String admimPwd;
	private String playId;
	private Integer charNo;
	
	public UserDTO() {}

	public UserDTO(String userId, int userNo, String adminId, String admimPwd, String playId, Integer charNo) {
		super();
		this.userId = userId;
		this.userNo = userNo;
		this.adminId = adminId;
		this.admimPwd = admimPwd;
		this.playId = playId;
		this.charNo = charNo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getAdmimPwd() {
		return admimPwd;
	}

	public void setAdmimPwd(String admimPwd) {
		this.admimPwd = admimPwd;
	}

	public String getPlayId() {
		return playId;
	}

	public void setPlayId(String playId) {
		this.playId = playId;
	}

	public Integer getCharNo() {
		return charNo;
	}

	public void setCharNo(Integer charNo) {
		this.charNo = charNo;
	}

	@Override
	public String toString() {
		return "UserDTO [userId=" + userId + ", userNo=" + userNo + ", adminId=" + adminId + ", admimPwd=" + admimPwd
				+ ", playId=" + playId + ", charNo=" + charNo + "]";
	}
	
}
