package com.greedy.jb.member.model.dto;

public class GameResultDTO {

	private int userNo;
	private int playerHead;
	private int ansPercent;
	private int ansAll;
	private String playerRank;
	private int failQuiz;
	private String playDate;
	
	public GameResultDTO() {}

	public GameResultDTO(int userNo, int playerHead, int ansPercent, int ansAll, String playerRank, int failQuiz,
			String playDate) {
		super();
		this.userNo = userNo;
		this.playerHead = playerHead;
		this.ansPercent = ansPercent;
		this.ansAll = ansAll;
		this.playerRank = playerRank;
		this.failQuiz = failQuiz;
		this.playDate = playDate;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public int getPlayerHead() {
		return playerHead;
	}

	public void setPlayerHead(int playerHead) {
		this.playerHead = playerHead;
	}

	public int getAnsPercent() {
		return ansPercent;
	}

	public void setAnsPercent(int ansPercent) {
		this.ansPercent = ansPercent;
	}

	public int getAnsAll() {
		return ansAll;
	}

	public void setAnsAll(int ansAll) {
		this.ansAll = ansAll;
	}

	public String getPlayerRank() {
		return playerRank;
	}

	public void setPlayerRank(String playerRank) {
		this.playerRank = playerRank;
	}

	public int getFailQuiz() {
		return failQuiz;
	}

	public void setFailQuiz(int failQuiz) {
		this.failQuiz = failQuiz;
	}

	public String getPlayDate() {
		return playDate;
	}

	public void setPlayDate(String playDate) {
		this.playDate = playDate;
	}

	@Override
	public String toString() {
		return "GameResultDTO [userNo=" + userNo + ", playerHead=" + playerHead + ", ansPercent=" + ansPercent
				+ ", ansAll=" + ansAll + ", playerRank=" + playerRank + ", failQuiz=" + failQuiz + ", playDate="
				+ playDate + "]";
	}
	
}
