package com.greedy.jb.member.model.dto;

public class RuleDTO {

	private int ruleNo;
	private String ruleContent;
	
	public RuleDTO() {}

	public RuleDTO(int ruleNo, String ruleContent) {
		super();
		this.ruleNo = ruleNo;
		this.ruleContent = ruleContent;
	}

	public int getRuleNo() {
		return ruleNo;
	}

	public String getRuleContent() {
		return ruleContent;
	}

	public void setRuleNo(int ruleNo) {
		this.ruleNo = ruleNo;
	}

	public void setRuleContent(String ruleContent) {
		this.ruleContent = ruleContent;
	}

	/* 게임설명 화면에 출력될 양식 지정 */
	@Override
	public String toString() {
		return ruleNo + ". " + ruleContent;
	}
	
	
}
