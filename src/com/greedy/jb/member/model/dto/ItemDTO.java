package com.greedy.jb.member.model.dto;

public class ItemDTO {

	private String itemNo;
	private String itemName;
	private int itemPrice;
	
	public ItemDTO() {}

	public ItemDTO(String itemNo, String itemName, int itemPrice) {
		super();
		this.itemNo = itemNo;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(int itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "ItemDTO [itemNo=" + itemNo + ", itemName=" + itemName + ", itemPrice=" + itemPrice + "]";
	}
	
	
	
	
}
