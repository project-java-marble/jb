package com.greedy.jb.member.model.dto;

public class InventoryDTO {

	private int invenNo;
	private String invenDivision;
	private int userNo;
	private String chanceNo;
	private Integer landNo;
	private String itemNo;
	
	public InventoryDTO() {}

	public InventoryDTO(int invenNo, String invenDivision, int userNo, String chanceNo, Integer landNo, String itemNo) {
		super();
		this.invenNo = invenNo;
		this.invenDivision = invenDivision;
		this.userNo = userNo;
		this.chanceNo = chanceNo;
		this.landNo = landNo;
		this.itemNo = itemNo;
	}

	public int getInvenNo() {
		return invenNo;
	}

	public void setInvenNo(int invenNo) {
		this.invenNo = invenNo;
	}

	public String getInvenDivision() {
		return invenDivision;
	}

	public void setInvenDivision(String invenDivision) {
		this.invenDivision = invenDivision;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getChanceNo() {
		return chanceNo;
	}

	public void setChanceNo(String chanceNo) {
		this.chanceNo = chanceNo;
	}

	public Integer getLandNo() {
		return landNo;
	}

	public void setLandNo(Integer landNo) {
		this.landNo = landNo;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	@Override
	public String toString() {
		return "InventoryDTO [invenNo=" + invenNo + ", invenDivision=" + invenDivision + ", userNo=" + userNo
				+ ", chanceNo=" + chanceNo + ", landNo=" + landNo + ", itemNo=" + itemNo + "]";
	}
	
}
