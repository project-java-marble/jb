package com.greedy.jb.run;

import com.greedy.jb.member.views.MainFrame;
/** 
 * <pre>
 *Class : Application
 *comment : 프로그램 실행부
 *History
 *2021/12/13 (박성준) 처음 작성
 * * </pre>*
 * @version 1.0.0
 * @author 박성준
 * @see MainFrame
 */
public class Application {

	public static void main(String[] args) {

		/* mainframe을 호출 */
		new MainFrame();
	}
}
