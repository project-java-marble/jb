DROP TABLE JB_CHANCE_CARD;

CREATE TABLE JB_CHANCE_CARD
(
    CHANCE_NO    CHAR(1) NOT NULL,
    CHANCE_TYPE    VARCHAR2(150) NOT NULL,
    CHANCE_TRIGGER    CHAR(1) NOT NULL
);

COMMENT ON COLUMN JB_CHANCE_CARD.CHANCE_NO IS '찬스카드번호';

COMMENT ON COLUMN JB_CHANCE_CARD.CHANCE_TYPE IS '찬스카드내용';

COMMENT ON COLUMN JB_CHANCE_CARD.CHANCE_TRIGGER IS '즉시발동여부';

COMMENT ON TABLE JB_CHANCE_CARD IS '찬스카드';

CREATE UNIQUE INDEX 엔터티1_PK8 ON JB_CHANCE_CARD
( CHANCE_NO );

ALTER TABLE JB_CHANCE_CARD
 ADD CONSTRAINT 엔터티1_PK8 PRIMARY KEY ( CHANCE_NO )
 USING INDEX 엔터티1_PK8;


DROP TABLE JB_CHAR;

CREATE TABLE JB_CHAR
(
    PLAY_ID    INTEGER NOT NULL,
    CHAR_NAME    VARCHAR2(20) NOT NULL
);

COMMENT ON COLUMN JB_CHAR.PLAY_ID IS '캐릭터번호';

COMMENT ON COLUMN JB_CHAR.CHAR_NAME IS '캐릭터이름';

COMMENT ON TABLE JB_CHAR IS '캐릭터';

CREATE UNIQUE INDEX 엔터티1_PK9 ON JB_CHAR
( PLAY_ID );

ALTER TABLE JB_CHAR
 ADD CONSTRAINT 엔터티1_PK9 PRIMARY KEY ( PLAY_ID )
 USING INDEX 엔터티1_PK9;


DROP TABLE JB_GAME_RESULT;

CREATE TABLE JB_GAME_RESULT
(
    USER_NO    INTEGER NOT NULL,
    PLAYER_HEAD    INTEGER NOT NULL,
    ANS_PERCENT    INTEGER NOT NULL,
    ANS_ALL    INTEGER NOT NULL,
    PLAYER_RANK    VARCHAR2(5) NOT NULL,
    FAIL_QUIZ    INTEGER NOT NULL,
    PLAY_DATE    DATE NOT NULL
);

COMMENT ON COLUMN JB_GAME_RESULT.USER_NO IS '회원번호';

COMMENT ON COLUMN JB_GAME_RESULT.PLAYER_HEAD IS '플레이어수';

COMMENT ON COLUMN JB_GAME_RESULT.ANS_PERCENT IS '정답률';

COMMENT ON COLUMN JB_GAME_RESULT.ANS_ALL IS '총점수';

COMMENT ON COLUMN JB_GAME_RESULT.PLAYER_RANK IS '순위';

COMMENT ON COLUMN JB_GAME_RESULT.FAIL_QUIZ IS '틀린문제개수';

COMMENT ON COLUMN JB_GAME_RESULT.PLAY_DATE IS '플레이날짜';

COMMENT ON TABLE JB_GAME_RESULT IS '게임결과';

CREATE UNIQUE INDEX 엔터티1_PK4 ON JB_GAME_RESULT
( USER_NO );

ALTER TABLE JB_GAME_RESULT
 ADD CONSTRAINT 엔터티1_PK4 PRIMARY KEY ( USER_NO )
 USING INDEX 엔터티1_PK4;


DROP TABLE JB_INVENTORY;

CREATE TABLE JB_INVENTORY
(
    INVEN_NO    INTEGER NOT NULL,
    INVEN_DIVISION    CHAR(24) NOT NULL,
    USER_NO    INTEGER NOT NULL,
    CHANCE_NO    CHAR(1),
    LAND_NO    INTEGER,
    ITEM_NO    CHAR(1)
);

COMMENT ON COLUMN JB_INVENTORY.INVEN_NO IS '인벤토리번호';

COMMENT ON COLUMN JB_INVENTORY.INVEN_DIVISION IS '보유구분';

COMMENT ON COLUMN JB_INVENTORY.USER_NO IS '회원번호';

COMMENT ON COLUMN JB_INVENTORY.CHANCE_NO IS '찬스카드번호';

COMMENT ON COLUMN JB_INVENTORY.LAND_NO IS '땅번호';

COMMENT ON COLUMN JB_INVENTORY.ITEM_NO IS '아이템번호';

COMMENT ON TABLE JB_INVENTORY IS '플레이어인벤토리';

CREATE UNIQUE INDEX 엔터티1_PK1 ON JB_INVENTORY
( USER_NO,INVEN_NO );

ALTER TABLE JB_INVENTORY
 ADD CONSTRAINT 엔터티1_PK1 PRIMARY KEY ( USER_NO,INVEN_NO )
 USING INDEX 엔터티1_PK1;


DROP TABLE JB_ITEM_STORE;

CREATE TABLE JB_ITEM_STORE
(
    ITEM_NO    CHAR(1) NOT NULL,
    ITEM_NAME    VARCHAR2(150) NOT NULL,
    ITEM_PRICE    INTEGER NOT NULL
);

COMMENT ON COLUMN JB_ITEM_STORE.ITEM_NO IS '아이템번호';

COMMENT ON COLUMN JB_ITEM_STORE.ITEM_NAME IS '아이템이름';

COMMENT ON COLUMN JB_ITEM_STORE.ITEM_PRICE IS '아이템가격';

COMMENT ON TABLE JB_ITEM_STORE IS '아이템상점';

CREATE UNIQUE INDEX 엔터티1_PK7 ON JB_ITEM_STORE
( ITEM_NO );

ALTER TABLE JB_ITEM_STORE
 ADD CONSTRAINT 엔터티1_PK7 PRIMARY KEY ( ITEM_NO )
 USING INDEX 엔터티1_PK7;


DROP TABLE JB_LAND;

CREATE TABLE JB_LAND
(
    LAND_NO    INTEGER NOT NULL,
    LAND_TYPE    CHAR(10) NOT NULL,
    LAND_STAR    INTEGER NOT NULL
);

COMMENT ON COLUMN JB_LAND.LAND_NO IS '땅번호';

COMMENT ON COLUMN JB_LAND.LAND_TYPE IS '땅유형';

COMMENT ON COLUMN JB_LAND.LAND_STAR IS '별갯수';

COMMENT ON TABLE JB_LAND IS '땅';

CREATE UNIQUE INDEX 엔터티1_PK5 ON JB_LAND
( LAND_NO );

ALTER TABLE JB_LAND
 ADD CONSTRAINT 엔터티1_PK5 PRIMARY KEY ( LAND_NO )
 USING INDEX 엔터티1_PK5;


DROP TABLE JB_PLAYER_ANS;

CREATE TABLE JB_PLAYER_ANS
(
    ANS_PLAYER    VARCHAR2(30) NOT NULL,
    ANS_NO    INTEGER NOT NULL,
    QUIZ_CODE    VARCHAR2(10) NOT NULL,
    READ_LIST    INTEGER NOT NULL,
    USER_NO    INTEGER NOT NULL
);

COMMENT ON COLUMN JB_PLAYER_ANS.ANS_PLAYER IS '플레이어답안';

COMMENT ON COLUMN JB_PLAYER_ANS.ANS_NO IS '답안지번호';

COMMENT ON COLUMN JB_PLAYER_ANS.QUIZ_CODE IS '문제코드';

COMMENT ON COLUMN JB_PLAYER_ANS.READ_LIST IS '보기번호';

COMMENT ON COLUMN JB_PLAYER_ANS.USER_NO IS '회원번호';

COMMENT ON TABLE JB_PLAYER_ANS IS '플레이어답안지';

CREATE UNIQUE INDEX 엔터티1_PK6 ON JB_PLAYER_ANS
( ANS_NO );

ALTER TABLE JB_PLAYER_ANS
 ADD CONSTRAINT 엔터티1_PK6 PRIMARY KEY ( ANS_NO )
 USING INDEX 엔터티1_PK6;


DROP TABLE JB_QUIZ;

CREATE TABLE JB_QUIZ
(
    QUIZ_CODE    VARCHAR2(10) NOT NULL,
    QUIZ_TYPE    VARCHAR2(15) NOT NULL,
    QUIZ_QUESTION    VARCHAR2(150) NOT NULL,
    QUIZ_SCORE    INTEGER NOT NULL,
    QUIZ_ANSWER    VARCHAR2(30) NOT NULL
);

COMMENT ON COLUMN JB_QUIZ.QUIZ_CODE IS '문제코드';

COMMENT ON COLUMN JB_QUIZ.QUIZ_TYPE IS '문제유형';

COMMENT ON COLUMN JB_QUIZ.QUIZ_QUESTION IS '문제내용';

COMMENT ON COLUMN JB_QUIZ.QUIZ_SCORE IS '난이도별점수';

COMMENT ON COLUMN JB_QUIZ.QUIZ_ANSWER IS '정답';

COMMENT ON TABLE JB_QUIZ IS '문제은행';

CREATE UNIQUE INDEX 엔터티1_PK2 ON JB_QUIZ
( QUIZ_CODE );

ALTER TABLE JB_QUIZ
 ADD CONSTRAINT 엔터티1_PK2 PRIMARY KEY ( QUIZ_CODE )
 USING INDEX 엔터티1_PK2;


DROP TABLE JB_READ;

CREATE TABLE JB_READ
(
    QUIZ_CODE    VARCHAR2(10) NOT NULL,
    READ_LIST    INTEGER NOT NULL,
    READ_CONTENT    VARCHAR2(150) NOT NULL
);

COMMENT ON COLUMN JB_READ.QUIZ_CODE IS '문제코드';

COMMENT ON COLUMN JB_READ.READ_LIST IS '보기번호';

COMMENT ON COLUMN JB_READ.READ_CONTENT IS '보기내용';

COMMENT ON TABLE JB_READ IS '객관식보기';

CREATE UNIQUE INDEX 엔터티1_PK3 ON JB_READ
( QUIZ_CODE,READ_LIST );

ALTER TABLE JB_READ
 ADD CONSTRAINT 엔터티1_PK3 PRIMARY KEY ( QUIZ_CODE,READ_LIST )
 USING INDEX 엔터티1_PK3;


DROP TABLE JB_USER;

CREATE TABLE JB_USER
(
    USER_ID    CHAR(12) NOT NULL,
    USER_NO    INTEGER NOT NULL,
    ADMIN_ID    VARCHAR2(15),
    ADMIN_PWD    VARCHAR2(15),
    PLAY_ID    VARCHAR2(30),
    CHAR_NO    INTEGER
);

COMMENT ON COLUMN JB_USER.USER_ID IS '사용자구분';

COMMENT ON COLUMN JB_USER.USER_NO IS '회원번호';

COMMENT ON COLUMN JB_USER.ADMIN_ID IS '관리자ID';

COMMENT ON COLUMN JB_USER.ADMIN_PWD IS '관리자PWD';

COMMENT ON COLUMN JB_USER.PLAY_ID IS '닉네임';

COMMENT ON COLUMN JB_USER.CHAR_NO IS '캐릭터번호';

COMMENT ON TABLE JB_USER IS '사용자';

CREATE UNIQUE INDEX 엔터티1_PK ON JB_USER
( USER_NO );

ALTER TABLE JB_USER
 ADD CONSTRAINT 엔터티1_PK PRIMARY KEY ( USER_NO )
 USING INDEX 엔터티1_PK;


















-- 테이블 속성 값 
-- 인벤토리 임의 값 1번 
INSERT
  INTO JB_INVENTORY G
(
  G.INVEN_NO, G.INVEN_DIVISION, G.USER_NO 
, G.CHANCE_NO, G.LAND_NO, G.ITEM_NO
)  
VALUES
(
  1, '아이템상점', 1
, '1', 1, '1'     
);


COMMIT;

-- 인벤토리확인

SELECT 
       G.*
  FROM JB_INVENTORY G;

-- 문제은행 퀴즈 '하' 난이도
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EAGY_01', 'BAGIC', 'TRUE와 FALSE는 논리자료형이다'
, 1, 'O'
);
ROLLBACK;

SELECT
       G.*
  FROM JB_QUIZ G;

-- 문제은행 퀴즈 '중' 난이도 
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'NORMAL_01', 'NORMAL', '다음 보기 중 정수가 아닌것은 ?'
, 3, '4'
);

-- 문제은행 퀴즈 '상' 난이도
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_01', 'HARD', 'OOO이란, 변수 또는 상수의 타입을 다른 타입으로 변환하는 것이다.'
, 5, '형변환'
);

-- 객관식보기 노말_1번 난이도 1번 보기문항
INSERT
  INTO JB_READ G
( 
  G.QUIZ_CODE, G.READ_LIST, G.READ_CONTENT
)
VALUES
( 
  'NORMAL_01', 1, '12 + 12 = 24' 
);
  
SELECT
       G.*
  FROM JB_PLAYER_ANS G;

-- 객관식보기 노말_1번 난이도 2번 보기문항
INSERT
  INTO JB_READ G
( 
  G.QUIZ_CODE, G.READ_LIST, G.READ_CONTENT
)
VALUES
( 
  'NORMAL_01', 2, '-12 - 12 = -24' 
);  

-- 객관식보기 노말_1번 난이도 3번 보기문항
INSERT
  INTO JB_READ G
( 
  G.QUIZ_CODE, G.READ_LIST, G.READ_CONTENT
)
VALUES
( 
  'NORMAL_01', 3, 'A + A = 194' 
);

-- 객관식보기 노말_1번 난이도 4번 보기문항
INSERT
  INTO JB_READ G
( 
  G.QUIZ_CODE, G.READ_LIST, G.READ_CONTENT
)
VALUES
( 
  'NORMAL_01', 4, '97 + % = 97%' 
);

COMMIT;


-- 플레이어 답안지 '하' 난이도
INSERT
  INTO JB_PLAYER_ANS G
(
  G.USER_NO, ANS_NO, QUIZ_CODE
, READ_LIST, ANS_PLAYER  
)
VALUES
(
  1, 1, 'BAGIC_01'
, '', 'O'  
);
-- 보기번호 널값 넣기
ALTER TABLE JB_PLAYER_ANS 
MODIFY READ_LIST CONSTRAINT NN_LID NULL;

-- 플레이어 답안지 '중' 난이도
INSERT
  INTO JB_PLAYER_ANS G
(  
  G.USER_NO, ANS_NO, QUIZ_CODE
, READ_LIST, ANS_PLAYER
)
VALUES
(
  1, 2, 'NORMAL_01'
, 4, '4'
);



--------------------------------------------------------------------------------
-- 유저 테이블 값 입력 
INSERT
  INTO JB_USER GP
(
  GP.USER_ID
, GP.USER_NO
, GP.ADMIN_ID
, GP.ADMIN_PWD
, GP.PLAY_ID
, GP.CHAR_NO
)
VALUES
(   
  'PLAYER'
, 1
, ''
, ''
, 'GLORY'
, 2
);

COMMIT;

--------------------------------------------------------------------------------
-- 캐릭터 테이블 값 입력 
INSERT
  INTO JB_CHAR CH
(
  CH.PLAY_ID
, CH.CHAR_NAME
)
VALUES
(   
  2
, '짱아'
);

COMMIT;

--------------------------------------------------------------------------------
-- 땅 테이블 값 입력 
INSERT
  INTO JB_LAND LD
(
  LD.LAND_NO
, LD.LAND_TYPE
, LD.LAND_STAR
)
VALUES
(   
  1
, '기본칸'
, 3
);

COMMIT;

--------------------------------------------------------------------------------
-- 찬스카드 테이블 값 입력 
INSERT
  INTO JB_CHANCE_CARD CC
(
  CC.CHANCE_NO
, CC.CHANCE_TYPE
, CC.CHANCE_TRIGGER
)
VALUES
(   
  1
, '복습지옥 1회 면제'
, 'Y'
);

COMMIT;

--------------------------------------------------------------------------------
-- 아이템상점 테이블 값 입력 
INSERT
  INTO JB_ITEM_STORE ITST
(
  ITST.ITEM_NO
, ITST.ITEM_NAME
, ITST.ITEM_PRICE
)
VALUES
(   
  1
, '점수 5점 획득'
, 5
);

COMMIT;

--------------------------------------------------------------------------------
-- 게임결과 테이블 값 입력 
INSERT
  INTO JB_GAME_RESULT GR
(
  GR.USER_NO
, GR.PLAYER_HEAD
, GR.ANS_PERCENT
, GR.ANS_ALL
, GR.PLAYER_RANK
, GR.FAIL_QUIZ
, GR.PLAY_DATE
)
VALUES
(   
  1
, 3
, 88
, 21
, 1
, 2
, '2021/12/13'
);

COMMIT;


-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
------난이도별 문제 추가

-------------------------------------------------------------------------------
---21.12.17 민주 요청 테이블 추가

ALTER TABLE JB_PLAYER_ANS ADD ANS_CORRECT CHAR(5) DEFAULT'FALSE' NOT NULL;

COMMENT ON COLUMN JB_PLAYER_ANS.ANS_CORRECT IS '정답확인';

-------------------------------------------------------------------------------
--21.12.18 문제 테이블 문제내용 바이트 모자라서 늘려줌
ALTER TABLE JB_QUIZ MODIFY (QUIZ_QUESTION VARCHAR2(350));

-------------------------------------------------------------------------------
-- 중 1번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_01'
     , 'STANDARD_01'
     , 'DBMS와 연동하여 SQL 질의를 실행하는 프로그램을 작성할 때, 올바른 절차를 고르시오.'
     , 3
     , '2'
);

-------------------------------------------------------------------------------
-- 중 2번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_02'
     , 'STANDARD_02'
     , 'Statement 또는 PreparedStatement 객체를 통해 executieQuery()를 호출할 때 리턴되는 객체는?'
     , 3
     , '2'
);

-------------------------------------------------------------------------------
-- 중 3번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_03'
     , 'STANDARD_03'
     , 'DB의 내용을 변경시킬 수 있는 SQL 구문이 아닌것은?'
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 4번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_04'
     , 'STANDARD_04'
     , 'List 인터페이스를 구현한 클래스가 아닌것을 고르시오 '
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 5번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_05'
     , 'STANDARD_05'
     , '다음 중 생성자에 대한 설명으로 옳지 않은 것은?'
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 6번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_06'
     , 'STANDARD_06'
     , '다음 중 오버로딩 조건으로 옳지 않은 것은?'
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 7번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_07'
     , 'STANDARD_07'
     , '다음 중 오버라이딩 조건으로 옳지 않은 것은? '
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 8번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_08'
     , 'STANDARD_08'
     , '자바에서 클래스를 선언할 때 사용하는 자바의 키워드는? '
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 9번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_09'
     , 'STANDARD_09'
     , '다음 중 성격이 다른 것은?'
     , 3
     , '2'
);

-------------------------------------------------------------------------------
-- 중 10번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_10'
     , 'STANDARD_10'
     , '다음 중 변수 선언이 잘못된 것은? '
     , 3
     , '2'
);

-------------------------------------------------------------------------------
-- 중 11번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_11'
     , 'STANDARD_11'
     , '상위 클래스 Pet과 Pet을 상속받는 Dog, Cat 클래스가 있다. 다음 중 업 캐스팅이 발생하는 것은 무엇인가? '
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 12번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_12'
     , 'STANDARD_12'
     , '클래스 외부에서 (         ) 멤버에 접근할 수 없다.'
     , 3
     , '2'
);

-------------------------------------------------------------------------------
-- 중 13번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_13'
     , 'STANDARD_13'
     , '객체를 가리키는 변수에 [ ? ] 를 대입할 수 있다.'
     , 3
     , '3'
);

-------------------------------------------------------------------------------
-- 중 14번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_14'
     , 'STANDARD_14'
     , 'switch의 연산식 결과로 클래스 String 객체와 Byte, Short, Integer,(      )등 의 랩퍼 클래스 객체도 지원한다.
다음 중 빈칸에 들어갈 것은?'
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 15번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_15'
     , 'STANDARD_15'
     , '하나의 클래스에서 인자가 다르면 생성자를 여러개 만들 수 있다. 이러한 특징을 [  ](이)라 한다.'
     , 3
     , '3'
);

-------------------------------------------------------------------------------
-- 중 16번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_16'
     , 'STANDARD_16'
     , '접근 지정자 중에서 클래스 내부에서만 참조할 수 있는 가장 제한적인 참조 지정자 키워드는 무엇인가?'
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 17번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_17'
     , 'STANDARD_17'
     , '다음 보기중 예외 클래스 계층구조에서 예외처리를 해야하는 Checked Exception 을 고르시오'
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 18번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_18'
     , 'STANDARD_18'
     , '다음 중 배열에 대한 옳은 설명이 아닌 것은?'
     , 3
     , '4'
);

-------------------------------------------------------------------------------
-- 중 19번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_19'
     , 'STANDARD_19'
     , '다음 중 추상클래스에 대한 설명으로 옳지 않은 것은?'
     , 3
     , '1'
);

-------------------------------------------------------------------------------
-- 중 20번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'NORMAL_20'
     , 'STANDARD_20'
     , '다음 중 인터페이스에 대한 설명으로 가장 적절하지 않은 것은?'
     , 3
     , '3'
);


COMMIT;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- 중 1번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_01'
     , '1'
     , 'DBMS 연결, SQL 질의 실행, Statement 객체 생성, ResultSet 객체 처리, 연결 종료'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_01'
     , '2'
     , 'DBMS 연결, Statement 객체 생성, SQL 질의 실행, ResultSet 객체 처리, 연결 종료'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_01'
     , '3'
     , 'DBMS 연결, ResultSet 객체 처리, Statement 객체 생성, SQL 질의 실행, 연결 종료'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_01'
     , '4'
     , 'DBMS 연결, Statement 객체 생성, ResultSet 객체 처리, SQL 질의 실행, 연결 종료'
);

-------------------------------------------------------------------------------
-- 중 2번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_02'
     , '1'
     , 'Connection 객체'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_02'
     , '2'
     , 'ResultSet 객체'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_02'
     , '3'
     , 'Statement 객체'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_02'
     , '4'
     , 'PrepareedStatement 객체'
);

-------------------------------------------------------------------------------
-- 중 3번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_03'
     , '1'
     , 'SELECT'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_03'
     , '2'
     , 'INSERT'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_03'
     , '3'
     , 'UPDATE'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_03'
     , '4'
     , 'DELETE'
);

-------------------------------------------------------------------------------
-- 중 4번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_04'
     , '1'
     , 'ArrayList'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_04'
     , '2'
     , 'LinkedList'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_04'
     , '3'
     , 'Stack'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_04'
     , '4'
     , 'Map'
);

-------------------------------------------------------------------------------
-- 중 5번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_05'
     , '1'
     , '모든 생성자의 이름은 클래스의 이름과 동일하다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_05'
     , '2'
     , '클래스에서 기본 생성자는 반드시 하나만 존재 가능하다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_05'
     , '3'
     , '생성자가 없는 클래스는 컴파일러가 기본 생성자를 추가한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_05'
     , '4'
     , '생성자는 오버로딩 할 수 없다.'
);

-------------------------------------------------------------------------------
-- 중 6번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_06'
     , '1'
     , '메서드의 이름이 같아야 한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_06'
     , '2'
     , '매개변수의 갯수나 타입이 달라야 한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_06'
     , '3'
     , '리턴타입은 상관 없다'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_06'
     , '4'
     , '매개변수의 이름이 달라야 한다.'
);

-------------------------------------------------------------------------------
-- 중 7번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_07'
     , '1'
     , '조상의 메서드와 이름이 같아야 한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_07'
     , '2'
     , '매개변수의 수와 타입, 리턴타입이 모두 같아야 한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_07'
     , '3'
     , '접근 제어자는 조상의 메서드보다 넓은 범위로만 변경할 수 있다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_07'
     , '4'
     , '조상의 메서드보다 더 많은 수의 예외를 선언할 수 있다.'
);

COMMIT;

-------------------------------------------------------------------------------
-- 중 8번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_08'
     , '1'
     , 'class'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_08'
     , '2'
     , 'void'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_08'
     , '3'
     , 'interface'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_08'
     , '4'
     , 'public'
);

-------------------------------------------------------------------------------
-- 중 9번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_09'
     , '1'
     , 'ArrayList'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_09'
     , '2'
     , 'Properties'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_09'
     , '3'
     , 'LinkedList'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_09'
     , '4'
     , 'Vector'
);

-------------------------------------------------------------------------------
-- 중 10번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_10'
     , '1'
     , 'int i = 1;'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_10'
     , '2'
     , 'char c = "안녕";'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_10'
     , '3'
     , 'String str = "자바";'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_10'
     , '4'
     , 'boolean b = false;'
);

-------------------------------------------------------------------------------
-- 중 11번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_11'
     , '1'
     , 'Pet p1 = new Cat();'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_11'
     , '2'
     , 'Cat c1 = new Cat();'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_11'
     , '3'
     , 'Dog d1 = new Dog();'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_11'
     , '4'
     , 'Pet p2 = new Pet();'
);

-------------------------------------------------------------------------------
-- 중 12번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_12'
     , '1'
     , 'public'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_12'
     , '2'
     , 'private'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_12'
     , '3'
     , 'property'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_12'
     , '4'
     , 'protected'
);

COMMIT;

-------------------------------------------------------------------------------
-- 중 13번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_13'
     , '1'
     , 'method'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_13'
     , '2'
     , 'static'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_13'
     , '3'
     , 'null'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_13'
     , '4'
     , 'field'
);

-------------------------------------------------------------------------------
-- 중 14번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_14'
     , '1'
     , 'Character'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_14'
     , '2'
     , 'Long'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_14'
     , '3'
     , 'Double'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_14'
     , '4'
     , 'boolean'
);


-------------------------------------------------------------------------------
-- 중 15번문제 객관식 보기
DELETE FROM JB_READ;
COMMIT;

ALTER TABLE JB_READ MODIFY READ_LIST VARCHAR2(3);
COMMIT;

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_15'
     , '1'
     , '생성자 인스턴스'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_15'
     , '2'
     , '생성자 오버라이딩'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_15'
     , '3'
     , '생성자 오버로딩'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_15'
     , '4'
     , '생성자 오토바이'
);

-------------------------------------------------------------------------------
-- 중 16번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_16'
     , '1'
     , 'private'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_16'
     , '2'
     , 'protected'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_16'
     , '3'
     , 'default'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_16'
     , '4'
     , 'public'
);

-------------------------------------------------------------------------------
-- 중 17번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_17'
     , '1'
     , 'NullPointerException'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_17'
     , '2'
     , 'IndexOutOfBoundsException'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_17'
     , '3'
     , 'RuntimeException'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_17'
     , '4'
     , 'FileNotFoundException'
);

-------------------------------------------------------------------------------
-- 중 18번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_18'
     , '1'
     , '데이터들을 연속된 메모리 공간으로 관리할 수 있다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_18'
     , '2'
     , '반복문을 이용하여 값들을 연속처리 해줄 수 있다. '
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_18'
     , '3'
     , '배열의 사이즈는 한 번 설정하고 나면 변경할 수 없다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_18'
     , '4'
     , '더이상 주소를 참조할 수 없는 배열은 즉시 삭제할 수 있다.'
);

-------------------------------------------------------------------------------
-- 중 19번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_19'
     , '1'
     , '추상 메소드를 1개 이상 가지고 있는 클래스이다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_19'
     , '2'
     , '생성자를 가질 수 있다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_19'
     , '3'
     , '직접적으로 인스턴스를 생성할 수는 없다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_19'
     , '4'
     , '필드를 생성할 수 있다.'
);

-------------------------------------------------------------------------------
-- 중 20번문제 객관식 보기
INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_20'
     , '1'
     , '인터페이스는 오버라이딩의 강제성을 부여한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_20'
     , '2'
     , '단일 상속의 규칙을 깨기 위해 존재한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_20'
     , '3'
     , '확장의 개념을 실현한다.'
);

INSERT
  INTO JB_READ R
( 
       R.QUIZ_CODE
     , R.READ_LIST
     , R.READ_CONTENT
)  
VALUES
(
       'NORMAL_20'
     , '4'
     , 'static 키워드 메소드 작성이 가능하다.'
);

COMMIT;

--------------------------------------------------------------------------------

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_01';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_02';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_03';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_04';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_05';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_06';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_07';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_08';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_09';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_10';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_11';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_12';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_13';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_14';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_15';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_16';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_17';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_18';


UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'EASY'
WHERE QUIZ_TYPE = 'BASIC_19';

COMMIT;

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_01';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_02';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_03';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_04';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_05';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_06';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_07';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_08';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_09';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_10';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_11';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_12';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_13';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_14';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_15';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_16';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_17';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_18';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_19';

UPDATE  JB_QUIZ
SET QUIZ_TYPE = 'NORMAL'
WHERE QUIZ_TYPE = 'STANDARD_20';

COMMIT;

--------------------------------------------------------------------------------
-- 하 20번문제
INSERT
  INTO JB_QUIZ G
( 
       G.QUIZ_CODE
     , G.QUIZ_TYPE
     , G.QUIZ_QUESTION
     , G.QUIZ_SCORE
     , G.QUIZ_ANSWER  
)  
VALUES
(
       'EASY_20'
     , 'EASY'
     , '자바는 객체 지향 언어이다. (O / X)'
     , 1
     , 'O'
);

COMMIT;

--------------------------------------------------------------------------------
-- 유저 테이블 값 입력 ( 관리자 )
INSERT
  INTO JB_USER GP
(
  GP.USER_ID
, GP.USER_NO
, GP.ADMIN_ID
, GP.ADMIN_PWD
, GP.PLAY_ID
, GP.CHAR_NO
)
VALUES
(   
  'ADMIN'
, 0
, 'ADMIN'
, 'ADMIN'
, ''
, ''
);

COMMIT;
--------------------------------------------------------------------------------

SELECT 
       G.*
  FROM JB_INVENTORY G;

-- 문제은행 퀴즈 '하' 난이도
-- 하 1번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_01', 'BASIC_01', 'SQL 구문을 실행할때 필요한 SQL 구문을 표현하는 객체는 Statement이다.(O / X)'
, 1, 'O'
);
-- 하 2번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_02', 'BASIC_02', '클래스가 인터페이스를 상속 받을 때 사용하는 키워드는 implements이다.(O / X)'
, 1, 'O'
);
-- 하 3번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_03', 'BASIC_03', '모든 자바 애플리케이션은 ''메인''메서드에서 실행을 시작한다.(O / X)'
, 1, 'O'
);
-- 하 4번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_04', 'BASIC_04', '모든 자바 실행문은 ''; (세미콜론)''으로 끝난다.(O / X)'
, 1, 'O'
);
-- 하 5번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_05', 'BASIC_05', '모든 변수는 선언될 때 타입을 지정해야 한다.(O / X)'
, 1, 'O'
);
-- 하 6번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_06', 'BASIC_06', 'hello와 HELLO는 동일한 변수이다.(O / X)'
, 1, 'X'
);
-- 하 7번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_07', 'BASIC_07', 'byte 타입 변수에 300을 대입할 수 있다.(O / X)'
, 1, 'X'
);
-- 하 8번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_08', 'BASIC_08', 'boolean b = (3<5)? true : false; 의 결과는 false이다.(O / X)'
, 1, 'X'
);
-- 하 9번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_09', 'BASIC_09', '대입연산식 a = a+b 는 간결하게 a += b로 나타낼 수 있다.(O / X)'
, 1, 'O'
);
-- 하 10번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_10', 'BASIC_10', '클래스에서 매개변수 있는 생성자가 하나라도 존재하면 기본 생성자는 명시적으로 작성해야 한다.(O / X)'
, 1, 'O'
);
-- 하 11번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_11', 'BASIC_11', '클래스 외부에서 public 멤버에 접근할 수 있다.(O / X)'
, 1, 'O'
);
-- 하 12번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_12', 'BASIC_12', 'int형 변수에는 소수점이 붙은 값을 저장시킬 수 있다.(O / X)'
, 1, 'O'
);
-- 하 13번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_13', 'BASIC_13', '배열 변수를 선언하면 자동으로 배열 요소가 생성된다.(O / X)'
, 1, 'X'
);
-- 하 14번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_14', 'BASIC_14', '산술 연산자는 * / + - % 로 각각 더하기, 뺴기, 곱하기, 나누기, 나머지 연산자이다. (O / X)'
, 1, 'O'
);
-- 하 15번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_15', 'BASIC_15', 'double 자료형 데이터와 int 자료형 데이터를 연산하여 int 자료형으로 반환값을 받고자 한다.
   이 때 double 자료형은 묵시적 형변환이 이루어진다.(O / X)'
, 1, 'X'
);
-- 하 16번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_16', 'BASIC_16', '필드 접근 제한자의 접근가능 영역이 넓은 순서대로 나열하면 public, protected, default, private이다.(O / X)'
, 1, 'O'
);
-- 하 17번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_17', 'BASIC_17', '시스템 상에서 프로그램에 심각한 문제를 발생하여 비정상적으로 프로그램을 종료시키지만,
    미리 예측하고 처리할 수 있는 미약한 오류를 [예외 Exception](으)로 구분한다.(O / X)'
, 1, 'O'
);

-- 하 18번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_18', 'BASIC_18', '이미지 파일을 읽으려 한다. 가장 적합한 클래스는 FilInputStream이다 (O / X)'
, 1, 'O'
);
-- 하 19번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'EASY_19', 'BASIC_19', '파일의 크기를 알려고 할 때 필요한 클래스는 File이다. (O / X)'
, 1, 'O'
);
-- 상 1번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_01', 'EXPERT_01','원소들의 중복을 허용하지 않고, 원소의 순서는 의미가 없는 컬렉션에 대한 기능을 정의하고 있는 인터페이스는? (영어 3자)'
, 5, 'SET'
);
-- 상 2번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_02', 'EXPERT_02', '스레드는 CPU 자원을 얻어 실행하는 과정에서 여러 상태 변화를 겪을 수 있는데 Thread 클래스에서 정의된 메소드로
Running 상태에서 Not Running 상태로의 상태변화를 야기하는 것 중 sleep()과 다른 하나는 무엇인가?(영어 4()자)'
, 5, 'JOIN()'
);
-- 상 3번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_03', 'EXPERT_03','컨테이너에 자식 컴포넌트를 추가할 때 사용되는 메소드의 이름은? (영어 3자)'
, 5, 'ADD'
);
-- 상 4번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_04', 'EXPERT_04','Panel의 기본 배치관리자는? (영어 10자)'
, 5, 'FLOWLAYOUT'
);
-- 상 5번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_05', 'EXPERT_05','OR, AND 연산자는 OO 연산자이다. (한글 2자)'
, 5, '논리'
);
-- 상 6번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_06', 'EXPERT_06', 'byte타입의 연산 결과는 OOO타입이다. (영어 3자)'
, 5, 'INT'
);
-- 상 7번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_07', 'EXPERT_07', 'Collection API 중 key 값과 value 값으로 저장되는 것은 [   ] 이다. (영어 3자)'
, 5, 'MAP'
);
-- 상 8번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_08', 'EXPERT_08', 'try에서 예외 발생 여부와 관계 없이 강제(무조건)적으로 사용되는 키워드는? (영어 7자)'
, 5, 'FINALLY'
);
-- 상 9번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_09', 'EXPERT_09', '자기 자신을 의미하는 키워드는 (   ) 이다.  (영어 4자)'
, 5, 'THIS'
);
-- 상 10번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_10', 'EXPERT_10', '상속 관계에서 이름이 같은 메소드가 여러 개 정의될 수 있는 특징을 메소드 오버( ) 이라 한다. (한글 2자)'
, 5, '로딩'
);
-- 상 11번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_11', 'EXPERT_11', '하위 클래스에서 상위 클래스의 기본 생성자를 호출하는 키워드는 (    )() 이며 하위 클래스에서 상위 클래스의 필드나 메소드를 참조하는 키워드는 (   ). 이다.
           (    ) 안에 공통적으로 들어가는 키워드는? (영어 5자)'
, 5, 'SUPER'
);
-- 상 12번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_12', 'EXPERT_12', 'double형의 크기는 [?] 바이트이다.(숫자 1자)'
, 5, '8'
);
-- 상 13번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_13', 'EXPERT_13', '서브 클래스에서는 슈퍼 클래스와 같은 이름을 가지는 [ ? ]를 정의할 수 있다.(한글 3자)'
, 5, '메소드'
);
-- 상 14번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_14', 'EXPERT_14', '객체를 생성할 때는 [ ? ] 라는 단어를 사용한다.(영어 3자)'
, 5, 'NEW'
);
-- 상 15번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_15', 'EXPERT_15', '클래스는 [ ? ]와 메소드를 멤버로 가질 수 있다.(한글 2자)'
, 5, '필드'
);
-- 상 16번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_16', 'EXPERT_16', '보호하고 싶은 멤버에 private를 붙여 접근을 제한하는 기능은?.(한글 3자)'
, 5, '캡슐화'
);
-- 상 17번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_17', 'EXPERT_17', '생성자는 일반 메소드와는 달리 [    ](이)가 없으며 이름은 반드시 클래스 이름과 같아야 한다.(영어 6자)'
, 5, 'RETURN'
);
-- 상 18번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_18', 'EXPERT_18', '메소드 지정자[   ](은)는 더이상 하위 클래스에서 메소드 오버라이딩을 허용하지 않도록 지정하는 키워드이다.(영어 5자)'
, 5, 'FINAL'
);
-- 상 19번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_19', 'EXPERT_19', '컴파일에 영향을 미치지 않고, 상속받은 메소드를 재작성하는 메소드임을 알려주기 위한 주석과 유사한 개념인 이것은 무엇일까요?(영어 10자)'
, 5, 'ANNOTATION'
);
-- 상 20번문제
INSERT
  INTO JB_QUIZ G
( 
  G.QUIZ_CODE, G.QUIZ_TYPE, G.QUIZ_QUESTION
, G.QUIZ_SCORE, G.QUIZ_ANSWER  
)  
VALUES
(
  'HARD_20', 'EXPERT_20', '오버라이딩 시 매개변수의 타입,( ),순서(이)가 동일해야 한다. 괄호 안에 들어갈 요소를 작성하시오.?(한글 2자)'
, 5, '갯수'
);


